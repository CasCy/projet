-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: raidtracker
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (16),(16),(16),(16),(16),(16),(16);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `ilvl` int(11) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `magic` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `physick` int(11) NOT NULL,
  `repair` varchar(255) DEFAULT NULL,
  `slot` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_rxk63f1bcbpd9gha0e1xxoh7p` (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (5,400,'http://www.garlandtools.org/files/icons/item/42615.png',668,'Omega Gambison of Maiming',851,'Armorer','Body','O12S'),(10,405,'http://www.garlandtools.org/files/icons/item/34319.png',0,'Omega Claymore',109,'Blacksmith','Weapon','O12S'),(15,400,'http://www.garlandtools.org/files/icons/item/43836.png',851,'Omega Coat of Healing',486,'Weaver','Body','O12S');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_jobs`
--

DROP TABLE IF EXISTS `item_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `item_jobs` (
  `Item_id` int(11) NOT NULL,
  `jobs` varchar(255) DEFAULT NULL,
  KEY `FKo8e38bn2s45wtw61u39v31q8i` (`Item_id`),
  CONSTRAINT `FKo8e38bn2s45wtw61u39v31q8i` FOREIGN KEY (`Item_id`) REFERENCES `item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_jobs`
--

LOCK TABLES `item_jobs` WRITE;
/*!40000 ALTER TABLE `item_jobs` DISABLE KEYS */;
INSERT INTO `item_jobs` VALUES (5,'Dragoon'),(10,'DarkKnight'),(15,'WhiteMage'),(15,'Astrologian'),(15,'Scholar');
/*!40000 ALTER TABLE `item_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_stats`
--

DROP TABLE IF EXISTS `item_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `item_stats` (
  `Item_id` int(11) NOT NULL,
  `stats_id` int(11) NOT NULL,
  PRIMARY KEY (`Item_id`,`stats_id`),
  UNIQUE KEY `UK_svmkhrq8oo5iu70jn5rpbrctp` (`stats_id`),
  CONSTRAINT `FK9kh3tupqhy8n5h0eydm3c1wfe` FOREIGN KEY (`stats_id`) REFERENCES `stats` (`id`),
  CONSTRAINT `FKjvfoabheofq9bnbns7j0ofb2a` FOREIGN KEY (`Item_id`) REFERENCES `item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_stats`
--

LOCK TABLES `item_stats` WRITE;
/*!40000 ALTER TABLE `item_stats` DISABLE KEYS */;
INSERT INTO `item_stats` VALUES (5,1),(5,2),(5,3),(5,4),(10,6),(10,7),(10,8),(10,9),(15,11),(15,12),(15,13),(15,14);
/*!40000 ALTER TABLE `item_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listevoeux`
--

DROP TABLE IF EXISTS `listevoeux`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `listevoeux` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listevoeux`
--

LOCK TABLES `listevoeux` WRITE;
/*!40000 ALTER TABLE `listevoeux` DISABLE KEYS */;
/*!40000 ALTER TABLE `listevoeux` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listevoeux_item`
--

DROP TABLE IF EXISTS `listevoeux_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `listevoeux_item` (
  `listeVoeux_id` int(11) NOT NULL,
  `items_id` int(11) NOT NULL,
  PRIMARY KEY (`listeVoeux_id`,`items_id`),
  KEY `FKb087kjonxlx6i5hv08irp4quv` (`items_id`),
  CONSTRAINT `FKb087kjonxlx6i5hv08irp4quv` FOREIGN KEY (`items_id`) REFERENCES `item` (`id`),
  CONSTRAINT `FKqskwqy4d69bqhyqxxbnfw7ape` FOREIGN KEY (`listeVoeux_id`) REFERENCES `listevoeux` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listevoeux_item`
--

LOCK TABLES `listevoeux_item` WRITE;
/*!40000 ALTER TABLE `listevoeux_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `listevoeux_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loot`
--

DROP TABLE IF EXISTS `loot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `loot` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `membre_id` int(11) DEFAULT NULL,
  `raid_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5x1mvfyo0rtcwjey9is4vs2ep` (`item_id`),
  KEY `FKl4865pbpnm0jp72daunnse7vd` (`membre_id`),
  KEY `FK2ep2meihf0fkexviww5e97t58` (`raid_id`),
  CONSTRAINT `FK2ep2meihf0fkexviww5e97t58` FOREIGN KEY (`raid_id`) REFERENCES `raid` (`id`),
  CONSTRAINT `FK5x1mvfyo0rtcwjey9is4vs2ep` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`),
  CONSTRAINT `FKl4865pbpnm0jp72daunnse7vd` FOREIGN KEY (`membre_id`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loot`
--

LOCK TABLES `loot` WRITE;
/*!40000 ALTER TABLE `loot` DISABLE KEYS */;
/*!40000 ALTER TABLE `loot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membre_jobs`
--

DROP TABLE IF EXISTS `membre_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `membre_jobs` (
  `Membre_id` int(11) NOT NULL,
  `jobs` varchar(255) DEFAULT NULL,
  KEY `FKbgfeo5pi2x6srrpql4ru2v49l` (`Membre_id`),
  CONSTRAINT `FKbgfeo5pi2x6srrpql4ru2v49l` FOREIGN KEY (`Membre_id`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membre_jobs`
--

LOCK TABLES `membre_jobs` WRITE;
/*!40000 ALTER TABLE `membre_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `membre_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membre_prios`
--

DROP TABLE IF EXISTS `membre_prios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `membre_prios` (
  `Membre_id` int(11) NOT NULL,
  `prios` varchar(255) DEFAULT NULL,
  KEY `FKgwq3b9mbdgmhiht10niepe71a` (`Membre_id`),
  CONSTRAINT `FKgwq3b9mbdgmhiht10niepe71a` FOREIGN KEY (`Membre_id`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membre_prios`
--

LOCK TABLES `membre_prios` WRITE;
/*!40000 ALTER TABLE `membre_prios` DISABLE KEYS */;
/*!40000 ALTER TABLE `membre_prios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membre_roles`
--

DROP TABLE IF EXISTS `membre_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `membre_roles` (
  `Membre_id` int(11) NOT NULL,
  `roles` varchar(255) DEFAULT NULL,
  KEY `FKbpcy8fmdhrle2oepjtkhmjjls` (`Membre_id`),
  CONSTRAINT `FKbpcy8fmdhrle2oepjtkhmjjls` FOREIGN KEY (`Membre_id`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membre_roles`
--

LOCK TABLES `membre_roles` WRITE;
/*!40000 ALTER TABLE `membre_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `membre_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raid`
--

DROP TABLE IF EXISTS `raid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `raid` (
  `id` int(11) NOT NULL,
  `date` datetime(6) DEFAULT NULL,
  `roster_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKtimnnx3kynxsjooongle0kgx7` (`roster_id`),
  CONSTRAINT `FKtimnnx3kynxsjooongle0kgx7` FOREIGN KEY (`roster_id`) REFERENCES `roster` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raid`
--

LOCK TABLES `raid` WRITE;
/*!40000 ALTER TABLE `raid` DISABLE KEYS */;
/*!40000 ALTER TABLE `raid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roster`
--

DROP TABLE IF EXISTS `roster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roster` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_tl11f7nevp19yqrral33su0uq` (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roster`
--

LOCK TABLES `roster` WRITE;
/*!40000 ALTER TABLE `roster` DISABLE KEYS */;
/*!40000 ALTER TABLE `roster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats`
--

DROP TABLE IF EXISTS `stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stats` (
  `id` int(11) NOT NULL,
  `attrib` varchar(255) DEFAULT NULL,
  `valeur` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats`
--

LOCK TABLES `stats` WRITE;
/*!40000 ALTER TABLE `stats` DISABLE KEYS */;
INSERT INTO `stats` VALUES (1,'CriticalHit',227),(2,'Strenght',371),(3,'SkillSpeed',325),(4,'Vitality',419),(6,'Strenght',403),(7,'CriticalHit',249),(8,'Vitality',455),(9,'Determination',356),(11,'Vitality',377),(12,'CriticalHit',227),(13,'SpellSpeed',325),(14,'Mind',371);
/*!40000 ALTER TABLE `stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `utilisateur` (
  `DTYPE` varchar(31) NOT NULL,
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `serveur` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `listeVoeux_id` int(11) DEFAULT NULL,
  `roster_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_66vu1vfh4m2fw682xmd4lobqy` (`username`),
  KEY `FKebmeheu7ivyw25r07v9uivcvu` (`listeVoeux_id`),
  KEY `FKbf2ps0op9i0r0xnfjtecxlm32` (`roster_id`),
  CONSTRAINT `FKbf2ps0op9i0r0xnfjtecxlm32` FOREIGN KEY (`roster_id`) REFERENCES `roster` (`id`),
  CONSTRAINT `FKebmeheu7ivyw25r07v9uivcvu` FOREIGN KEY (`listeVoeux_id`) REFERENCES `listevoeux` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-14 16:43:40
