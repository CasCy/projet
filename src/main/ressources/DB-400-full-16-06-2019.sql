CREATE DATABASE  IF NOT EXISTS `raidtracker` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `raidtracker`;
-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: raidtracker
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (757),(757),(757),(757),(757),(757),(757);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `ilvl` int(11) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `magic` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `physick` int(11) NOT NULL,
  `repair` varchar(255) DEFAULT NULL,
  `slot` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_rxk63f1bcbpd9gha0e1xxoh7p` (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (5,400,'http://www.garlandtools.org/files/icons/item/42615.png',668,'Omega Gambison of Maiming',851,'Armorer','Body','O12S'),(10,405,'http://www.garlandtools.org/files/icons/item/34319.png',0,'Omega Claymore',109,'Blacksmith','Weapon','O12S'),(15,400,'http://www.garlandtools.org/files/icons/item/43836.png',851,'Omega Coat of Healing',486,'Weaver','Body','O12S'),(20,400,'http://www.garlandtools.org/files/icons/item/41635.png',608,'Omega Cap of Healing',347,'Weaver','Head','O11S'),(25,400,'http://www.garlandtools.org/files/icons/item/49348.png',608,'Omega Gloves of Healing',347,'Armorer','Hands','O10S'),(30,400,'http://www.garlandtools.org/files/icons/item/47347.png',527,'Omega Tassets of healing',301,'Armorer','Waist','O9S'),(35,400,'http://www.garlandtools.org/files/icons/item/49862.png',851,'Omega Trousers of Healing',486,'Weaver','Legs','O11S'),(40,400,'http://www.garlandtools.org/files/icons/item/49984.png',608,'Omega Shoes of Healing',347,'Leatherworker','Feet','O10S'),(44,400,'http://www.garlandtools.org/files/icons/item/48725.png',1,'Omega Ear Cuff of Healing',1,'Goldsmith','Earrings','O9S'),(48,400,'http://www.garlandtools.org/files/icons/item/48492.png',1,'Omega Chocker of Healing',1,'Goldsmith','Necklace','O9S'),(52,400,'http://www.garlandtools.org/files/icons/item/48997.png',1,'Omega Bracelet of Healing',1,'Goldsmith','Wirst','O9S'),(56,400,'http://www.garlandtools.org/files/icons/item/48246.png',1,'Omega Ring of Healing',1,'Goldsmith','Ring','O9S'),(61,400,'http://www.garlandtools.org/files/icons/item/32738.png',146,'Augmented Scaevan Magitek Cane',0,'Goldsmith','Weapon','Vendor'),(66,400,'http://www.garlandtools.org/files/icons/item/41717.png',608,'Augmented Scaevan Mask of Healing',347,'Goldsmith','Head','Vendor'),(71,400,'http://www.garlandtools.org/files/icons/item/43828.png',851,'Augmented Scaevan Coat of Healing',486,'Weaver','Body','Vendor'),(76,400,'http://www.garlandtools.org/files/icons/item/49602.png',608,'Augmented Scaevan Gloves of Healing',347,'Weaver','Hands','Vendor'),(81,400,'http://www.garlandtools.org/files/icons/item/47346.png',527,'Augmented Scaevan Tassets of Healing',301,'Armorer','Waist','Vendor'),(86,400,'http://www.garlandtools.org/files/icons/item/49841.png',851,'Augmented Scaevan Trousers of Healing',486,'Weaver','Legs','Vendor'),(91,400,'http://www.garlandtools.org/files/icons/item/49974.png',608,'Augmented Scaevan Shoes of Healing',347,'Leatherworker','Feet','Vendor'),(95,400,'http://www.garlandtools.org/files/icons/item/48724.png',1,'Augmented Scaevan Ear Cuff of Healing',1,'Goldsmith','Earrings','Vendor'),(99,400,'http://www.garlandtools.org/files/icons/item/48491.png',1,'Augmented Scaevan Choker of Healing',1,'Goldsmith','Necklace','Vendor'),(103,400,'http://www.garlandtools.org/files/icons/item/48996.png',1,'Augmented Scaevan Bracelet of Healing',1,'Goldsmith','Wirst','Vendor'),(107,400,'http://www.garlandtools.org/files/icons/item/48245.png',1,'Augmented Scaevan Ring of Healing',1,'Goldsmith','Ring','Vendor'),(112,405,'http://www.garlandtools.org/files/icons/item/32740.png',147,'Omega Cane',0,'Goldsmith','Weapon','O12S'),(117,405,'http://www.garlandtools.org/files/icons/item/39443.png',147,'Omega Codex',0,'Alchemist','Weapon','O12S'),(122,400,'http://www.garlandtools.org/files/icons/item/39439.png',146,'Augmented Scaevan Magitek Codex',0,'Alchemist','Weapon','Vendor'),(127,405,'http://www.garlandtools.org/files/icons/item/34916.png',147,'Omega Torquetum',0,'Alchemist','Weapon','O12S'),(132,405,'http://www.garlandtools.org/files/icons/item/34914.png',146,'Augmented Scaevan Magitek Orrery',0,'Goldsmith','Weapon','Vendor'),(137,400,'http://www.garlandtools.org/files/icons/item/41634.png',868,'Omega Circlet of Fending',868,'Goldsmith','Head','O10S'),(142,400,'http://www.garlandtools.org/files/icons/item/47347.png',527,'Omega Tassets of Casting',301,'Armorer','Waist','O9S'),(147,400,'http://www.garlandtools.org/files/icons/item/42614.png',1215,'Omega Gambison of Fending',1215,'Armorer','Body','O12S'),(152,400,'http://www.garlandtools.org/files/icons/item/49617.png',868,'Omega Armguards of Fending',868,'Armorer','Hands','O11S'),(156,400,'http://www.garlandtools.org/files/icons/item/48725.png',1,'Omega Ear Cuff of Casting',1,'Goldsmith','Earrings','O9S'),(161,400,'http://www.garlandtools.org/files/icons/item/47347.png',752,'Omega Tassets of Fending',752,'Armorer','Waist','O9S'),(166,400,'http://www.garlandtools.org/files/icons/item/49861.png',1215,'Omega Trousers of Fending',1215,'Leatherworker','Legs','O11S'),(170,400,'http://www.garlandtools.org/files/icons/item/48997.png',1,'Omega Bracelet of Casting',1,'Goldsmith','Wirst','O9S'),(175,400,'http://www.garlandtools.org/files/icons/item/49983.png',868,'Omega Shoes of Fending',868,'Armorer','Feet','O10S'),(179,400,'http://www.garlandtools.org/files/icons/item/48492.png',1,'Omega Choker of Casting',1,'Goldsmith','Necklace','O9S'),(183,400,'http://www.garlandtools.org/files/icons/item/48246.png',1,'Omega Ring of Casting',1,'Goldsmith','Ring','O9S'),(188,400,'http://www.garlandtools.org/files/icons/item/48725.png',1,'Omega Ear Cuff of Fending',1,'Goldsmith','Earrings','O9S'),(193,400,'http://www.garlandtools.org/files/icons/item/48492.png',1,'Omega Choker of Fending',1,'Goldsmith','Necklace','O9S'),(198,400,'http://www.garlandtools.org/files/icons/item/48997.png',1,'Omega Bracelets of Fending',1,'Goldsmith','Wirst','O9S'),(203,400,'http://www.garlandtools.org/files/icons/item/41633.png',608,'Omega Eye Mask of Casting',347,'Goldsmith','Head','O11S'),(208,400,'http://www.garlandtools.org/files/icons/item/48246.png',1,'Omega Ring of Fending',1,'Goldsmith','Ring','O9S'),(213,400,'http://www.garlandtools.org/files/icons/item/49347.png',608,'Omega Gloves of Casting',347,'Armorer','Hands','O10S'),(218,405,'http://www.garlandtools.org/files/icons/item/30591.png',0,'Omega Sword',109,'Blacksmith','Weapon','O12S'),(223,400,'http://www.garlandtools.org/files/icons/item/43835.png',851,'Omega Jacket of Casting',486,'Armorer','Body','O12S'),(228,405,'http://www.garlandtools.org/files/icons/item/30184.png',1472,'Omega Shield',1472,'Armorer','Shield','O12S'),(233,400,'http://www.garlandtools.org/files/icons/item/49982.png',608,'Omega Shoes of Casting',347,'Leatherworker','Feet','O10S'),(238,400,'http://www.garlandtools.org/files/icons/item/49860.png',851,'Omega Trousers of Casting',486,'Leatherworker','Legs','O11S'),(243,400,'http://www.garlandtools.org/files/icons/item/30589.png',0,'Augmented Scaevan Magitek Katzbalger',108,'Blacksmith','Weapon','Vendor'),(248,400,'http://www.garlandtools.org/files/icons/item/30182.png',1447,'Augmented Scaevan Magitek Shield',1447,'Armorer','Shield','Vendor'),(253,405,'http://www.garlandtools.org/files/icons/item/38874.png',147,'Omega Smallsword',0,'Goldsmith','Weapon','O12S'),(258,400,'http://www.garlandtools.org/files/icons/item/31563.png',0,'Augmented Scaevan Magitek Axe',108,'Blacksmith','Weapon','Vendor'),(263,405,'http://www.garlandtools.org/files/icons/item/39442.png',147,'Omega Grimoire',0,'Alchemist','Weapon','O12S'),(268,405,'http://www.garlandtools.org/files/icons/item/31565.png',0,'Omega Battleaxe',109,'Blacksmith','Weapon','O12S'),(273,405,'http://www.garlandtools.org/files/icons/item/33108.png',147,'Omega Rod',0,'Goldsmith','Weapon','O12S'),(278,400,'http://www.garlandtools.org/files/icons/item/34317.png',0,'Augmented Scaevan Magitek Greatsword',108,'Blacksmith','Weapon','Vendor'),(283,400,'http://www.garlandtools.org/files/icons/item/41514.png',868,'Augmented Scaevan Helm of Fending',868,'Armorer','Head','Vendor'),(288,400,'http://www.garlandtools.org/files/icons/item/38872.png',146,'Augmented Scaevan Magitek Hanger',0,'Goldsmith','Weapon','Vendor'),(293,400,'http://www.garlandtools.org/files/icons/item/43414.png',1215,'Augmented Scaevan Armor of Fending',1215,'Armorer','Body','Vendor'),(297,400,'http://www.garlandtools.org/files/icons/item/48996.png',1,'Augmented Scaevan Bracelet of Casting',1,'Goldsmith','Wirst','Vendor'),(302,400,'http://www.garlandtools.org/files/icons/item/49601.png',868,'Augmented Scaevan Gauntlets of Fending',868,'Armorer','Hands','Vendor'),(306,400,'http://www.garlandtools.org/files/icons/item/48491.png',1,'Augmented Scaevan Choker of Casting',1,'Goldsmith','Necklace','Vendor'),(311,400,'http://www.garlandtools.org/files/icons/item/47346.png',752,'Augmented Scaevan Tassets of Fending',752,'Armorer','Waist','Vendor'),(316,400,'http://www.garlandtools.org/files/icons/item/49840.png',1215,'Augmented Scaevan Trousers of Fending',1215,'Leatherworker','Legs','Vendor'),(321,400,'http://www.garlandtools.org/files/icons/item/43827.png',851,'Augmented Scaevan Coat of Casting',486,'Weaver','Body','Vendor'),(325,400,'http://www.garlandtools.org/files/icons/item/48724.png',1,'Augmented Scaevan Ear Cuff of Casting',1,'Goldsmith','Earrings','Vendor'),(330,400,'http://www.garlandtools.org/files/icons/item/47156.png',868,'Augmented Scaevan Greaves of Fending',868,'Armorer','Feet','Vendor'),(335,400,'http://www.garlandtools.org/files/icons/item/48724.png',1,'Augmented Scaevan Ear Cuff of Fending',1,'Goldsmith','Earrings','Vendor'),(340,400,'http://www.garlandtools.org/files/icons/item/49600.png',608,'Augmented Scaevan Gloves of Casting',347,'Weaver','Hands','Vendor'),(345,400,'http://www.garlandtools.org/files/icons/item/48491.png',1,'Augmented Scaevan Choker of Fending',1,'Goldsmith','Necklace','Vendor'),(350,400,'http://www.garlandtools.org/files/icons/item/41716.png',608,'Augmented Scaevan Mask of Casting',347,'Goldsmith','Head','Vendor'),(355,400,'http://www.garlandtools.org/files/icons/item/48996.png',1,'Augmented Scaevan Bracelet of Fending',1,'Goldsmith','Wirst','Vendor'),(359,400,'http://www.garlandtools.org/files/icons/item/48245.png',1,'Augmented Scaevan Ring of Casting',1,'Goldsmith','Ring','Vendor'),(364,400,'http://www.garlandtools.org/files/icons/item/48245.png',1,'Augmented Scaevan Ring of Fending',1,'Goldsmith','Ring','Vendor'),(369,400,'http://www.garlandtools.org/files/icons/item/49973.png',608,'Augmented Scaevan Shoes of Casting',347,'Leatherworker','Feet','Vendor'),(374,400,'http://www.garlandtools.org/files/icons/item/47346.png',527,'Augmented Scaevan Tassets of Casting',301,'Armorer','Waist','Vendor'),(379,400,'http://www.garlandtools.org/files/icons/item/49839.png',851,'Augmented Scaevan Trousers of Casting',486,'Weaver','Legs','Vendor'),(384,400,'http://www.garlandtools.org/files/icons/item/39438.png',146,'Augmented Scaevan Magitek Grimoire',0,'Alchemist','Weapon','Vendor'),(389,400,'http://www.garlandtools.org/files/icons/item/33106.png',146,'Augmented Scaevan Magitek Rod',0,'Goldsmith','Weapon','Vendor'),(394,400,'http://www.garlandtools.org/files/icons/item/41636.png',477,'Omega Circlet of Maiming',608,'Goldsmith','Head','O11S'),(399,400,'http://www.garlandtools.org/files/icons/item/49618.png',477,'Omega Armguards of Maiming',608,'Armorer','Hands','O10S'),(404,400,'http://www.garlandtools.org/files/icons/item/47347.png',414,'Omega Tassets of Maiming',527,'Armorer','Waist','O9S'),(409,400,'http://www.garlandtools.org/files/icons/item/49863.png',668,'Omega Trousers of Maiming',851,'Leatherworker','Legs','O11S'),(414,400,'http://www.garlandtools.org/files/icons/item/49985.png',477,'Omega Shoes of Maiming',608,'Armorer','Feet','O10S'),(418,400,'http://www.garlandtools.org/files/icons/item/48725.png',1,'Omega Ear Cuff of Slaying',1,'Goldsmith','Earrings','O9S'),(422,400,'http://www.garlandtools.org/files/icons/item/48492.png',1,'Omega Choker of Slaying',1,'Goldsmith','Necklace','O9S'),(426,400,'http://www.garlandtools.org/files/icons/item/48997.png',1,'Omega Bracelet of Slaying',1,'Goldsmith','Wirst','O9S'),(430,400,'http://www.garlandtools.org/files/icons/item/48246.png',1,'Omega Ring of Slaying',1,'Goldsmith','Ring','O9S'),(435,405,'http://www.garlandtools.org/files/icons/item/32356.png',0,'Omega Bow',109,'Goldsmith','Weapon','O12S'),(440,405,'http://www.garlandtools.org/files/icons/item/31965.png',0,'Omega Trident',109,'Blacksmith','Weapon','O12S'),(445,400,'http://www.garlandtools.org/files/icons/item/31963.png',0,'Augmented Scaevan Magitek Spear',108,'Blacksmith','Weapon','Vendor'),(449,400,'http://www.garlandtools.org/files/icons/item/48725.png',1,'Omega Ear of Aiming',1,'Goldsmith','Earrings','O9S'),(453,400,'http://www.garlandtools.org/files/icons/item/48492.png',1,'Omega Choker of Aiming',1,'Goldsmith','Necklace','O9S'),(457,400,'http://www.garlandtools.org/files/icons/item/48997.png',1,'Omega Bracelet of Aiming',1,'Goldsmith','Wirst','O9S'),(461,400,'http://www.garlandtools.org/files/icons/item/48246.png',1,'Omega Ring of Aiming',1,'Goldsmith','Ring','O9S'),(466,400,'http://www.garlandtools.org/files/icons/item/41638.png',477,'Omega Circlet of Aiming',477,'Goldsmith','Head','O11S'),(471,400,'http://www.garlandtools.org/files/icons/item/49865.png',668,'Omega Trousers of Aiming',668,'Leatherworker','Legs','O11S'),(476,400,'http://www.garlandtools.org/files/icons/item/41515.png',477,'Augmented Scaevan Helm of Maiming',608,'Armorer','Head','Vendor'),(481,400,'http://www.garlandtools.org/files/icons/item/47347.png',414,'Omega tassets of Aiming',414,'Armorer','Waist','O9S'),(486,400,'http://www.garlandtools.org/files/icons/item/43415.png',668,'Augmented Scaevan Armor of Maiming',851,'Armorer','Body','Vendor'),(491,400,'http://www.garlandtools.org/files/icons/item/49603.png',477,'Augmented Scaevan Gauntlets of Maiming',608,'Armorer','Hands','Vendor'),(496,400,'http://www.garlandtools.org/files/icons/item/49349.png',477,'Omega Gloves of Aiming',477,'Armorer','Hands','O10S'),(501,400,'http://www.garlandtools.org/files/icons/item/47346.png',414,'Augmented Scaevan Tassets of Maiming',527,'Armorer','Waist','Vendor'),(506,400,'http://www.garlandtools.org/files/icons/item/43837.png',668,'Omega Jacket of Aiming',668,'Armorer','Body','O12S'),(511,400,'http://www.garlandtools.org/files/icons/item/49842.png',668,'Augmented Scaevan Trousers of Maiming',851,'Leatherworker','Legs','Vendor'),(516,400,'http://www.garlandtools.org/files/icons/item/49987.png',477,'Omega Boots of Aiming',477,'Armorer','Feet','O10S'),(521,400,'http://www.garlandtools.org/files/icons/item/47157.png',477,'Augmented Scaevan Greaves of Maiming',608,'Armorer','Feet','Vendor'),(526,405,'http://www.garlandtools.org/files/icons/item/39717.png',0,'Omegafire',109,'Blacksmith','Weapon','O12S'),(530,400,'http://www.garlandtools.org/files/icons/item/48724.png',1,'Augmented Scaevan Ear Cuff of Slaying',1,'Goldsmith','Earrings','Vendor'),(534,400,'http://www.garlandtools.org/files/icons/item/48491.png',1,'Augmented Scaevan Choker of Slaying',1,'Goldsmith','Necklace','Vendor'),(539,400,'http://www.garlandtools.org/files/icons/item/39715.png',0,'Augmented Scaevan Magitek Blunderbuss',108,'Blacksmith','Weapon','Vendor'),(543,400,'http://www.garlandtools.org/files/icons/item/48996.png',1,'Augmented Scaevan Bracelet of Slaying',1,'Goldsmith','Wirst','Vendor'),(547,400,'http://www.garlandtools.org/files/icons/item/48245.png',1,'Augmented Scaevan Ring of Slaying',1,'Goldsmith','Ring','Vendor'),(552,400,'http://www.garlandtools.org/files/icons/item/47906.png',477,'Augmented Scaevan Headgear of Aiming',477,'Goldsmith','Head','Vendor'),(557,400,'http://www.garlandtools.org/files/icons/item/49605.png',477,'Augmented Scaevan Gloves of Aiming',477,'Armorer','Hands','Vendor'),(562,400,'http://www.garlandtools.org/files/icons/item/38571.png',0,'Augmented Scaevan Magitek Samurai Blade',108,'Blacksmith','Weapon','Vendor'),(567,400,'http://www.garlandtools.org/files/icons/item/47346.png',414,'Augmented Scaevan Tassets of Aiming',414,'Armorer','Waist','Vendor'),(572,405,'http://www.garlandtools.org/files/icons/item/38573.png',0,'Omega Samurai Blade',109,'Blacksmith','Weapon','O12S'),(577,400,'http://www.garlandtools.org/files/icons/item/43830.png',668,'Augmented Scaevan Tabard of Aiming',668,'Weaver','Body','Vendor'),(582,405,'http://www.garlandtools.org/files/icons/item/31157.png',0,'Omega Knuckles',109,'Blacksmith','Weapon','O12S'),(587,400,'http://www.garlandtools.org/files/icons/item/31155.png',0,'Augmented Scaevan Magitek Tonfa',108,'Blacksmith','Weapon','Vendor'),(592,400,'http://www.garlandtools.org/files/icons/item/49844.png',668,'Augmented Scaevan Trousers of Aiming',668,'Weaver','Legs','Vendor'),(597,400,'http://www.garlandtools.org/files/icons/item/47159.png',477,'Augmented Scaevan Greaves of Aiming',477,'Armorer','Feet','Vendor'),(602,400,'http://www.garlandtools.org/files/icons/item/41637.png',477,'Omega Circlet of Striking',477,'Goldsmith','Head','O10S'),(606,400,'http://www.garlandtools.org/files/icons/item/48724.png',1,'Augmented Scaevan Ear Cuff of Aiming',1,'Goldsmith','Earrings','Vendor'),(611,400,'http://www.garlandtools.org/files/icons/item/42616.png',668,'Omega Gambison of Striking',668,'Armorer','Body','O12S'),(615,400,'http://www.garlandtools.org/files/icons/item/48491.png',1,'Augmented Scaevan Choker of Aiming',1,'Goldsmith','Necklace','Vendor'),(620,400,'http://www.garlandtools.org/files/icons/item/49619.png',477,'Omega Armguards of Striking',477,'Armorer','Hands','O11S'),(624,400,'http://www.garlandtools.org/files/icons/item/48996.png',1,'Augmented Scaevan Bracelet of Aiming',1,'Goldsmith','Wirst','Vendor'),(629,400,'http://www.garlandtools.org/files/icons/item/47347.png',414,'Omega Tasstes of Striking',414,'Armorer','Waist','O9S'),(633,400,'http://www.garlandtools.org/files/icons/item/48245.png',1,'Augmented Scaevan Ring of Aiming',1,'Goldsmith','Ring','Vendor'),(638,400,'http://www.garlandtools.org/files/icons/item/49864.png',668,'Omega Trousers of Striking',668,'Leatherworker','Legs','O11S'),(643,400,'http://www.garlandtools.org/files/icons/item/32354.png',0,'Augmented Scaevan Magitek Bow',108,'Blacksmith','Weapon','Vendor'),(648,400,'http://www.garlandtools.org/files/icons/item/49986.png',477,'Omega Shoes of Striking',477,'Armorer','Feet','O10S'),(657,400,'http://www.garlandtools.org/files/icons/item/47160.png',477,'Augmented Scaevan Greaves of Scouting',477,'Armorer','Feet','Vendor'),(666,400,'http://www.garlandtools.org/files/icons/item/49845.png',668,'Augmented Scaevan Trousers of Scouting',668,'Weaver','Legs','Vendor'),(671,400,'http://www.garlandtools.org/files/icons/item/47346.png',414,'Augmented Scaevan Tassets of Scouting',414,'Armorer','Waist','Vendor'),(676,400,'http://www.garlandtools.org/files/icons/item/47905.png',477,'Augmented Scaevan Headgear of Striking',477,'Goldsmith','Head','Vendor'),(681,400,'http://www.garlandtools.org/files/icons/item/43831.png',668,'Augmented Scaevan Tabard of Scouting',668,'Weaver','Body','Vendor'),(686,400,'http://www.garlandtools.org/files/icons/item/43829.png',668,'Augmented Scaevan Jacket of Striking',668,'Leatherworker','Body','Vendor'),(691,400,'http://www.garlandtools.org/files/icons/item/49606.png',477,'Augmented Scaevan Gloves of Scouting',477,'Armorer','Hands','Vendor'),(696,400,'http://www.garlandtools.org/files/icons/item/49604.png',477,'Augmented Scaevan Armguards of Striking',477,'Armorer','Hands','Vendor'),(701,400,'http://www.garlandtools.org/files/icons/item/41718.png',477,'Augmented Scaevan Visor of Scouting',477,'Goldsmith','Head','Vendor'),(706,400,'http://www.garlandtools.org/files/icons/item/47346.png',414,'Augmented Scaevan Tassets of Striking',414,'Armorer','Waist','Vendor'),(711,405,'http://www.garlandtools.org/files/icons/item/33943.png',0,'Omega Sickles',109,'Blacksmith','Weapon','O12S'),(716,400,'http://www.garlandtools.org/files/icons/item/49843.png',668,'Augmented Scaevan Trousers of Striking',668,'Leatherworker','Legs','Vendor'),(721,400,'http://www.garlandtools.org/files/icons/item/33941.png',0,'Augmented Scaevan Magitek Daggers',108,'Blacksmith','Weapon','Vendor'),(726,400,'http://www.garlandtools.org/files/icons/item/47158.png',477,'Augmented Scaevan Greaves of Striking',477,'Armorer','Feet','Vendor'),(731,400,'http://www.garlandtools.org/files/icons/item/41639.png',477,'Omega Circlet of Scouting',477,'Goldsmith','Head','O11S'),(736,400,'http://www.garlandtools.org/files/icons/item/49350.png',477,'Omega Armguards of Scouting',477,'Armorer','Hands','O10S'),(741,400,'http://www.garlandtools.org/files/icons/item/43838.png',668,'Omega Coat of Scouting',668,'Armorer','Body','O12S'),(746,400,'http://www.garlandtools.org/files/icons/item/47347.png',414,'Omega Tassets of Scouting',414,'Armorer','Waist','O9S'),(751,400,'http://www.garlandtools.org/files/icons/item/49866.png',668,'Omega Trousers of Scouting',668,'Weaver','Legs','O11S'),(756,400,'http://www.garlandtools.org/files/icons/item/49988.png',477,'Omega Boots of Scouting',477,'Armorer','Feet','O10S');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_jobs`
--

DROP TABLE IF EXISTS `item_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `item_jobs` (
  `Item_id` int(11) NOT NULL,
  `jobs` varchar(255) DEFAULT NULL,
  KEY `FKo8e38bn2s45wtw61u39v31q8i` (`Item_id`),
  CONSTRAINT `FKo8e38bn2s45wtw61u39v31q8i` FOREIGN KEY (`Item_id`) REFERENCES `item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_jobs`
--

LOCK TABLES `item_jobs` WRITE;
/*!40000 ALTER TABLE `item_jobs` DISABLE KEYS */;
INSERT INTO `item_jobs` VALUES (5,'Dragoon'),(10,'DarkKnight'),(15,'WhiteMage'),(15,'Astrologian'),(15,'Scholar'),(20,'WhiteMage'),(20,'Scholar'),(20,'Astrologian'),(25,'WhiteMage'),(25,'Scholar'),(25,'Astrologian'),(30,'WhiteMage'),(30,'Scholar'),(30,'Astrologian'),(35,'WhiteMage'),(35,'Scholar'),(35,'Astrologian'),(40,'WhiteMage'),(40,'Scholar'),(40,'Astrologian'),(44,'WhiteMage'),(44,'Scholar'),(44,'Astrologian'),(48,'WhiteMage'),(48,'Scholar'),(48,'Astrologian'),(52,'WhiteMage'),(52,'Scholar'),(52,'Astrologian'),(56,'WhiteMage'),(56,'Scholar'),(56,'Astrologian'),(61,'WhiteMage'),(66,'WhiteMage'),(66,'Scholar'),(66,'Astrologian'),(71,'WhiteMage'),(71,'Scholar'),(71,'Astrologian'),(76,'WhiteMage'),(76,'Scholar'),(76,'Astrologian'),(81,'WhiteMage'),(81,'Scholar'),(81,'Astrologian'),(86,'WhiteMage'),(86,'Scholar'),(86,'Astrologian'),(91,'WhiteMage'),(91,'Scholar'),(91,'Astrologian'),(95,'WhiteMage'),(95,'Scholar'),(95,'Astrologian'),(99,'WhiteMage'),(99,'Scholar'),(99,'Astrologian'),(103,'WhiteMage'),(103,'Scholar'),(103,'Astrologian'),(107,'WhiteMage'),(107,'Scholar'),(107,'Astrologian'),(112,'WhiteMage'),(117,'Scholar'),(122,'Scholar'),(127,'Astrologian'),(132,'Astrologian'),(137,'Gunbreaker'),(137,'DarkKnight'),(137,'Warrior'),(137,'Paladin'),(142,'BlackMage'),(142,'Summoner'),(142,'RedMage'),(147,'Gunbreaker'),(147,'DarkKnight'),(147,'Warrior'),(147,'Paladin'),(152,'Gunbreaker'),(152,'DarkKnight'),(152,'Warrior'),(152,'Paladin'),(156,'BlackMage'),(156,'Summoner'),(156,'RedMage'),(161,'Gunbreaker'),(161,'DarkKnight'),(161,'Warrior'),(161,'Paladin'),(166,'Gunbreaker'),(166,'DarkKnight'),(166,'Warrior'),(166,'Paladin'),(170,'BlackMage'),(170,'Summoner'),(170,'RedMage'),(175,'Gunbreaker'),(175,'DarkKnight'),(175,'Warrior'),(175,'Paladin'),(179,'BlackMage'),(179,'Summoner'),(179,'RedMage'),(183,'BlackMage'),(183,'Summoner'),(183,'RedMage'),(188,'Gunbreaker'),(188,'DarkKnight'),(188,'Warrior'),(188,'Paladin'),(193,'Gunbreaker'),(193,'DarkKnight'),(193,'Warrior'),(193,'Paladin'),(198,'Gunbreaker'),(198,'DarkKnight'),(198,'Warrior'),(198,'Paladin'),(203,'BlackMage'),(203,'Summoner'),(203,'RedMage'),(208,'Gunbreaker'),(208,'DarkKnight'),(208,'Warrior'),(208,'Paladin'),(213,'BlackMage'),(213,'Summoner'),(213,'RedMage'),(218,'Paladin'),(223,'BlackMage'),(223,'Summoner'),(223,'RedMage'),(228,'Paladin'),(233,'BlackMage'),(233,'Summoner'),(233,'RedMage'),(238,'BlackMage'),(238,'Summoner'),(238,'RedMage'),(243,'Paladin'),(248,'Paladin'),(253,'RedMage'),(258,'Warrior'),(263,'Summoner'),(268,'Warrior'),(273,'BlackMage'),(278,'DarkKnight'),(283,'Gunbreaker'),(283,'DarkKnight'),(283,'Warrior'),(283,'Paladin'),(288,'RedMage'),(293,'Gunbreaker'),(293,'DarkKnight'),(293,'Warrior'),(293,'Paladin'),(297,'BlackMage'),(297,'Summoner'),(297,'RedMage'),(302,'Gunbreaker'),(302,'DarkKnight'),(302,'Warrior'),(302,'Paladin'),(306,'BlackMage'),(306,'Summoner'),(306,'RedMage'),(311,'Gunbreaker'),(311,'DarkKnight'),(311,'Warrior'),(311,'Paladin'),(316,'Gunbreaker'),(316,'DarkKnight'),(316,'Warrior'),(316,'Paladin'),(321,'BlackMage'),(321,'Summoner'),(321,'RedMage'),(325,'BlackMage'),(325,'Summoner'),(325,'RedMage'),(330,'Gunbreaker'),(330,'DarkKnight'),(330,'Warrior'),(330,'Paladin'),(335,'Gunbreaker'),(335,'DarkKnight'),(335,'Warrior'),(335,'Paladin'),(340,'BlackMage'),(340,'Summoner'),(340,'RedMage'),(345,'Gunbreaker'),(345,'DarkKnight'),(345,'Warrior'),(345,'Paladin'),(350,'BlackMage'),(350,'Summoner'),(350,'RedMage'),(355,'Gunbreaker'),(355,'DarkKnight'),(355,'Warrior'),(355,'Paladin'),(359,'BlackMage'),(359,'Summoner'),(359,'RedMage'),(364,'Gunbreaker'),(364,'DarkKnight'),(364,'Warrior'),(364,'Paladin'),(369,'BlackMage'),(369,'Summoner'),(369,'RedMage'),(374,'BlackMage'),(374,'Summoner'),(374,'RedMage'),(379,'BlackMage'),(379,'Summoner'),(379,'RedMage'),(384,'Summoner'),(389,'BlackMage'),(394,'Dragoon'),(399,'Dragoon'),(404,'Dragoon'),(409,'Dragoon'),(414,'Dragoon'),(418,'Monk'),(418,'Dragoon'),(418,'Samurai'),(422,'Monk'),(422,'Dragoon'),(422,'Samurai'),(426,'Monk'),(426,'Dragoon'),(426,'Samurai'),(430,'Monk'),(430,'Dragoon'),(430,'Samurai'),(435,'Bard'),(440,'Dragoon'),(445,'Dragoon'),(449,'Machinist'),(449,'Dancer'),(449,'Bard'),(449,'Ninja'),(453,'Machinist'),(453,'Dancer'),(453,'Bard'),(453,'Ninja'),(457,'Machinist'),(457,'Dancer'),(457,'Bard'),(457,'Ninja'),(461,'Machinist'),(461,'Dancer'),(461,'Bard'),(461,'Ninja'),(466,'Machinist'),(466,'Dancer'),(466,'Bard'),(471,'Machinist'),(471,'Dancer'),(471,'Bard'),(476,'Dragoon'),(481,'Machinist'),(481,'Dancer'),(481,'Bard'),(486,'Dragoon'),(491,'Dragoon'),(496,'Machinist'),(496,'Dancer'),(496,'Bard'),(501,'Dragoon'),(506,'Machinist'),(506,'Dancer'),(506,'Bard'),(511,'Dragoon'),(516,'Machinist'),(516,'Dancer'),(516,'Bard'),(521,'Dragoon'),(526,'Machinist'),(530,'Monk'),(530,'Dragoon'),(530,'Samurai'),(534,'Monk'),(534,'Dragoon'),(534,'Samurai'),(539,'Machinist'),(543,'Monk'),(543,'Dragoon'),(543,'Samurai'),(547,'Monk'),(547,'Dragoon'),(547,'Samurai'),(552,'Machinist'),(552,'Dancer'),(552,'Bard'),(557,'Machinist'),(557,'Dancer'),(557,'Bard'),(562,'Samurai'),(567,'Machinist'),(567,'Dancer'),(567,'Bard'),(572,'Samurai'),(577,'Machinist'),(577,'Dancer'),(577,'Bard'),(582,'Monk'),(587,'Monk'),(592,'Machinist'),(592,'Dancer'),(592,'Bard'),(597,'Machinist'),(597,'Dancer'),(597,'Bard'),(602,'Monk'),(602,'Samurai'),(606,'Machinist'),(606,'Dancer'),(606,'Bard'),(606,'Ninja'),(611,'Monk'),(611,'Samurai'),(615,'Machinist'),(615,'Dancer'),(615,'Bard'),(615,'Ninja'),(620,'Monk'),(620,'Samurai'),(624,'Machinist'),(624,'Dancer'),(624,'Bard'),(624,'Ninja'),(629,'Monk'),(629,'Samurai'),(633,'Machinist'),(633,'Dancer'),(633,'Bard'),(633,'Ninja'),(638,'Monk'),(638,'Samurai'),(643,'Bard'),(648,'Monk'),(648,'Samurai'),(657,'Ninja'),(666,'Ninja'),(671,'Ninja'),(676,'Monk'),(676,'Samurai'),(681,'Ninja'),(686,'Monk'),(686,'Samurai'),(691,'Ninja'),(696,'Monk'),(696,'Samurai'),(701,'Ninja'),(706,'Monk'),(706,'Samurai'),(711,'Ninja'),(716,'Monk'),(716,'Samurai'),(721,'Ninja'),(726,'Monk'),(726,'Samurai'),(731,'Ninja'),(736,'Ninja'),(741,'Ninja'),(746,'Ninja'),(751,'Ninja'),(756,'Ninja');
/*!40000 ALTER TABLE `item_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_stats`
--

DROP TABLE IF EXISTS `item_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `item_stats` (
  `Item_id` int(11) NOT NULL,
  `stats_id` int(11) NOT NULL,
  PRIMARY KEY (`Item_id`,`stats_id`),
  UNIQUE KEY `UK_svmkhrq8oo5iu70jn5rpbrctp` (`stats_id`),
  CONSTRAINT `FK9kh3tupqhy8n5h0eydm3c1wfe` FOREIGN KEY (`stats_id`) REFERENCES `stats` (`id`),
  CONSTRAINT `FKjvfoabheofq9bnbns7j0ofb2a` FOREIGN KEY (`Item_id`) REFERENCES `item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_stats`
--

LOCK TABLES `item_stats` WRITE;
/*!40000 ALTER TABLE `item_stats` DISABLE KEYS */;
INSERT INTO `item_stats` VALUES (5,1),(5,2),(5,3),(5,4),(10,6),(10,7),(10,8),(10,9),(15,11),(15,12),(15,13),(15,14),(20,16),(20,17),(20,18),(20,19),(25,21),(25,22),(25,23),(25,24),(30,26),(30,27),(30,28),(30,29),(35,31),(35,32),(35,33),(35,34),(40,36),(40,37),(40,38),(40,39),(44,41),(44,42),(44,43),(48,45),(48,46),(48,47),(52,49),(52,50),(52,51),(56,53),(56,54),(56,55),(61,57),(61,58),(61,59),(61,60),(66,62),(66,63),(66,64),(66,65),(71,67),(71,68),(71,69),(71,70),(76,72),(76,73),(76,74),(76,75),(81,77),(81,78),(81,79),(81,80),(86,82),(86,83),(86,84),(86,85),(91,87),(91,88),(91,89),(91,90),(95,92),(95,93),(95,94),(99,96),(99,97),(99,98),(103,100),(103,101),(103,102),(107,104),(107,105),(107,106),(112,108),(112,109),(112,110),(112,111),(117,113),(117,114),(117,115),(117,116),(122,118),(122,119),(122,120),(122,121),(127,123),(127,124),(127,125),(127,126),(132,128),(132,129),(132,130),(132,131),(137,133),(137,134),(137,135),(137,136),(142,138),(142,139),(142,140),(142,141),(147,143),(147,144),(147,145),(147,146),(152,148),(152,149),(152,150),(152,151),(156,153),(156,154),(156,155),(161,157),(161,158),(161,159),(161,160),(166,162),(166,163),(166,164),(166,165),(170,167),(170,168),(170,169),(175,171),(175,172),(175,173),(175,174),(179,176),(179,177),(179,178),(183,180),(183,181),(183,182),(188,184),(188,185),(188,186),(188,187),(193,189),(193,190),(193,191),(193,192),(198,194),(198,195),(198,196),(198,197),(203,199),(203,200),(203,201),(203,202),(208,204),(208,205),(208,206),(208,207),(213,209),(213,210),(213,211),(213,212),(218,214),(218,215),(218,216),(218,217),(223,219),(223,220),(223,221),(223,222),(228,224),(228,225),(228,226),(228,227),(233,229),(233,230),(233,231),(233,232),(238,234),(238,235),(238,236),(238,237),(243,239),(243,240),(243,241),(243,242),(248,244),(248,245),(248,246),(248,247),(253,249),(253,250),(253,251),(253,252),(258,254),(258,255),(258,256),(258,257),(263,259),(263,260),(263,261),(263,262),(268,264),(268,265),(268,266),(268,267),(273,269),(273,270),(273,271),(273,272),(278,274),(278,275),(278,276),(278,277),(283,279),(283,280),(283,281),(283,282),(288,284),(288,285),(288,286),(288,287),(293,289),(293,290),(293,291),(293,292),(297,294),(297,295),(297,296),(302,298),(302,299),(302,300),(302,301),(306,303),(306,304),(306,305),(311,307),(311,308),(311,309),(311,310),(316,312),(316,313),(316,314),(316,315),(321,317),(321,318),(321,319),(321,320),(325,322),(325,323),(325,324),(330,326),(330,327),(330,328),(330,329),(335,331),(335,332),(335,333),(335,334),(340,336),(340,337),(340,338),(340,339),(345,341),(345,342),(345,343),(345,344),(350,346),(350,347),(350,348),(350,349),(355,351),(355,352),(355,353),(355,354),(359,356),(359,357),(359,358),(364,360),(364,361),(364,362),(364,363),(369,365),(369,366),(369,367),(369,368),(374,370),(374,371),(374,372),(374,373),(379,375),(379,376),(379,377),(379,378),(384,380),(384,381),(384,382),(384,383),(389,385),(389,386),(389,387),(389,388),(394,390),(394,391),(394,392),(394,393),(399,395),(399,396),(399,397),(399,398),(404,400),(404,401),(404,402),(404,403),(409,405),(409,406),(409,407),(409,408),(414,410),(414,411),(414,412),(414,413),(418,415),(418,416),(418,417),(422,419),(422,420),(422,421),(426,423),(426,424),(426,425),(430,427),(430,428),(430,429),(435,431),(435,432),(435,433),(435,434),(440,436),(440,437),(440,438),(440,439),(445,441),(445,442),(445,443),(445,444),(449,446),(449,447),(449,448),(453,450),(453,451),(453,452),(457,454),(457,455),(457,456),(461,458),(461,459),(461,460),(466,462),(466,463),(466,464),(466,465),(471,467),(471,468),(471,469),(471,470),(476,472),(476,473),(476,474),(476,475),(481,477),(481,478),(481,479),(481,480),(486,482),(486,483),(486,484),(486,485),(491,487),(491,488),(491,489),(491,490),(496,492),(496,493),(496,494),(496,495),(501,497),(501,498),(501,499),(501,500),(506,502),(506,503),(506,504),(506,505),(511,507),(511,508),(511,509),(511,510),(516,512),(516,513),(516,514),(516,515),(521,517),(521,518),(521,519),(521,520),(526,522),(526,523),(526,524),(526,525),(530,527),(530,528),(530,529),(534,531),(534,532),(534,533),(539,535),(539,536),(539,537),(539,538),(543,540),(543,541),(543,542),(547,544),(547,545),(547,546),(552,548),(552,549),(552,550),(552,551),(557,553),(557,554),(557,555),(557,556),(562,558),(562,559),(562,560),(562,561),(567,563),(567,564),(567,565),(567,566),(572,568),(572,569),(572,570),(572,571),(577,573),(577,574),(577,575),(577,576),(582,578),(582,579),(582,580),(582,581),(587,583),(587,584),(587,585),(587,586),(592,588),(592,589),(592,590),(592,591),(597,593),(597,594),(597,595),(597,596),(602,598),(602,599),(602,600),(602,601),(606,603),(606,604),(606,605),(611,607),(611,608),(611,609),(611,610),(615,612),(615,613),(615,614),(620,616),(620,617),(620,618),(620,619),(624,621),(624,622),(624,623),(629,625),(629,626),(629,627),(629,628),(633,630),(633,631),(633,632),(638,634),(638,635),(638,636),(638,637),(643,639),(643,640),(643,641),(643,642),(648,644),(648,645),(648,646),(648,647),(657,653),(657,654),(657,655),(657,656),(666,662),(666,663),(666,664),(666,665),(671,667),(671,668),(671,669),(671,670),(676,672),(676,673),(676,674),(676,675),(681,677),(681,678),(681,679),(681,680),(686,682),(686,683),(686,684),(686,685),(691,687),(691,688),(691,689),(691,690),(696,692),(696,693),(696,694),(696,695),(701,697),(701,698),(701,699),(701,700),(706,702),(706,703),(706,704),(706,705),(711,707),(711,708),(711,709),(711,710),(716,712),(716,713),(716,714),(716,715),(721,717),(721,718),(721,719),(721,720),(726,722),(726,723),(726,724),(726,725),(731,727),(731,728),(731,729),(731,730),(736,732),(736,733),(736,734),(736,735),(741,737),(741,738),(741,739),(741,740),(746,742),(746,743),(746,744),(746,745),(751,747),(751,748),(751,749),(751,750),(756,752),(756,753),(756,754),(756,755);
/*!40000 ALTER TABLE `item_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listevoeux`
--

DROP TABLE IF EXISTS `listevoeux`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `listevoeux` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listevoeux`
--

LOCK TABLES `listevoeux` WRITE;
/*!40000 ALTER TABLE `listevoeux` DISABLE KEYS */;
/*!40000 ALTER TABLE `listevoeux` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listevoeux_item`
--

DROP TABLE IF EXISTS `listevoeux_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `listevoeux_item` (
  `listeVoeux_id` int(11) NOT NULL,
  `items_id` int(11) NOT NULL,
  PRIMARY KEY (`listeVoeux_id`,`items_id`),
  KEY `FKb087kjonxlx6i5hv08irp4quv` (`items_id`),
  CONSTRAINT `FKb087kjonxlx6i5hv08irp4quv` FOREIGN KEY (`items_id`) REFERENCES `item` (`id`),
  CONSTRAINT `FKqskwqy4d69bqhyqxxbnfw7ape` FOREIGN KEY (`listeVoeux_id`) REFERENCES `listevoeux` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listevoeux_item`
--

LOCK TABLES `listevoeux_item` WRITE;
/*!40000 ALTER TABLE `listevoeux_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `listevoeux_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loot`
--

DROP TABLE IF EXISTS `loot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `loot` (
  `id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `membre_id` int(11) DEFAULT NULL,
  `raid_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5x1mvfyo0rtcwjey9is4vs2ep` (`item_id`),
  KEY `FKl4865pbpnm0jp72daunnse7vd` (`membre_id`),
  KEY `FK2ep2meihf0fkexviww5e97t58` (`raid_id`),
  CONSTRAINT `FK2ep2meihf0fkexviww5e97t58` FOREIGN KEY (`raid_id`) REFERENCES `raid` (`id`),
  CONSTRAINT `FK5x1mvfyo0rtcwjey9is4vs2ep` FOREIGN KEY (`item_id`) REFERENCES `item` (`id`),
  CONSTRAINT `FKl4865pbpnm0jp72daunnse7vd` FOREIGN KEY (`membre_id`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loot`
--

LOCK TABLES `loot` WRITE;
/*!40000 ALTER TABLE `loot` DISABLE KEYS */;
/*!40000 ALTER TABLE `loot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membre_jobs`
--

DROP TABLE IF EXISTS `membre_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `membre_jobs` (
  `Membre_id` int(11) NOT NULL,
  `jobs` varchar(255) DEFAULT NULL,
  KEY `FKbgfeo5pi2x6srrpql4ru2v49l` (`Membre_id`),
  CONSTRAINT `FKbgfeo5pi2x6srrpql4ru2v49l` FOREIGN KEY (`Membre_id`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membre_jobs`
--

LOCK TABLES `membre_jobs` WRITE;
/*!40000 ALTER TABLE `membre_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `membre_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membre_prios`
--

DROP TABLE IF EXISTS `membre_prios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `membre_prios` (
  `Membre_id` int(11) NOT NULL,
  `prios` varchar(255) DEFAULT NULL,
  KEY `FKgwq3b9mbdgmhiht10niepe71a` (`Membre_id`),
  CONSTRAINT `FKgwq3b9mbdgmhiht10niepe71a` FOREIGN KEY (`Membre_id`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membre_prios`
--

LOCK TABLES `membre_prios` WRITE;
/*!40000 ALTER TABLE `membre_prios` DISABLE KEYS */;
/*!40000 ALTER TABLE `membre_prios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membre_roles`
--

DROP TABLE IF EXISTS `membre_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `membre_roles` (
  `Membre_id` int(11) NOT NULL,
  `roles` varchar(255) DEFAULT NULL,
  KEY `FKbpcy8fmdhrle2oepjtkhmjjls` (`Membre_id`),
  CONSTRAINT `FKbpcy8fmdhrle2oepjtkhmjjls` FOREIGN KEY (`Membre_id`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membre_roles`
--

LOCK TABLES `membre_roles` WRITE;
/*!40000 ALTER TABLE `membre_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `membre_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raid`
--

DROP TABLE IF EXISTS `raid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `raid` (
  `id` int(11) NOT NULL,
  `date` datetime(6) DEFAULT NULL,
  `roster_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKtimnnx3kynxsjooongle0kgx7` (`roster_id`),
  CONSTRAINT `FKtimnnx3kynxsjooongle0kgx7` FOREIGN KEY (`roster_id`) REFERENCES `roster` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raid`
--

LOCK TABLES `raid` WRITE;
/*!40000 ALTER TABLE `raid` DISABLE KEYS */;
/*!40000 ALTER TABLE `raid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roster`
--

DROP TABLE IF EXISTS `roster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roster` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `leader_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_tl11f7nevp19yqrral33su0uq` (`nom`),
  KEY `FKbc2m1lf8e9cu9o0fgt31ah3u3` (`leader_id`),
  CONSTRAINT `FKbc2m1lf8e9cu9o0fgt31ah3u3` FOREIGN KEY (`leader_id`) REFERENCES `utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roster`
--

LOCK TABLES `roster` WRITE;
/*!40000 ALTER TABLE `roster` DISABLE KEYS */;
/*!40000 ALTER TABLE `roster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stats`
--

DROP TABLE IF EXISTS `stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stats` (
  `id` int(11) NOT NULL,
  `attrib` varchar(255) DEFAULT NULL,
  `valeur` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stats`
--

LOCK TABLES `stats` WRITE;
/*!40000 ALTER TABLE `stats` DISABLE KEYS */;
INSERT INTO `stats` VALUES (1,'CriticalHit',227),(2,'Strenght',371),(3,'SkillSpeed',325),(4,'Vitality',419),(6,'Strenght',403),(7,'CriticalHit',249),(8,'Vitality',455),(9,'Determination',356),(11,'Vitality',377),(12,'CriticalHit',227),(13,'SpellSpeed',325),(14,'Mind',371),(16,'Piety',200),(17,'Vitality',232),(18,'SpellSpeed',140),(19,'Mind',228),(21,'Determination',200),(22,'Vitality',232),(23,'CriticalHit',140),(24,'Mind',228),(26,'Vitality',174),(27,'Mind',171),(28,'Determination',105),(29,'Piety',150),(31,'Vitality',377),(32,'Piety',227),(33,'CriticalHit',325),(34,'Mind',371),(36,'SpellSpeed',200),(37,'Determination',140),(38,'Vitality',232),(39,'Mind',228),(41,'SpellSpeed',150),(42,'CriticalHit',105),(43,'Mind',171),(45,'CriticalHit',150),(46,'Mind',171),(47,'Determination',105),(49,'CriticalHit',105),(50,'Mind',171),(51,'Determination',150),(53,'CriticalHit',105),(54,'Mind',171),(55,'Piety',150),(57,'Mind',399),(58,'Determination',350),(59,'SpellSpeed',245),(60,'Vitality',406),(62,'CriticalHit',200),(63,'Vitality',232),(64,'SpellSpeed',140),(65,'Mind',228),(67,'Vitality',377),(68,'Piety',227),(69,'Determination',325),(70,'Mind',371),(72,'SpellSpeed',200),(73,'Determination',140),(74,'Vitality',232),(75,'Mind',228),(77,'Vitality',174),(78,'CriticalHit',150),(79,'Mind',171),(80,'Determination',105),(82,'Vitality',377),(83,'Piety',227),(84,'SpellSpeed',325),(85,'Mind',371),(87,'Determination',200),(88,'Vitality',232),(89,'CriticalHit',140),(90,'Mind',228),(92,'CriticalHit',150),(93,'Mind',171),(94,'Piety',105),(96,'SpellSpeed',105),(97,'Mind',171),(98,'Determination',150),(100,'CriticalHit',105),(101,'Mind',171),(102,'Piety',150),(104,'SpellSpeed',150),(105,'Mind',171),(106,'Determination',105),(108,'Vitality',410),(109,'SpellSpeed',356),(110,'Piety',249),(111,'Mind',403),(113,'Vitality',410),(114,'SpellSpeed',249),(115,'CriticalHit',356),(116,'Mind',403),(118,'Mind',399),(119,'Piety',245),(120,'SpellSpeed',350),(121,'Vitality',406),(123,'Vitality',410),(124,'Piety',356),(125,'Determination',249),(126,'Mind',403),(128,'Mind',399),(129,'Piety',245),(130,'Vitality',406),(131,'CriticalHit',350),(133,'CriticalHit',200),(134,'Determination',140),(135,'Vitality',258),(136,'Strenght',228),(138,'Intelligence',171),(139,'CriticalHit',105),(140,'Vitality',174),(141,'Determination',150),(143,'SkillSpeed',325),(144,'CriticalHit',227),(145,'Vitality',419),(146,'Strenght',371),(148,'Tenacity',200),(149,'Determination',140),(150,'Vitality',258),(151,'Strenght',228),(153,'Intelligence',171),(154,'CriticalHit',150),(155,'Determination',105),(157,'Strenght',171),(158,'Tenacity',105),(159,'SkillSpeed',150),(160,'Vitality',193),(162,'Tenacity',325),(163,'SkillSpeed',227),(164,'Vitality',419),(165,'Strenght',371),(167,'Intelligence',171),(168,'DirectHit',105),(169,'Determination',150),(171,'Vitality',258),(172,'Tenacity',140),(173,'Determination',200),(174,'Strenght',228),(176,'Intelligence',171),(177,'DirectHit',150),(178,'SpellSpeed',105),(180,'Intelligence',171),(181,'CriticalHit',150),(182,'DirectHit',104),(184,'SkillSpeed',105),(185,'CriticalHit',150),(186,'Vitality',51),(187,'Strenght',193),(189,'Tenacity',105),(190,'Vitality',193),(191,'Strenght',51),(192,'Determination',150),(194,'SkillSpeed',105),(195,'Vitality',193),(196,'Strenght',51),(197,'Determination',150),(199,'DirectHit',200),(200,'Intelligence',228),(201,'Vitality',232),(202,'CriticalHit',140),(204,'CriticalHit',105),(205,'SkillSpeed',150),(206,'Vitality',193),(207,'Strenght',51),(209,'DirectHit',200),(210,'Intelligence',228),(211,'SpellSpeed',138),(212,'Vitality',232),(214,'Strenght',288),(215,'Determination',178),(216,'CriticalHit',254),(217,'Vitality',325),(219,'Determination',225),(220,'Vitality',377),(221,'CriticalHit',325),(222,'Intelligence',371),(224,'CriticalHit',102),(225,'Vitality',130),(226,'Determination',71),(227,'Strenght',115),(229,'CriticalHit',200),(230,'Intelligence',228),(231,'Vitality',230),(232,'SpellSpeed',140),(234,'Vitality',377),(235,'Intelligence',371),(236,'Determination',325),(237,'DirectHit',227),(239,'Strenght',285),(240,'Vitality',322),(241,'Determination',250),(242,'Tenacity',175),(244,'Tenacity',70),(245,'Vitality',129),(246,'Strenght',114),(247,'Determination',100),(249,'Vitality',410),(250,'CriticalHit',356),(251,'Intelligence',403),(252,'Determination',247),(254,'Strenght',399),(255,'CriticalHit',245),(256,'Vitality',451),(257,'Tenacity',350),(259,'Vitality',410),(260,'DirectHit',356),(261,'Intelligence',403),(262,'Determination',249),(264,'SkillSpeed',356),(265,'Determination',249),(266,'Strenght',403),(267,'Vitality',455),(269,'Vitality',410),(270,'CriticalHit',249),(271,'SpellSpeed',354),(272,'Intelligence',403),(274,'Strenght',399),(275,'SkillSpeed',245),(276,'Vitality',451),(277,'CriticalHit',350),(279,'Tenacity',200),(280,'Vitality',258),(281,'Strenght',228),(282,'CriticalHit',140),(284,'Intelligence',399),(285,'Vitality',406),(286,'CriticalHit',350),(287,'DirectHit',243),(289,'CriticalHit',325),(290,'Vitality',419),(291,'Strenght',371),(292,'Determination',227),(294,'Intelligence',171),(295,'SpellSpeed',148),(296,'DirectHit',105),(298,'SkillSpeed',200),(299,'Vitality',258),(300,'Tenacity',140),(301,'Strenght',228),(303,'Intelligence',171),(304,'CriticalHit',150),(305,'DirectHit',105),(307,'Strenght',171),(308,'CriticalHit',150),(309,'Tenacity',105),(310,'Vitality',193),(312,'SkillSpeed',227),(313,'CriticalHit',325),(314,'Vitality',419),(315,'Strenght',371),(317,'Vitality',377),(318,'Intelligence',371),(319,'Determination',325),(320,'DirectHit',227),(322,'Intelligence',171),(323,'SpellSpeed',105),(324,'Determination',150),(326,'SkillSpeed',200),(327,'Determination',140),(328,'Vitality',258),(329,'Strenght',228),(331,'SkillSpeed',105),(332,'Vitality',193),(333,'Strenght',51),(334,'Determination',150),(336,'SpellSpeed',200),(337,'Determination',140),(338,'Intelligence',228),(339,'Vitality',232),(341,'Tenacity',105),(342,'SkillSpeed',150),(343,'Vitality',193),(344,'Strenght',51),(346,'SpellSpeed',200),(347,'Determination',140),(348,'Intelligence',228),(349,'Vitality',232),(351,'CriticalHit',105),(352,'Vitality',193),(353,'Strenght',51),(354,'Determination',150),(356,'Intelligence',171),(357,'CriticalHit',105),(358,'Determination',149),(360,'Tenacity',150),(361,'Determination',105),(362,'Vitality',193),(363,'Strenght',51),(365,'CriticalHit',200),(366,'Determination',140),(367,'Intelligence',228),(368,'Vitality',232),(370,'Intelligence',171),(371,'Vitality',174),(372,'CriticalHit',150),(373,'DirectHit',105),(375,'Vitality',377),(376,'DirectHit',325),(377,'Intelligence',371),(378,'CriticalHit',227),(380,'Intelligence',399),(381,'Vitality',406),(382,'CriticalHit',350),(383,'Determination',245),(385,'Intelligence',399),(386,'DirectHit',350),(387,'Vitality',406),(388,'Determination',245),(390,'DirectHit',200),(391,'Determination',140),(392,'Vitality',258),(393,'Strenght',228),(395,'CriticalHit',200),(396,'Determination',140),(397,'Vitality',258),(398,'Strenght',228),(400,'Strenght',171),(401,'CriticalHit',150),(402,'DirectHit',105),(403,'Vitality',193),(405,'CriticalHit',227),(406,'Vitality',419),(407,'Strenght',371),(408,'Determination',325),(410,'Vitality',258),(411,'Determination',200),(412,'SkillSpeed',140),(413,'Strenght',228),(415,'Strenght',171),(416,'SkillSpeed',150),(417,'Determination',105),(419,'DirectHit',150),(420,'CriticalHit',105),(421,'Strenght',171),(423,'DirectHit',150),(424,'SkillSpeed',105),(425,'Strenght',171),(427,'Strenght',171),(428,'SkillSpeed',150),(429,'DirectHit',105),(431,'CriticalHit',249),(432,'Dexterity',403),(433,'Vitality',455),(434,'Determination',356),(436,'CriticalHit',249),(437,'Strenght',403),(438,'Vitality',455),(439,'Determination',356),(441,'Strenght',399),(442,'DirectHit',350),(443,'Vitality',451),(444,'Determination',245),(446,'SkillSpeed',150),(447,'DirectHit',105),(448,'Dexterity',171),(450,'CriticalHit',150),(451,'Determination',105),(452,'Dexterity',171),(454,'CriticalHit',150),(455,'DirectHit',105),(456,'Dexterity',171),(458,'SkillSpeed',150),(459,'DirectHit',105),(460,'Dexterity',171),(462,'CriticalHit',200),(463,'Dexterity',228),(464,'Determination',140),(465,'Vitality',258),(467,'Dexterity',371),(468,'CriticalHit',227),(469,'Vitality',419),(470,'Determination',325),(472,'SkillSpeed',200),(473,'Determination',140),(474,'Vitality',258),(475,'Strenght',228),(477,'CriticalHit',105),(478,'SkillSpeed',150),(479,'Vitality',193),(480,'Dexterity',171),(482,'CriticalHit',325),(483,'Vitality',419),(484,'Strenght',371),(485,'Determination',227),(487,'Vitality',258),(488,'DirectHit',140),(489,'Determination',200),(490,'Strenght',228),(492,'SkillSpeed',200),(493,'Dexterity',228),(494,'Determination',140),(495,'Vitality',258),(497,'SkillSpeed',105),(498,'Strenght',171),(499,'CriticalHit',150),(500,'Vitality',193),(502,'DirectHit',325),(503,'Dexterity',371),(504,'CriticalHit',227),(505,'Vitality',419),(507,'SkillSpeed',325),(508,'CriticalHit',227),(509,'Vitality',419),(510,'Strenght',371),(512,'SkillSpeed',200),(513,'Dexterity',228),(514,'Vitality',258),(515,'DirectHit',140),(517,'Vitality',258),(518,'DirectHit',140),(519,'Determination',200),(520,'Strenght',228),(522,'DirectHit',356),(523,'SkillSpeed',249),(524,'Dexterity',403),(525,'Vitality',455),(527,'CriticalHit',105),(528,'Strenght',171),(529,'SkillSpeed',150),(531,'DirectHit',150),(532,'Strenght',171),(533,'Determination',105),(535,'DirectHit',244),(536,'Dexterity',399),(537,'Determination',350),(538,'Vitality',451),(540,'Strenght',171),(541,'SkillSpeed',150),(542,'Determination',105),(544,'SkillSpeed',105),(545,'Strenght',171),(546,'CriticalHit',150),(548,'CriticalHit',200),(549,'Dexterity',228),(550,'Vitality',258),(551,'SkillSpeed',140),(553,'CriticalHit',200),(554,'Dexterity',228),(555,'Vitality',258),(556,'SkillSpeed',140),(558,'Strenght',399),(559,'DirectHit',245),(560,'Vitality',451),(561,'SkillSpeed',350),(563,'CriticalHit',150),(564,'Determination',105),(565,'Vitality',193),(566,'Dexterity',171),(568,'SkillSpeed',249),(569,'CriticalHit',356),(570,'Strenght',403),(571,'Vitality',455),(573,'Dexterity',371),(574,'Vitality',419),(575,'Determination',325),(576,'DirectHit',227),(578,'DirectHit',249),(579,'Strenght',403),(580,'Vitality',455),(581,'Determination',356),(583,'Strenght',399),(584,'Dexterity',451),(585,'SkillSpeed',245),(586,'CriticalHit',350),(588,'Dexterity',371),(589,'SkillSpeed',325),(590,'CriticalHit',227),(591,'Vitality',419),(593,'CriticalHit',200),(594,'Dexterity',228),(595,'Vitality',258),(596,'DirectHit',140),(598,'DirectHit',200),(599,'Vitality',258),(600,'SkillSpeed',140),(601,'Strenght',228),(603,'CriticalHit',150),(604,'Determination',105),(605,'Dexterity',171),(607,'CriticalHit',325),(608,'Vitality',419),(609,'Strenght',371),(610,'DirectHit',227),(612,'SkillSpeed',150),(613,'DirectHit',105),(614,'Dexterity',171),(616,'SkillSpeed',200),(617,'Vitality',258),(618,'Strenght',228),(619,'CriticalHit',140),(621,'SkillSpeed',150),(622,'Determination',105),(623,'Dexterity',171),(625,'Strenght',171),(626,'CriticalHit',150),(627,'Determination',105),(628,'Vitality',193),(630,'CriticalHit',150),(631,'DirectHit',105),(632,'Dexterity',171),(634,'CriticalHit',227),(635,'Vitality',419),(636,'Strenght',371),(637,'Determination',325),(639,'Dexterity',399),(640,'DirectHit',245),(641,'Vitality',451),(642,'SkillSpeed',350),(644,'Vitality',258),(645,'Determination',200),(646,'SkillSpeed',140),(647,'Strenght',228),(649,'Strenght',171),(650,'SkillSpeed',150),(651,'Determination',105),(653,'Dexterity',228),(654,'Vitality',258),(655,'Determination',200),(656,'CriticalHit',140),(658,'Strenght',171),(659,'SkillSpeed',150),(660,'Determination',105),(662,'Dexterity',371),(663,'Vitality',419),(664,'Determination',325),(665,'DirectHit',227),(667,'SkillSpeed',105),(668,'CriticalHit',150),(669,'Vitality',193),(670,'Dexterity',171),(672,'SkillSpeed',200),(673,'Vitality',258),(674,'Strenght',228),(675,'CriticalHit',140),(677,'DirectHit',325),(678,'Dexterity',371),(679,'CriticalHit',227),(680,'Vitality',419),(682,'SkillSpeed',227),(683,'Vitality',419),(684,'Strenght',371),(685,'Determination',325),(687,'CriticalHit',200),(688,'Dexterity',228),(689,'Determination',140),(690,'Vitality',258),(692,'DirectHit',200),(693,'Vitality',258),(694,'Strenght',228),(695,'CriticalHit',140),(697,'SkillSpeed',200),(698,'Dexterity',228),(699,'Determination',140),(700,'Vitality',258),(702,'SkillSpeed',105),(703,'Strenght',171),(704,'Vitality',193),(705,'Determination',150),(707,'DirectHit',356),(708,'Dexterity',403),(709,'Determination',249),(710,'Vitality',455),(712,'SkillSpeed',227),(713,'CriticalHit',325),(714,'Vitality',419),(715,'Strenght',371),(717,'Dexterity',399),(718,'Vitality',451),(719,'CriticalHit',350),(720,'Determination',245),(722,'DirectHit',200),(723,'Vitality',258),(724,'Strenght',228),(725,'CriticalHit',140),(727,'Dexterity',228),(728,'Vitality',258),(729,'Determination',200),(730,'CriticalHit',140),(732,'SkillSpeed',200),(733,'Dexterity',228),(734,'Vitality',258),(735,'DirectHit',140),(737,'Dexterity',371),(738,'CriticalHit',325),(739,'Vitality',419),(740,'Determination',227),(742,'DirectHit',150),(743,'Determination',105),(744,'Vitality',193),(745,'Dexterity',171),(747,'Dexterity',371),(748,'CriticalHit',227),(749,'Vitality',419),(750,'Determination',325),(752,'Dexterity',228),(753,'Vitality',258),(754,'DirectHit',140),(755,'Determination',200);
/*!40000 ALTER TABLE `stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `utilisateur` (
  `DTYPE` varchar(31) NOT NULL,
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `serveur` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `listeVoeux_id` int(11) DEFAULT NULL,
  `roster_id` int(11) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_66vu1vfh4m2fw682xmd4lobqy` (`username`),
  KEY `FKebmeheu7ivyw25r07v9uivcvu` (`listeVoeux_id`),
  KEY `FKbf2ps0op9i0r0xnfjtecxlm32` (`roster_id`),
  CONSTRAINT `FKbf2ps0op9i0r0xnfjtecxlm32` FOREIGN KEY (`roster_id`) REFERENCES `roster` (`id`),
  CONSTRAINT `FKebmeheu7ivyw25r07v9uivcvu` FOREIGN KEY (`listeVoeux_id`) REFERENCES `listevoeux` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-29 19:56:12
