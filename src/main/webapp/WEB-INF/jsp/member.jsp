<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="img/favicon.ico" />
<title>Orion - Member</title>

	<!-- JQuery -->
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Popper -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<!-- FontAwesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

	<!-- Custom CSS & JS -->
	<link rel="stylesheet" type="text/css" href="css/main.css">	
	<script src="js/profile.js"></script>
	<script>var ctx = "${pageContext.request.contextPath}"</script>

</head>
<body>


		<!-- Header ---------------------------------------------------------->
		<%@ include file="components/header.jsp" %>


		<!-- Sidebar ------------------------------------------------------------>
		<%@ include file="components/sidebar.jsp" %>


		<!-- Content --------------------------------------------------------->
		<div class="content">
			<div class="content-content">	
				<div class="roster-content-content">
					<div class="member-top">
						<div class="member-top-img">
							<img src="${member.img}" style="width: 115px;">
						</div>			
						<div class="member-top-infos">
							<div class="member-top-infos-name">
								${member.prenom} ${member.nom} 
							</div>
							<div class="member-top-infos-roster">
								<c:if test="${!empty member.roster}">
									< ${member.roster.nom} >
								</c:if><br>
							</div>
							<div class="member-top-infos-icons">
								Jobs : 
								<c:forEach items="${member.jobs}" var="job">
									<img src="img/icon/${job}.png" style="width: 30px;">
								</c:forEach>
							</div>
							<div class="member-top-infos-icons">
								Roles : 
								<c:forEach items="${member.roles}" var="role">
									<img src="img/icon/${role}.png" style="width: 30px; opacity: 0.65;">
								</c:forEach>
							</div>
						</div>	
					</div>
					
					<div class="member-tabs">
						<div class="member-tab-wl"}>
							<div class="member-tab-head">
								Wishlist
							</div>
							<c:forEach items="${member.listeVoeux.items}" var="item">
								<div class="member-tab-row" data-name="${item.id}">
									<div class="member-tab-row-img">
										<img src="${item.img}"  style="height: 48px;">
									</div>
									<div class="member-tab-row-name">
										${item.nom}
									</div>
								</div>
							</c:forEach>						
						</div>
						
						<div class="member-tab-none">&nbsp</div>
						
						<div class="member-tab-loot">
							<div class="member-tab-head">
								Loot
							</div>		
							<c:forEach items="${itemLoot}" var="item">
								<div class="member-tab-row" data-name="${item.id}">
									<div class="member-tab-row-img">
										<img src="${item.img}"  style="height: 48px;">
									</div>
									<div class="member-tab-row-name">
										${item.nom}
									</div>
								</div>
							</c:forEach>
						
						
						</div>
					
					
					
					</div>
				</div>				
			</div>
		</div>


		<!-- Footer ---------------------------------------------------------->
		<%@ include file="components/footer.jsp" %>


</body>
</html>