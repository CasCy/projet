<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="img/favicon.ico" />
<title>Orion - Edit Member Info</title>

	<!-- JQuery -->
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Popper -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<!-- FontAwesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

	<!-- Custom CSS & JS -->
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script>var ctx = "${pageContext.request.contextPath}"</script>

</head>
<body>


		<!-- Header ---------------------------------------------------------->
		<%@ include file="components/header.jsp" %>


		<!-- Menu ------------------------------------------------------------>
		<%@ include file="components/sidebar.jsp" %>


		<!-- Content --------------------------------------------------------->
		<div class="content">
			<form action="editmember" method="POST" class="form-group">
			
				<%@ include file="components/editCharInfo.jsp" %>
		
				<div class="text-center">
					<input type="reset" class="btn btn-secondary" value="Reset"/>
					<input type="submit" id="submit" class="btn btn-success" value="Submit"/>
				</div>
			</form>
		</div>


		<!-- Footer ---------------------------------------------------------->
		<%@ include file="components/footer.jsp" %>


</body>
</html>