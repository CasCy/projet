<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


    
    <div class="header">
		<div class="menu">
			<div class="menu-left">
				<c:if test="${user.id eq 757 && !empty user}">
					<a href=newitem class="btn btn-outline-dark">
						<span class="fas fa-user-edit mr-1"></span>
						ADMIN
					</a>
				</c:if>
			</div>
			<div class="menu-center">
				
				<c:if test="${!empty user.roster}">
					<div class="dropdown">
						<button class="btn btn-outline-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="fas fa-user-plus mr-1"></span>
							Roster ${user.roster.nom}
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">					
														
							<a href=viewroster class="btn btn-outline-light dropdown-item">
								<span class="fas fa-eye mr-1"></span>
								View Roster 
							</a>	
							
							<a href=viewraids class="btn btn-outline-light dropdown-item">
								<span class="fab fa-optin-monster mr-1"></span>
								View Raids 
							</a>			
								
							<c:if test="${user.id eq user.roster.leader.id}">						
							<a href=addmemberroster class="btn btn-outline-light dropdown-item">
								<span class="fas fa-user-plus mr-1"></span>
								Add Member
							</a>
							</c:if>					
									
							<c:if test="${user.id eq user.roster.leader.id}">					
							<a href=addraidroster class="btn btn-outline-light dropdown-item">
								<span class="fas fa-calendar-plus mr-1"></span>
								Add Raid
							</a>
							</c:if>					
									
							<c:if test="${user.id eq user.roster.leader.id}">					
							<a href=rostermemberkick class="btn btn-outline-light dropdown-item">
								<span class="fas fa-user-alt-slash"></span>
								Kick Member
							</a>
							</c:if>					
									
							<c:if test="${user.id eq user.roster.leader.id}">					
							<a href=rosterleadswap class="btn btn-outline-light dropdown-item">
								<span class="fas fa-exchange-alt"></span>
								Give Lead
							</a>
							</c:if>					
										
							<c:if test="${user.id ne user.roster.leader.id}">				
							<a href=leaveroster class="btn btn-outline-light dropdown-item">
								<span class="fas fa-sign-out-alt mr-1"></span>
								Leave Roster
							</a>
							</c:if>
							
						</div>
					</div>
				</c:if>
				
			</div>
			<div class="menu-right">
				<c:if test="${!empty user}">
					<div class="dropdown">
						<button class="btn btn-outline-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="fas fa-user-plus mr-1"></span>
							${user.prenom} ${user.nom}
						</button>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">					
							<a href="editmember" class="btn btn-outline-light dropdown-item">
								<span class="fas fa-feather-alt mr-1"></span>
								Edit Character
							</a>
							
							<a href=editpassword class="btn btn-outline-light dropdown-item">
								<span class="fas fa-key mr-1"></span>
								Edit Password
							</a>
														
							<a href=profile class="btn btn-outline-light dropdown-item">
								<span class="far fa-address-card mr-1"></span>
								My Character
							</a>
														
							<a href=viewWL class="btn btn-outline-light dropdown-item">
								<span class="fas fa-piggy-bank mr-1"></span>
								View Wishlist
							</a>
							
							<c:if test="${empty user.prios}">								
							<a href=priolist class="btn btn-outline-light dropdown-item">
								<span class="fas fa-list-ol mr-1"></span>
								Create Prio List
							</a>
							</c:if>	
							
							<c:if test="${empty user.roster}">								
							<a href=newroster class="btn btn-outline-light dropdown-item">
								<span class="fas fa-users mr-1"></span>
								Create Roster
							</a>
							</c:if>							
							
							<a href=logout class="btn btn-outline-light dropdown-item">
								<span class="fas fa-sign-out-alt mr-1"></span>
								Logout
							</a>
						</div>
					</div>
				</c:if>
				<c:if test="${empty user}">
				<a href="newuser" class="btn btn-outline-dark">
					<span class="fas fa-user-plus mr-1"></span>
					NEW
				</a>
				&nbsp&nbsp
				<a href=login class="btn btn-outline-dark">
					<span class="fas fa-sign-in-alt mr-1"></span>
					LOGIN
				</a>
				</c:if>
			</div>
		</div>
	</div>
		