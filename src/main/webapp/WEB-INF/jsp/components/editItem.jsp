<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="js/selectorplus.js"></script>


<!-- <script src="js/validation.js"></script> -->








<fieldset>
	<legend>
		<i class="fas fa-shield-alt"></i> Item Editor
	</legend>

	<div style="color: red;">${errorMessage}</div>
	<div style="color: red;">${offline}</div>
	<div style="color: green;">${itemSuccess}</div>
	<br> <label for="item name">Item name: </label> <input type="text"
		id="name" name="name" class="form-control" value="${editedItem.nom}" />
	<div style="color: red; display: none"></div>
	<br> <label for="ilvl">Item Level:</label> 
	<input type="number" name="ilvl" class="form-control" value="${editedItem.ilvl}" />
	<div style="color: red; display: none"></div>
	<br> <label for="physick">Physical ATT/DEF :</label> 
	<input type="number" name="physick" class="form-control" value="${editedItem.physic}" />
	<div style="color: red; display: none"></div>
	<br> <label for="magic">Magical ATT/DEF :</label> 
	<input type="number" name="magic" class="form-control" value="${editedItem.magic}" />
	<div style="color: red; display: none"></div>
	<br><label for="item name">Repair: </label> 
	
	<select name="repair">
			<option value="Alchemist">Alchemist</option>
			<option value="Armorer">Armorer</option>
			<option value="Blacksmith">Blacksmith</option>
			<option value="Carpenter">Carpenter</option>
			<option value="Culinarian">Culinarian</option>
			<option value="Goldsmith">Goldsmith</option>
			<option value="Leatherworker">Leatherworker</option>
			<option value="Weaver">Weaver</option>
		</select>
		
	<br><label for="item name">Slot: </label> 
		
		<select name="slot">

		<c:if test="${editedItem.slot eq 'Weapon'}">
			<option value="Weapon" selected>Weapon</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Weapon'}">
			<option value="Weapon">Weapon</option>
		</c:if>

		<c:if test="${editedItem.slot eq 'Shield'}">
			<option value="Shield" selected>Shield</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Shield'}">
			<option value="Shield">Shield</option>
		</c:if>

		<c:if test="${editedItem.slot eq 'Head'}">
			<option value="Head" selected>Head</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Head'}">
			<option value="Head">Head</option>
		</c:if>

		<c:if test="${editedItem.slot eq 'Body'}">
			<option value="Body" selected>Body</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Body'}">
			<option value="Body">Body</option>
		</c:if>

		<c:if test="${editedItem.slot eq 'Hands'}">
			<option value="Hands" selected>Hands</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Hands'}">
			<option value="Hands">Hands</option>
		</c:if>

		<c:if test="${editedItem.slot eq 'Waist'}">
			<option value="Waist" selected>Waist</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Waist'}">
			<option value="Waist">Waist</option>
		</c:if>

		<c:if test="${editedItem.slot eq 'Legs'}">
			<option value="Legs" selected>Legs</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Legs'}">
			<option value="Legs">Legs</option>
		</c:if>

		<c:if test="${editedItem.slot eq 'Feet'}">
			<option value="Feet" selected>Feet</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Feet'}">
			<option value="Feet">Feet</option>
		</c:if>

		<c:if test="${editedItem.slot eq 'Earrings'}">
			<option value="Earrings" selected>Earrings</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Earrings'}">
			<option value="Earrings">Earrings</option>
		</c:if>

		<c:if test="${editedItem.slot eq 'Necklace'}">
			<option value="Necklace" selected>Necklace</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Necklace'}">
			<option value="Necklace">Necklace</option>
		</c:if>

		<c:if test="${editedItem.slot eq 'Wirst'}">
			<option value="Wirst" selected>Wirst</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Wirst'}">
			<option value="Wirst">Wirst</option>
		</c:if>

		<c:if test="${editedItem.slot eq 'Ring'}">
			<option value="Ring" selected>Ring</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Ring'}">
			<option value="Ring">Ring</option>
		</c:if>

	</select><br><br><br>
	<label for="item name">Source: </label> 
	<select name="source">

			<option value="None" ></option>

		<c:if test="${editedItem.source eq 'Vendor'}">
			<option value="Vendor" selected>Vendor</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'Vendor'}">
			<option value="Vendor">Vendor</option>
		</c:if>

		<c:if test="${editedItem.source eq 'O1S'}">
			<option value="O1S" selected>O1S</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'O1S'}">
			<option value="O1S">O1S</option>
		</c:if>

		<c:if test="${editedItem.source eq 'O2S'}">
			<option value="O2S" selected>O2S</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'O2S'}">
			<option value="O2S">O2S</option>
		</c:if>

		<c:if test="${editedItem.source eq 'O3S'}">
			<option value="O3S" selected>O3S</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'O3S'}">
			<option value="O3S">O3S</option>
		</c:if>

		<c:if test="${editedItem.source eq 'O4S'}">
			<option value="O4S" selected>O4S</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'O4S'}">
			<option value="O4S">O4S</option>
		</c:if>

		<c:if test="${editedItem.source eq 'O5S'}">
			<option value="O5S" selected>O5S</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'O5S'}">
			<option value="O5S">O5S</option>
		</c:if>

		<c:if test="${editedItem.source eq 'O6S'}">
			<option value="O6S" selected>O6S</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'O6S'}">
			<option value="O6S">O6S</option>
		</c:if>

		<c:if test="${editedItem.source eq 'O7S'}">
			<option value="O7S" selected>O7S</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'O7S'}">
			<option value="O7S">O7S</option>
		</c:if>

		<c:if test="${editedItem.source eq 'O8S'}">
			<option value="O8S" selected>O8S</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'O8S'}">
			<option value="O8S">O8S</option>
		</c:if>

		<c:if test="${editedItem.source eq 'O9S'}">
			<option value="O9S" selected>O9S</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'O9S'}">
			<option value="O9S">O9S</option>
		</c:if>

		<c:if test="${editedItem.source eq 'O10S'}">
			<option value="O10S" selected>O10S</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'O10S'}">
			<option value="O10S">O10S</option>
		</c:if>

		<c:if test="${editedItem.source eq 'O11S'}">
			<option value="O11S" selected>O11S</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'O11S'}">
			<option value="O11S">O11S</option>
		</c:if>

		<c:if test="${editedItem.source eq 'O12S'}">
			<option value="O12S" selected>O12S</option>
		</c:if>
		<c:if test="${editedItem.slot ne 'O12S'}">
			<option value="O12S">O12S</option>
		</c:if>

	</select><br>
	
	<br>
	<br>
	<div class="edit-jobs-roles">
		<div class="form-inline">
			<div class="edit-jobs-list">
				<label for="jobs">Jobs :</label><br>
				<c:if test="${!empty editedItem.jobs}">
					<c:forEach items="${editedItem.jobs}" var="job">
						<div class="edit-jobs">
							<div class="job-select">
								<select name="job">

									<option value="None"></option>

									<c:if test="${job eq 'Paladin'}">
										<option value="Paladin" selected>Paladin</option>
									</c:if>
									<c:if test="${job ne 'Paladin'}">
										<option value="Paladin">Paladin</option>
									</c:if>

									<c:if test="${job eq 'DarkKnight'}">
										<option value="DarkKnight" selected>DarkKnight</option>
									</c:if>
									<c:if test="${job ne 'DarkKnight'}">
										<option value="DarkKnight">DarkKnight</option>
									</c:if>

									<c:if test="${job eq 'Gunbreaker'}">
										<option value="Gunbreaker" selected>Gunbreaker</option>
									</c:if>
									<c:if test="${job ne 'Gunbreaker'}">
										<option value="Gunbreaker">Gunbreaker</option>
									</c:if>

									<c:if test="${job eq 'Warrior'}">
										<option value="Warrior" selected>Warrior</option>
									</c:if>
									<c:if test="${job ne 'Warrior'}">
										<option value="Warrior">Warrior</option>
									</c:if>

									<c:if test="${job eq 'WhiteMage'}">
										<option value="WhiteMage" selected>WhiteMage</option>
									</c:if>
									<c:if test="${job ne 'WhiteMage'}">
										<option value="WhiteMage">WhiteMage</option>
									</c:if>

									<c:if test="${job eq 'Astrologian'}">
										<option value="Astrologian" selected>Astrologian</option>
									</c:if>
									<c:if test="${job ne 'Astrologian'}">
										<option value="Astrologian">Astrologian</option>
									</c:if>

									<c:if test="${job eq 'Scholar'}">
										<option value="Scholar" selected>Scholar</option>
									</c:if>
									<c:if test="${job ne 'Scholar'}">
										<option value="Scholar">Scholar</option>
									</c:if>

									<c:if test="${job eq 'Bard'}">
										<option value="Bard" selected>Bard</option>
									</c:if>
									<c:if test="${job ne 'Bard'}">
										<option value="Bard">Bard</option>
									</c:if>

									<c:if test="${job eq 'Machinist'}">
										<option value="Machinist" selected>Machinist</option>
									</c:if>
									<c:if test="${job ne 'Machinist'}">
										<option value="Machinist">Machinist</option>
									</c:if>

									<c:if test="${job eq 'Dancer'}">
										<option value="Dancer" selected>Dancer</option>
									</c:if>
									<c:if test="${job ne 'Dancer'}">
										<option value="Dancer">Dancer</option>
									</c:if>

									<c:if test="${job eq 'BlackMage'}">
										<option value="BlackMage" selected>BlackMage</option>
									</c:if>
									<c:if test="${job ne 'BlackMage'}">
										<option value="BlackMage">BlackMage</option>
									</c:if>

									<c:if test="${job eq 'Summoner'}">
										<option value="Summoner" selected>Summoner</option>
									</c:if>
									<c:if test="${job ne 'Summoner'}">
										<option value="Summoner">Summoner</option>
									</c:if>

									<c:if test="${job eq 'RedMage'}">
										<option value="RedMage" selected>Redmage</option>
									</c:if>
									<c:if test="${job ne 'RedMage'}">
										<option value="RedMage">Redmage</option>
									</c:if>

									<c:if test="${job eq 'Ninja'}">
										<option value="Ninja" selected>Ninja</option>
									</c:if>
									<c:if test="${job ne 'Ninja'}">
										<option value="Ninja">Ninja</option>
									</c:if>

									<c:if test="${job eq 'Dragoon'}">
										<option value="Dragoon" selected>Dragoon</option>
									</c:if>
									<c:if test="${job ne 'Dragoon'}">
										<option value="Dragoon">Dragoon</option>
									</c:if>

									<c:if test="${job eq 'Monk'}">
										<option value="Monk" selected>Monk</option>
									</c:if>
									<c:if test="${job ne 'Monk'}">
										<option value="Monk">Monk</option>
									</c:if>

									<c:if test="${job eq 'Samurai'}">
										<option value="Samurai" selected>Samurai</option>
									</c:if>
									<c:if test="${job ne 'Dragoon'}">
										<option value="Samurai">Samurai</option>
									</c:if>

								</select>&nbsp&nbsp <i id="trash" class="fas fa-trash-alt"></i>
							</div>
						</div>
					</c:forEach>
				</c:if>

				<div class="edit-jobs">
					<div class="job-select">
						<select name="job">
							<option value="None" default></option>
							<option value="Paladin">Paladin</option>
							<option value="DarkKnight">Dark Knight</option>
							<option value="Gunbreaker">Gunbreaker</option>
							<option value="Warrior">Warrior</option>
							<option value="WhiteMage">White Mage</option>
							<option value="Astrologian">Astrologian</option>
							<option value="Scholar">Scholar</option>
							<option value="Bard">Bard</option>
							<option value="Machinist">Machinist</option>
							<option value="Dancer">Dancer</option>
							<option value="BlackMage">Black Mage</option>
							<option value="Summoner">Summoner</option>
							<option value="RedMage">Red Mage</option>
							<option value="Ninja">Ninja</option>
							<option value="Dragoon">Dragoon</option>
							<option value="Monk">Monk</option>
							<option value="Samurai">Samurai</option>
						</select>&nbsp&nbsp <i class="fas fa-plus"></i>
					</div>
				</div>

			</div>

		</div>

	</div>

	<br>
	<br>

	<div class="edit-stats">
		<label for="stats">Stats - Values :</label><br>
		<c:if test="${!empty editedItem.stats}">
		<c:forEach items="${editedItem.stats}" var="stat">
		<div class="form-inline">
			<div>
				<select name="stat">
					<option value="None"></option>
							<c:if test="${stat.attrib eq 'Strenght'}">
								<option value="Strenght" selected>Strenght</option>
							</c:if>
							<c:if test="${stat.attrib ne 'Strenght'}">
								<option value="Strenght">Strenght</option>
							</c:if>

							<c:if test="${stat.attrib eq 'Dexterity'}">
								<option value="Dexterity" selected>Dexterity</option>
							</c:if>
							<c:if test="${stat.attrib ne 'Dexterity'}">
								<option value="Dexterity">Dexterity</option>
							</c:if>

							<c:if test="${stat.attrib eq 'Vitality'}">
								<option value="Vitality" selected>Vitality</option>
							</c:if>
							<c:if test="${stat.attrib ne 'Vitality'}">
								<option value="Vitality">Vitality</option>
							</c:if>

							<c:if test="${stat.attrib eq 'Intelligence'}">
								<option value="Intelligence" selected>Intelligence</option>
							</c:if>
							<c:if test="${stat.attrib ne 'Intelligence'}">
								<option value="Intelligence">Intelligence</option>
							</c:if>

							<c:if test="${stat.attrib eq 'Mind'}">
								<option value="Mind" selected>Mind</option>
							</c:if>
							<c:if test="${stat.attrib ne 'Mind'}">
								<option value="Mind">Mind</option>
							</c:if>

							<c:if test="${stat.attrib eq 'CriticalHit'}">
								<option value="CriticalHit" selected>CriticalHit</option>
							</c:if>
							<c:if test="${stat.attrib ne 'CriticalHit'}">
								<option value="CriticalHit">CriticalHit</option>
							</c:if>

							<c:if test="${stat.attrib eq 'DirectHit'}">
								<option value="DirectHit" selected>DirectHit</option>
							</c:if>
							<c:if test="${stat.attrib ne 'DirectHit'}">
								<option value="DirectHit">DirectHit</option>
							</c:if>

							<c:if test="${stat.attrib eq 'Determination'}">
								<option value="Determination" selected>Determination</option>
							</c:if>
							<c:if test="${stat.attrib ne 'Determination'}">
								<option value="Determination">Determination</option>
							</c:if>

							<c:if test="${stat.attrib eq 'SkillSpeed'}">
								<option value="SkillSpeed" selected>SkillSpeed</option>
							</c:if>
							<c:if test="${stat.attrib ne 'SkillSpeed'}">
								<option value="SkillSpeed">SkillSpeed</option>
							</c:if>

							<c:if test="${stat.attrib eq 'SpellSpeed'}">
								<option value="SpellSpeed" selected>SpellSpeed</option>
							</c:if>
							<c:if test="${stat.attrib ne 'SpellSpeed'}">
								<option value="SpellSpeed">SpellSpeed</option>
							</c:if>

							<c:if test="${stat.attrib eq 'Tenacity'}">
								<option value="Tenacity" selected>Tenacity</option>
							</c:if>
							<c:if test="${stat.attrib ne 'Tenacity'}">
								<option value="Tenacity">Tenacity</option>
							</c:if>

							<c:if test="${stat.attrib eq 'Piety'}">
								<option value="Piety" selected>Piety</option>
							</c:if>
							<c:if test="${stat.attrib ne 'Piety'}">
								<option value="Piety">Piety</option>
							</c:if>

					</select>
				</div>
				<div>
					<input type="number" name="value" class="form-control" value="${stat.valeur}" min="0" />
					<div style="color: red; display: none"></div>&&nbsp&nbsp 
					<i id="trash" class="fas fa-trash-alt"></i><br>
				</div>
			</div>				
			</c:forEach>
			</c:if>

			<div class="form-inline">
				<div>
					<select name="stat">
						<option value="None" selected></option>
						<option value="Strenght">Strenght</option>
						<option value="Dexterity">Dexterity</option>
						<option value="Vitality">Vitality</option>
						<option value="Intelligence">Intelligence</option>
						<option value="Mind">Mind</option>
						<option value="CriticalHit">CriticalHit</option>
						<option value="DirectHit">DirectHit</option>
						<option value="Determination">Determination</option>
						<option value="SkillSpeed">SkillSpeed</option>
						<option value="SpellSpeed">SpellSpeed</option>
						<option value="Tenacity">Tenacity</option>
						<option value="Piety">Piety</option>
					</select>
				</div>
				
				<div>
					<input type="number" name="value" class="form-control" value="${stat.valeur}" min="0" />
					<div style="color: red; display: none"></div>&nbsp&nbsp 
					<i class="fas fa-plus"></i><br>
				</div>
			</div>


	</div><br>	
	<br> <label for="img">Image URL: </label> 
	<input type="text" id="img" name="img" class="form-control" value="${editedItem.img}" />
	<div style="color: red; display: none"></div>







</fieldset>