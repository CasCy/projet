<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<script src="js/selectorplus.js"></script>
	


		
				
			


				<fieldset>
					<legend><i class="fas fa-feather-alt mr-1"></i> Character info</legend>
					
					<div style="color:red;">${errorMessage}</div>						
					<div style="color:red;">${offline}</div> 				
					<div style="color:green;">${charInfoSuccess}</div><br>
					
					<label for="prenom">Firstname: </label> 
					<input type="text" id="prenom" name="prenom" class="form-control" value="${editedUser.prenom}" />
					<div style=" color:red; display:none"></div><br>
					<label for="nom">Lastname:</label>
					<input type="text" name="nom" class="form-control" value="${editedUser.nom}" />
					<div style=" color:red; display:none"></div><br>
					<label for="nom">Server:</label>
					<div style=" color:red; display:none"></div><br>
					<select name="serveur">
					
						<c:if test="${editedUser.serveur eq 'Cerberus'}">
					    <option value="Cerberus" selected>&nbsp Cerberus</option>
					    </c:if>
					    <c:if test="${editedUser.serveur ne 'Cerberus'}">
					    <option value="Cerberus">&nbsp Cerberus</option>
					    </c:if>
					
						<c:if test="${editedUser.serveur eq 'Louisoix'}">
					    <option value="Louisoix" selected>&nbsp Louisoix</option>
					    </c:if>
					    <c:if test="${editedUser.serveur ne 'Louisoix'}">
					    <option value="Louisoix">&nbsp Louisoix</option>
					    </c:if>
					
						<c:if test="${editedUser.serveur eq 'Moogle'}">
					    <option value="Moogle" selected>&nbsp Moogle</option>
					    </c:if>
					    <c:if test="${editedUser.serveur ne 'Moogle'}">
					    <option value="Moogle">&nbsp Moogle</option>
					    </c:if>
					
						<c:if test="${editedUser.serveur eq 'Omega'}">
					    <option value="Omega" selected>&nbsp Omega</option>
					    </c:if>
					    <c:if test="${editedUser.serveur ne 'Omega'}">
					    <option value="Omega">&nbsp Omega</option>
					    </c:if>
					
						<c:if test="${editedUser.serveur eq 'Ragnarok'}">
					    <option value="Ragnarok" selected>&nbsp Ragnarok</option>
					    </c:if>
					    <c:if test="${editedUser.serveur ne 'Ragnarok'}">
					    <option value="Ragnarok">&nbsp Ragnarok</option>
					    </c:if>
					    
					</select><br><br><br>
					<div class="edit-jobs-roles">					
						<div class="form-inline">
							<div class="edit-jobs-list">
								<label for="jobs">Jobs :</label><br>
								<c:if test="${!empty editedUser.jobs}">
								<c:forEach items="${editedUser.jobs}" var="job">						
								<div class="edit-jobs">	
									<div class="job-select">
 											 <select name="job">
					    						
					    						<c:if test="${job eq 'Paladin'}">
					    						<option value="None"></option>
					    						<option value="Paladin" selected>&nbsp Paladin</option>
					    						</c:if>
					    						<c:if test="${job ne 'Paladin'}">
					    						<option value="None"></option>
					    						<option value="Paladin">&nbsp Paladin</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'DarkKnight'}">
					    						<option value="DarkKnight" selected>&nbsp DarkKnight</option>
					    						</c:if>
					    						<c:if test="${job ne 'DarkKnight'}">
					    						<option value="DarkKnight">&nbsp DarkKnight</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'Gunbreaker'}">
					    						<option value="Gunbreaker" selected>&nbsp Gunbreaker</option>
					    						</c:if>
					    						<c:if test="${job ne 'Gunbreaker'}">
					    						<option value="Gunbreaker">&nbsp Gunbreaker</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'Warrior'}">
					    						<option value="Warrior" selected>&nbsp Warrior</option>
					    						</c:if>
					    						<c:if test="${job ne 'Warrior'}">
					    						<option value="Warrior">&nbsp Warrior</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'WhiteMage'}">
					    						<option value="WhiteMage" selected>&nbsp WhiteMage</option>
					    						</c:if>
					    						<c:if test="${job ne 'WhiteMage'}">
					    						<option value="WhiteMage">&nbsp WhiteMage</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'Astrologian'}">
					    						<option value="Astrologian" selected>&nbsp Astrologian</option>
					    						</c:if>
					    						<c:if test="${job ne 'Astrologian'}">
					    						<option value="Astrologian">&nbsp Astrologian</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'Scholar'}">
					    						<option value="Scholar" selected>&nbsp Scholar</option>
					    						</c:if>
					    						<c:if test="${job ne 'Scholar'}">
					    						<option value="Scholar">&nbsp Scholar</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'Bard'}">
					    						<option value="Bard" selected>&nbsp Bard</option>
					    						</c:if>
					    						<c:if test="${job ne 'Bard'}">
					    						<option value="Bard">&nbsp Bard</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'Machinist'}">
					    						<option value="Machinist" selected>&nbsp Machinist</option>
					    						</c:if>
					    						<c:if test="${job ne 'Machinist'}">
					    						<option value="Machinist">&nbsp Machinist</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'Dancer'}">
					    						<option value="Dancer" selected>&nbsp Dancer</option>
					    						</c:if>
					    						<c:if test="${job ne 'Dancer'}">
					    						<option value="Dancer">&nbsp Dancer</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'BlackMage'}">
					    						<option value="BlackMage" selected>&nbsp BlackMage</option>
					    						</c:if>
					    						<c:if test="${job ne 'BlackMage'}">
					    						<option value="BlackMage">&nbsp BlackMage</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'Summoner'}">
					    						<option value="Summoner" selected>&nbsp Summoner</option>
					    						</c:if>
					    						<c:if test="${job ne 'Summoner'}">
					    						<option value="Summoner">&nbsp Summoner</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'RedMage'}">
					    						<option value="RedMage" selected>&nbsp Redmage</option>
					    						</c:if>
					    						<c:if test="${job ne 'RedMage'}">
					    						<option value="RedMage">&nbsp Redmage</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'Ninja'}">
					    						<option value="Ninja" selected>&nbsp Ninja</option>
					    						</c:if>
					    						<c:if test="${job ne 'Ninja'}">
					    						<option value="Ninja">&nbsp Ninja</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'Dragoon'}">
					    						<option value="Dragoon" selected>&nbsp Dragoon</option>
					    						</c:if>
					    						<c:if test="${job ne 'Dragoon'}">
					    						<option value="Dragoon">&nbsp Dragoon</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'Monk'}">
					    						<option value="Monk" selected>&nbsp Monk</option>
					    						</c:if>
					    						<c:if test="${job ne 'Monk'}">
					    						<option value="Monk">&nbsp Monk</option>
					    						</c:if>
					    						
					    						<c:if test="${job eq 'Samurai'}">
					    						<option value="Samurai" selected>&nbsp Samurai</option>
					    						</c:if>
					    						<c:if test="${job ne 'Dragoon'}">
					    						<option value="Samurai">&nbsp Samurai</option>
					    						</c:if>
					    						
											</select>&nbsp&nbsp
											<i id="trash" class="fas fa-trash-alt"></i>
									</div> 
								</div>	
								</c:forEach>
								</c:if>
								
								<div class="edit-jobs">	
									<div class="job-select">
 											 <select name="job">
					    						<option value="None" default></option>
					    						<option value="Paladin">&nbsp Paladin</option>
					    						<option value="DarkKnight">&nbsp Dark Knight</option>
    											<option value="Gunbreaker">&nbsp Gunbreaker</option>
    											<option value="Warrior">&nbsp Warrior</option>
 				   								<option value="WhiteMage">&nbsp White Mage</option>
    											<option value="Astrologian">&nbsp Astrologian</option>
    											<option value="Scholar">&nbsp Scholar</option>
    											<option value="Bard">&nbsp Bard</option>
    											<option value="Machinist">&nbsp Machinist</option>
    											<option value="Dancer">&nbsp Dancer</option>
    											<option value="BlackMage">&nbsp Black Mage</option>
    											<option value="Summoner">&nbsp Summoner</option>
    											<option value="RedMage">&nbsp Red Mage</option>
    											<option value="Ninja">&nbsp Ninja</option>
    											<option value="Dragoon">&nbsp Dragoon</option>
    											<option value="Monk">&nbsp Monk</option>
    											<option value="Samurai">&nbsp Samurai</option>
											</select>&nbsp&nbsp
											<i class="fas fa-plus"></i>
									</div> 
								</div>	
																					
							</div>
							
							<div class="edit-roles-list">
								<label for="roles">Roles :</label><br>
								
								<c:if test="${!empty editedUser.roles}">
								<c:forEach items="${editedUser.roles}" var="role">	
									<div class="edit-roles">	
										<div class="role-select">		
 												<select name="role">
 												
						    					<c:if test="${role eq 'Tank'}">
					    						<option value="Tank" selected>&nbsp Tank</option>
					    						</c:if>
					    						<c:if test="${role ne 'Tank'}">
					    						<option value="Tank">&nbsp Tank</option>
					    						</c:if>
					    						
					    						<c:if test="${role eq 'Heal'}">
					    						<option value="Heal" selected>&nbsp Heal</option>
					    						</c:if>
					    						<c:if test="${role ne 'Heal'}">
					    						<option value="Heal">&nbsp Heal</option>
					    						</c:if>
					    						
					    						<c:if test="${role eq 'Dps'}">
					    						<option value="Dps" selected>&nbsp Dps</option>
					    						</c:if>
					    						<c:if test="${role ne 'Dps'}">
					    						<option value="Dps">&nbsp Dps</option>
					    						</c:if>
					    						
												</select>&nbsp&nbsp			
											<i class="fas fa-trash-alt"></i>
										</div>
									</div>
								</c:forEach>
								</c:if>
									
								<div class="edit-roles">	
									<div class="role-select">		
 											<select name="role">
					    						<option value="None" default></option>
					    						<option value="Tank">Tank</option>
					    						<option value="Heal">Heal</option>
    											<option value="Dps">Dps</option>
											</select>&nbsp&nbsp						
											<i class="fas fa-plus"></i>
									</div>
								</div>
								
													
							</div>
						</div>
						
					</div>
				</fieldset>