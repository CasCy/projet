<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script src="js/filters.js"></script>

<meta name='viewport' content='width=device-width' />

 <div class="sidebar">
 	
		<div class="sidebar-title">
			<div class="sidebar-title-text">		
					<br><span class="title"><a href="index"><img src="img/logo.png" style="width: 100%;"></a></span><br><br>
			</div>
		</div>	
		<div class="sidebar-search">
			<div class="search-section">
				<form>					
					<div class="search-header">
						<span class="form-row ">
							<input class="form-control col mr-sm-2" type="search" placeholder="Item Search" aria-label="Search" name="itemSearch">
							<button class="btn btn-outline-dark col-auto my-2 my-sm-0" type="button"><i class="fas fa-filter"></i></button>
						</span>				
					</div>
				
					<div class="sidebar-filters" style="display: none">
						<input class="reset" type="reset" value="Reset All">
							<div class="filters-content">
								<div class="filters-filter">
									<div class="filter-itemleft">
										<span>Item Level Min</span>
									</div>
									<div class="filter-itemright">
										<input name="ilvlmin" type="number" class="form-control form-control-sm" min="0" value="0">
									</div>
								</div>
								<div class="filters-filter">
									<div class="filter-itemleft">
										<span>Item Level Max</span>								
									</div>
									<div class="filter-itemright">
										<input name="ilvlmax" type="number" class="form-control form-control-sm" max="540" value="540">
									</div>
								</div>
								<div class="filters-filter">
									<div class="filter-itemleft">
										<span>Select a Slot</span>
									</div>
									<div class="filter-itemright">
										<div class="slot-select">
 											 <select name="slot">
					    						<option value="All" selected>All</option>
												<option value="Weapon">Weapon</option>
												<option value="Shield">Shield</option>
												<option value="Head">Head</option>
												<option value="Body">Body</option>
												<option value="Hands">Hands</option>
												<option value="Waist">Waist</option>
												<option value="Legs">Legs</option>
												<option value="Feet">Feet</option>
												<option value="Earrings">Earrings</option>
												<option value="Necklace">Necklace</option>
												<option value="Wirst">Wirst</option>
												<option value="Ring">Ring</option>
											</select>
										</div>
									</div>
								</div>
								<div class="filters-filter">
									<div class="filter-itemleft">
										<span>Select a Job</span>
									</div>
									<div class="filter-itemright">
										<div class="slot-select">
 											 <select name="job">
					    						<option value="All" selected>All</option>					    						<option value="Paladin">Paladin</option>
					    						<option value="DarkKnight">Dark Knight</option>
    											<option value="Gunbreaker">Gunbreaker</option>
    											<option value="Warrior">Warrior</option>
 				   								<option value="WhiteMage">White Mage</option>
    											<option value="Astrologian">Astrologian</option>
    											<option value="Scholar">Scholar</option>
    											<option value="Bard">Bard</option>
    											<option value="Machinist">Machinist</option>
    											<option value="Dancer">Dancer</option>
    											<option value="BlackMage">Black Mage</option>
    											<option value="Summoner">Summoner</option>
    											<option value="RedMage">Red Mage</option>
    											<option value="Ninja">Ninja</option>
    											<option value="Dragoon">Dragoon</option>
    											<option value="Monk">Monk</option>
    											<option value="Samurai">Samurai</option>
											</select>
										</div>
									</div>
								</div>
								<br>
							</div>	
					</div>				
					
				</form>
				</div>
				</div>

				<div class="search-tab">
				
				
					
						<div class="search-tab-head">
						<c:if test="${searchedItem.size() == 1}">
							<span>${searchedItem.size()} Item found</span>
						</c:if>
						<c:if test="${searchedItem.size() > 1}">
							<span>${searchedItem.size()} Items found</span>
						</c:if>
						<c:if test="${searchedItem.size() == null}">
							<span>0 Items found</span>
						</c:if>
						</div>
						
						<c:if test="${!empty searchedItem}">
						<div class="search-tab-content">
						
						<c:forEach items="${searchedItem}" var="item">
						
							<div  class="search-tab-row" data-name="${item.id}">			
								<span>&nbsp&nbsp<img src="${item.img}"  style="width: 20px; height: 20px;"> </span>
								<span class="search-tab-row-name"> ${item.nom}</span>
								<span class="search-tab-row-ilvl-slot"> - iLvl ${item.ilvl}</span>							
							</div>						
						</c:forEach>
						
						</div>
						</c:if>
								
				</div>


</div>	

