<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	



				

				<fieldset>
					<legend><i class="fas fa-key mr-1"></i> Login password</legend>				

					<div style="color:red;">${errorMessage}</div>
					<div style="color:red;;">${passwordErrorMessage}</div>
					<div style="color:red;">${offline}</div>
					<div style="color:green;">${loginInfoSuccess}</div> <br>
					
					<c:if test="${!empty editedUser}">
					<label for="username">Username:</label>
					<input type="text" name="username" class="form-control" value="${editedUser.username}" disabled/>
					<div style=" color:red; display:none"></div>
					<span style="color: red;">${usernameError}</span><br>
					</c:if>			
						
					<c:if test="${empty editedUser}">
					<label for="username">Username:</label>
					<input type="text" name="username" class="form-control" value="${editedUser.username}" />
					<div style=" color:red; display:none"></div>
					<span style="color: red;">${usernameError}</span><br>
					</c:if>
					
					<c:if test="${!empty editedUser}">
					<label for="email">Email :</label>
					<input type="text" name="email" placeholder="dummy@example.com" class="form-control" value="${editedUser.email}"  disabled/>
					<div style=" color:red; display:none"></div><br>
					</c:if>
					
					<c:if test="${empty editedUser}">
					<label for="email">Email :</label>
					<input type="text" name="email" placeholder="dummy@example.com" class="form-control" value="${editedUser.email}" />
					<div style=" color:red; display:none"></div><br>
					</c:if>					
					
					<c:if test="${!signIn}">
					<label for="password">Old Password:</label> 
					<input type="password" name="oldPassword" class="form-control" />
					<div style=" color:red; display:none"></div><br><br>
					<label>New Password:</label>
					<input type="password" name="newPassword" class="form-control" />
					<div style=" color:red; display:none"></div><br>					
					<label>New Password Confirmation:</label>
					<input type="password" name="newPasswordConfirmation" class="form-control" />
					<div style=" color:red; display:none"></div><br>					
					</c:if>				
					
					<c:if test="${signIn}">
					<label for="password">Password:</label> 
					<input type="password" name="password" class="form-control" />
					<div style=" color:red; display:none"></div><br>
					<label>Confirmation:</label>
					<input type="password" name="passwordConfirmation" class="form-control" />
					<div style=" color:red; display:none"></div><br>
					</c:if>
					
				</fieldset>