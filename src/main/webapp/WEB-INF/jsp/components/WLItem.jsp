<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="js/selectorplus.js"></script>


<!-- <script src="js/validation.js"></script> -->


<c:if test="${!empty item}">	
	<div class="item-head">
			<h6 style="text-align: center;">Is in ${user.roster.nom}'s members Wishlist</h6>
	</div>
	<div class="content-content-content">
		<div class="WL-list">
			<c:forEach items="${listeMembreWL}" var="membre">
				<c:if test="${membre.roster.id eq user.roster.id}">
					<div>
						<img src="${membre.img}" style="width: 35px;"> ${membre.prenom} ${membre.nom}
					</div>	
				</c:if> 
			</c:forEach>
		</div>	
	</div>
</c:if>