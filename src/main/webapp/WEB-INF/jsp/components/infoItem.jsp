<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="js/selectorplus.js"></script>
<!-- <script src="js/listeVoeux.js"></script> -->


<!-- <script src="js/validation.js"></script> -->


<c:if test="${!empty item}">


	<div class="item-head">
	
		<c:if test="${!empty user}">
			<c:if test="${!inWL}">
			<i class="fas fa-heart" id="notWL"  data-name="${item.id}"></i>
			</c:if>	
			<c:if test="${inWL}">
			<i class="fas fa-heart-broken" id="inWL"  data-name="${item.id}"></i>
			</c:if>
		</c:if>
			<h6>ITEM &nbsp #${item.id}</h6>
			
			
	</div>


	<div class="content-content-content">
		
		<div class="item-head-title">
			<div class="item-head-title-left">
				<img src="${item.img}" style="width: 90%;">				
			</div>
			<div class="item-head-title-right">
				<div class="item-name">
					${item.nom}
				</div>
				<div class="item-ilvl">
					[ Lv 70 &nbsp&nbsp ${item.slot} &nbsp&nbsp&nbsp Item level - ${item.ilvl} ]
				</div>	
				<br><br>
				
				<c:if test="${item.slot eq 'Weapon'}">
					<c:if test="${item.physick ne 0}">
				<div class="item-def">
						P.ATT : ${item.physick}
				</div>
					</c:if>
					<c:if test="${item.magic ne 0}">
				<div class="item-def">
						M.ATT : ${item.magic}
				</div>
					</c:if>
				</c:if>				
				
				<c:if test="${item.slot eq 'Shield'}">
					<div class="item-def">
					Block Strenght : ${item.magic} - Block Rate : ${item.magic}
				</div>
				</c:if>			
				
				<c:if test="${item.slot ne 'Weapon'}">
				<c:if test="${item.slot ne 'Shield'}">
				<div class="item-def">
					P.DEF : ${item.magic} - M.DEF : ${item.magic}
				</div>
				</c:if>
				</c:if>
				
				
				
			</div>
		</div>
		
		<div class="item-jobs">
						<c:if test="${!empty item.jobs}">
						<c:forEach items="${item.jobs}" var="job">
							<div class="item-ilvl">
								<img src="img/icon/${job}.png" style="width: 30px;"> &nbsp ${job} &nbsp 
							</div>
						</c:forEach>
						</c:if>
		</div>
		
		<br>
		
		
		
		
		<div class="item-details">
		
			<div class="item-stats">
				<c:forEach items="${item.stats}" var="stat">
					${stat.attrib} : <span>${stat.valeur}</span> <br>
				</c:forEach>
			</div>
		
			<div class="item-other">
				Repair by : <span>${item.repair} &nbsp</span><img src="img/icon/${item.repair}.png" style="width: 30px;"><br>
				Source : <span>${item.source}</span> 
			</div>
		
		</div>
		
	</div>	
	
</c:if>