<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	


				<fieldset>
					<legend><i class="fas fa-sign-in-alt mr-1"></i> Login</legend>
						<label for="username">Username:</label>
						<input type="text" name="username" class="form-control" />
						<label for="password">Password:</label> 
						<input type="password" name="password" class="form-control" />
						<span style="color: red;">${errorMessage}</span><br>
				</fieldset>