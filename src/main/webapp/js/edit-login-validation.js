
$(document).ready(function(){
	
	var error;
	
	function invalid(e, m) {
		e.css("border", "1px solid rgba(125, 0, 0, 1)");
		if (m !== undefined) {
			e.next().text(m);
			e.next().show();
		} else {
			e.next().hide();
		}
		error = true;
	}

	function valid(e) {
		e.next().hide();
		e.css("border", "1px solid rgba(197,182, 157, 0.5)");
	}
	
	
	
	
		
	$(".content form").submit(function() {
		error = false;

		console.log("COUCOU");
		
		// password validation
		// password != ""
		if ($("input[name=oldPassword]").val() == "") {
			invalid($("input[name=oldPassword]"), "The password cannot be empty.");
		}
		// password length < 8
		else if ($("input[name=newPassword]").val().length < 8) {
			console.log("password lenght < 8 FALSE");
			invalid($("input[name=newPassword]"), "The password must be at most 8 characters long.");
		}
				
		// password == confirmation
		else if ($("input[name=newPasswordConfirmation]").val() != $("input[name=newPassword]").val()) {
			console.log("password == confirmation FALSE"); // 
			invalid($("input[name=newPassword]"));
		} else {
			console.log("password == confirmation TRUE");
			valid($("input[name=newPassword]"));
		}
		
		// passwordConfirmation validation
		// password  == confirmation
		if ($("input[name=newPasswordConfirmation]").val() != $("input[name=newPassword]").val()) {
			console.log("password == confirmation 2 FALSE");
			invalid($("input[name=newPasswordConfirmation]"), "The confirmation does not match the new password.");
		} else {
			console.log("password == confirmation 2 TRUE");
			valid($("input[name=newPasswordConfirmation]"));
		}
		
		return !error;
	});
	
});