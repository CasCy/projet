
$(document).ready(function(){
	
	var error;
	var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	function invalid(e, m) {
		e.css("border", "1px solid rgba(125, 0, 0, 1)");
		if (m !== undefined) {
			e.next().text(m);
			e.next().show();
		} else {
			e.next().hide();
		}
		error = true;
	}

	function valid(e) {
		e.next().hide();
		e.css("border", "1px solid rgba(197,182, 157, 0.5)");
	}
	
	
	
	
	$("input[name=username]").keyup(function() {
		// envoyer une requete
		var req = new XMLHttpRequest();
		req.open("GET", "usernameexists?username="+$(this).val(), true);
		req.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	if (this.responseText == "true") {
		    		invalid($("input[name=username]"), "Username already in use.");
		    	} else {
		    		valid($("input[name=username]"));
		    	}
		    }
		};
		req.send();
	});
	
	$("input[name=rostername]").keyup(function() {
		// envoyer une requete
		var req = new XMLHttpRequest();
		req.open("GET", "rosternameexists?rostername="+$(this).val(), true);
		req.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	if (this.responseText == "true") {
		    		invalid($("input[name=rostername]"), "Username already in use.");
		    	} else {
		    		valid($("input[name=rostername]"));
		    	}
		    }
		};
		req.send();
	});
	
	
	
	
	$(".content form").submit(function() {
		error = false;
		
		// firstname validation
		if ($("input[name=prenom]").val() == "") {
			invalid($("input[name=prenom]"), "The firstname is mandatory.");
		} else {
			valid($("input[name=prenom]"));
		}
		
		// lastname validation
		if ($("input[name=nom]").val() == "") {
			invalid($("input[name=nom]"), "The lastname is mandatory.");
		} else {
			valid($("input[name=nom]"));
		}
		
		// serveur validation
		if ($("input[name=serveur]").val() == "") {
			invalid($("input[name=serveur]"), "The server is mandatory.");
		} else {
			valid($("input[name=serveur]"));
		}

		// username validation
		if ($("input[name=username]").val() == "") {
			invalid($("input[name=username]"), "The username is mandatory.");
		} else {
			valid($("input[name=username]"));
		}
		
		// email validation
		// email != ""
		if ($("input[name=email]").val() == "") {
			invalid($("input[name=email]"), "The email is mandatory.");
		}
		// email format
		else if (!emailRegExp.test($("input[name=email]").val())) {
			invalid($("input[name=email]"), "The email format is invalid.");
		} else {
			valid($("input[name=email]"));
		}
		
		// password validation
		// password != ""
		if ($("input[name=password]").val() == "") {
			invalid($("input[name=password]"), "The password cannot be empty.");
		}
		// password length < 8
		else if ($("input[name=password]").val().length < 8) {
			invalid($("input[name=password]"), "The password must be at least 8 characters long.");
		}
		// password == confirmation
		else if ($("input[name=passwordConfirmation]").val() != $("input[name=password]").val()) {  
			invalid($("input[name=password]"));
		} else {
			valid($("input[name=password]"));
		}
		
		// firstname validation
		if ($("input[name=rostername]").val() == "") {
			invalid($("input[name=rostername]"), "The roster name is mandatory.");
		}  
		else if ($("input[name=rostername]").val().length < 4) {
			invalid($("input[name=rostername]"), "The roster name must be at least 4 characters long.");
		}
		else {
			valid($("input[name=rostername]"));
		}
	
		return !error;
	});
	
});