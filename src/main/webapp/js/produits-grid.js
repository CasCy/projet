function updateProductButtonsVisibility(product) {
	if (product.find("input.quantite").val() == "0") {
		product.find(".quantite").invisible();
	} else {
		product.find(".quantite").visible();
	}
}

function updateCommandButtonVisibility(update) {
	if (update) {
		$("#commander").visible();
	} else {
		var nb = 0;
		$(".product input.quantite").each(function() {
			nb += Number($(this).val());
		});
		if (nb > 0) {
			$("#commander").visible();
		} else {
			$("#commander").invisible();
		}
	}
}



jQuery.fn.visible = function() {
    return this.css('visibility', 'visible');
};

jQuery.fn.invisible = function() {
    return this.css('visibility', 'hidden');
};

jQuery.fn.visibilityToggle = function() {
    return this.css('visibility', function(i, visibility) {
        return (visibility == 'visible') ? 'hidden' : 'visible';
    });
};






$(document).ready(function(){

	$(".product").each(function() {
		updateProductButtonsVisibility($(this));
	});
	updateCommandButtonVisibility();
	


	$(".product .fa-plus-circle").click(function() {
		var product = $(this).parents(".product");
		var input = product.find("input.quantite");
		input.val(Number(input.val())+1);
		updateProductButtonsVisibility(product);
		updateCommandButtonVisibility();
	});


	$(".product .fa-minus-circle").click(function() {
		var id = $(this).attr("data-id");
		var product = $(".product[data-id="+id+"]");
		var input = $("input[data-id="+id+"]");
		input.val(Number(input.val())-1);
		updateProductButtonsVisibility(product);
		updateCommandButtonVisibility();
	});

	$(".product input.quantite").change(function() {
		var value = $(this).val();
		if ((value < 0) ||
			isNaN(value)) {
			$(this).val("0");
		}
		var id = $(this).attr("data-id");
		var product = $(".product[data-id="+id+"]");
		updateProductButtonsVisibility(product);
		updateCommandButtonVisibility();
	});


}); 