package com.projet.models;

public enum Slot {
	Weapon,
	Shield,
	Head,
	Body,
	Hands,
	Waist,
	Legs,
	Feet,
	Earrings,
	Necklace,
	Wirst,
	Ring
}
