package com.projet.models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="raid")
public class Raid {
	
	@Id
	@GeneratedValue
	private int id;
	
	private LocalDate date;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Roster roster;
	
	@OneToMany(mappedBy="raid", cascade = CascadeType.ALL)
	private Set<Loot> loots = new HashSet<Loot>();

	public Raid() {
		super();
	}

	public Raid(LocalDate date, Roster roster) {
		super();
		this.date = date;
		this.roster = roster;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Roster getRoster() {
		return roster;
	}

	public void setRoster(Roster roster) {
		this.roster = roster;
	}

	public Set<Loot> getLoots() {
		return loots;
	}

	public void setLoots(Set<Loot> loots) {
		this.loots = loots;
	}

	@Override
	public String toString() {
		return "\tRaid (ID :" + id + " ) --> Date : " + date + " --> Roster :" + roster;
	}
}
