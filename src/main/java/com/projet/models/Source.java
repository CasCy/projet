package com.projet.models;

public enum Source {
	Vendor,
	O1S,
	O2S,
	O3S,
	O4S,
	O5S,
	O6S,
	O7S,
	O8S,
	O9S,
	O10S,
	O11S,
	O12S
}
