package com.projet.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="item")
public class Item {
	
	@Id
	@GeneratedValue
	private int id;
	
	@Column(unique=true)
	private String nom;
	
	private int ilvl;
	
	@Enumerated(EnumType.STRING)
	private Slot slot;
	
	@Enumerated(EnumType.STRING)
	private Source source;
	
	@Enumerated(EnumType.STRING)
	@ElementCollection(fetch = FetchType.EAGER)
	Set<Job> jobs = new HashSet<Job>();
	
	@Enumerated(EnumType.STRING)
	@ElementCollection(fetch = FetchType.EAGER)
	Set<Stats> stats = new HashSet<Stats>();
	
	@OneToMany(mappedBy="item", fetch = FetchType.EAGER)
	private Set<Loot> loots;
	
	@ManyToMany(mappedBy="items", fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Set<ListeVoeux> listeVoeux = new HashSet<ListeVoeux>();
	
	private String img;
	
	@Enumerated(EnumType.STRING)
	private Craft repair;
	
	private int physick;
	
	private int magic;

	public Item() {
		super();
	}

	public Item(String nom, int ilvl, Slot slot, Source source, Set<Job> jobs, Set<Stats> stats, Set<Loot> loots,
			Set<ListeVoeux> listeVoeux, String img, Craft repair, int physick, int magic) {
		super();
		this.nom = nom;
		this.ilvl = ilvl;
		this.slot = slot;
		this.source = source;
		this.jobs = jobs;
		this.stats = stats;
		this.loots = loots;
		this.listeVoeux = listeVoeux;
		this.img = img;
		this.repair = repair;
		this.physick = physick;
		this.magic = magic;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getIlvl() {
		return ilvl;
	}

	public void setIlvl(int ilvl) {
		this.ilvl = ilvl;
	}

	public Slot getSlot() {
		return slot;
	}

	public void setSlot(Slot slot) {
		this.slot = slot;
	}

	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}

	public Set<Job> getJobs() {
		return jobs;
	}

	public void setJobs(Set<Job> jobs) {
		this.jobs = jobs;
	}

	public Set<Stats> getStats() {
		return stats;
	}

	public void setStats(Set<Stats> stats) {
		this.stats = stats;
	}

	public Set<Loot> getLoots() {
		return loots;
	}

	public void setLoots(Set<Loot> loots) {
		this.loots = loots;
	}

	public Set<ListeVoeux> getListeVoeux() {
		return listeVoeux;
	}

	public void setListeVoeux(Set<ListeVoeux> listeVoeux) {
		this.listeVoeux = listeVoeux;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Craft getRepair() {
		return repair;
	}

	public void setRepair(Craft repair) {
		this.repair = repair;
	}

	public int getPhysick() {
		return physick;
	}

	public void setPhysic(int physick) {
		this.physick = physick;
	}

	public int getMagic() {
		return magic;
	}

	public void setMagic(int magic) {
		this.magic = magic;
	}

	@Override
	public String toString() {
		return "\t " + nom + " - iLvl : " + ilvl + " - slot : " + slot + "\n"
				+"\tjobs : " + jobs + "\n"
				+"\tstats : " + stats + "\n"
				+"\tImg URL : "+ img;
	}
	
}
