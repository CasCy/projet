package com.projet.models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.projet.models.Role;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Membre extends Utilisateur{

	@Column(unique=true)
	private String username;
	private String password;
	private String serveur;

	@Enumerated(EnumType.STRING)
	@ElementCollection(fetch = FetchType.EAGER)
	private Set<Role> roles = new HashSet<Role>();

	@Enumerated(EnumType.STRING)
	@ElementCollection(fetch = FetchType.EAGER)
	private Set<Job> jobs = new HashSet<Job>();

	@OneToMany(mappedBy="membre")
	private Set<Loot> loots = new HashSet<Loot>();

	@ManyToOne
	private Roster roster;

	@Enumerated(EnumType.STRING)
	@ElementCollection(fetch = FetchType.EAGER)
	private List<Prio> prios;

	@OneToOne(cascade=CascadeType.ALL)
	private ListeVoeux listeVoeux;
	
	private String img = "img/avatar.jpg";

	public Membre() {
		super();
	}

	public Membre(String prenom, String nom, String email, String username, String password, String serveur,  
			Set<Role> roles, Set<Job> job, Set<Loot> loots, Roster roster, List<Prio> prios, ListeVoeux listeVoeux, String img) {		
		super(prenom, nom , email);
		this.username = username;
		this.password = password;
		this.serveur = serveur;
		this.roles = roles;
		this.jobs = job;
		this.loots = loots;
		this.roster = roster;
		this.prios = prios;
		this.listeVoeux = listeVoeux;
		this.img = img;
	}

	public Membre(int id, String prenom, String nom, String email, String username, String password, String serveur,  
			Set<Role> roles, Set<Job> job, Set<Loot> loots, Roster roster, List<Prio> prios, ListeVoeux listeVoeux, String img) {		
		super(id, prenom, nom , email);
		this.username = username;
		this.password = password;
		this.serveur = serveur;
		this.roles = roles;
		this.jobs = job;
		this.loots = loots;
		this.roster = roster;
		this.prios = prios;
		this.listeVoeux = listeVoeux;
		this.img = img;
	}

	public Membre(String prenom, String nom, String email, String username, String password, String serveur) {		
		super(prenom, nom , email);
		this.username = username;
		this.password = password;
		this.serveur = serveur;
	}


	public Membre(int id,String prenom, String nom, String email, String username, String password, String serveur
			, Set<Loot> loots, Roster roster, List<Prio> prios, ListeVoeux listeVoeux, String img) {
		super(id, prenom, nom , email);
		this.username = username;
		this.password = password;
		this.serveur = serveur;
		this.loots = loots;
		this.roster = roster;
		this.prios = prios;
		this.listeVoeux = listeVoeux;
		this.img = img;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getServeur() {
		return serveur;
	}

	public void setServeur(String serveur) {
		this.serveur = serveur;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public Set<Job> getJobs() {
		return jobs;
	}

	public void setJobs(Set<Job> jobs) {
		this.jobs = jobs;
	}

	public Set<Loot> getLoots() {
		return loots;
	}

	public void setLoots(Set<Loot> loots) {
		this.loots = loots;
	}

	public Roster getRoster() {
		return roster;
	}

	public void setRoster(Roster roster) {
		this.roster = roster;
	}

	public List<Prio> getPrios() {
		return prios;
	}

	public void setPrios(List<Prio> prios) {
		this.prios = prios;
	}

	public ListeVoeux getListeVoeux() {
		return listeVoeux;
	}

	public void setListeVoeux(ListeVoeux listeVoeux) {
		this.listeVoeux = listeVoeux;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Override
	public String toString() {
		return "\t "+super.getPrenom()+" "+super.getNom()+" (ID : "+super.getId()+") \n"
			+ "\tlogin : " + username + " - password : " + password + " - mail : "+super.getEmail()+" \n"
			+ "\tserveur : " + serveur + " - roster : \n"
			+ "\tjobs : " + jobs + " \n"
			+ "\troles : "+ roles + " \n"
			+ "\tordre de prio : "+prios+ " \n"
			+ "\timg : "+img+ " \n"
			+ "\tListeVoeux : "+listeVoeux+ " \n";
	}
}

