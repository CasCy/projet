package com.projet.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="roster")
public class Roster {

	@Id
	@GeneratedValue
	private int id;
	
	@Column(unique=true)
	private String nom;
	
	@OneToMany(mappedBy="roster", fetch = FetchType.EAGER) 
	private Set<Membre> membres = new HashSet<Membre>();
	
	@OneToOne(fetch = FetchType.EAGER)
	private Membre leader;
	
	@OneToMany(mappedBy="roster", cascade = CascadeType.ALL)
	private Set<Raid> raids = new HashSet<Raid>();

	
	public Roster() {
		super();
	}

	public Roster(String nom, Set<Membre> membres, Membre leader, Set<Raid> raids) {
		super();
		this.nom = nom;
		this.membres = membres;
		this.leader = leader;
		this.raids = raids;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public Set<Membre> getMembres() {
		return membres;
	}


	public void setMembres(Set<Membre> membres) {
		this.membres = membres;
	}

	public Membre getLeader() {
		return leader;
	}

	public void setLeader(Membre leader) {
		this.leader = leader;
	}


	public Set<Raid> getRaids() {
		return raids;
	}


	public void setRaids(Set<Raid> raids) {
		this.raids = raids;
	}


	public Roster(String nom) {
		super();
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Roster [id=" + id + ", nom=" + nom + ", membres=" + membres + ", leader=" + leader + ", raids=" + raids
				+ "]";
	}
	
}
