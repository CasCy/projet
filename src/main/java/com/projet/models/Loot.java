package com.projet.models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="loot")
public class Loot {

	@Id
	@GeneratedValue
	private int id;
	
	@ManyToOne
	private Item item;
	
	@ManyToOne
	private Membre membre;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Raid raid;

	public Loot() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Membre getMembre() {
		return membre;
	}

	public void setMembre(Membre membre) {
		this.membre = membre;
	}

	public Raid getRaid() {
		return raid;
	}

	public void setRaid(Raid raid) {
		this.raid = raid;
	}

	public Loot(Item item, Membre membre, Raid raid) {
		super();
		this.item = item;
		this.membre = membre;
		this.raid = raid;
	}
	
	
}
