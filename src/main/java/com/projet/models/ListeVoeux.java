package com.projet.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="listevoeux")
public class ListeVoeux {

	@Id
	@GeneratedValue
	private int id;

	@ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Set<Item> items = new HashSet<Item>();

	@OneToOne(mappedBy="listeVoeux")
	private Membre membre;

	public ListeVoeux() {
		super();
	}

	public ListeVoeux(Set<Item> items, Membre membre) {
		super();
		this.items = items;
		this.membre = membre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}

	public Membre getMembre() {
		return membre;
	}

	public void setMembre(Membre membre) {
		this.membre = membre;
	}

	@Override
	public String toString() {
		return "[items=" + items +"]";
	}

}