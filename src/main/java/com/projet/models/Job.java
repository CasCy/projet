package com.projet.models;

public enum Job {
	
	//tank
	Paladin,
	DarkKnight,
	Warrior,
	Gunbreaker,
	
	//heal
	WhiteMage,
	Astrologian,
	Scholar,
	
	//ranged
	Bard,
	Machinist,
	Dancer,
	
	//caster
	BlackMage,
	Summoner,
	RedMage,
	
	//melee
	Ninja,
	Dragoon,
	Samurai,
	Monk
	
}
