package com.projet.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="stats")
public class Stats {
	
	@Id
	@GeneratedValue
	private int id;

	@Enumerated(EnumType.STRING)
	private Attrib attrib;
	private int valeur;
	
	
	public Stats() {
		super();
	}
	

	public Stats(Attrib attrib, int valeur) {
		super();
		this.attrib = attrib;
		this.valeur = valeur;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	@Enumerated(EnumType.STRING)
	public Attrib getAttrib() {
		return attrib;
	}


	public void setAttrib(Attrib attrib) {
		this.attrib = attrib;
	}


	public int getValeur() {
		return valeur;
	}


	public void setValeur(int valeur) {
		this.valeur = valeur;
	}


	@Override
	public String toString() {
		return "\t" + attrib + " : " + valeur;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attrib == null) ? 0 : attrib.hashCode());
		result = prime * result + id;
		result = prime * result + valeur;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stats other = (Stats) obj;
		if (attrib != other.attrib)
			return false;
		if (id != other.id)
			return false;
		if (valeur != other.valeur)
			return false;
		return true;
	}
}
