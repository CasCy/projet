package com.projet.models;

public enum Attrib {
	Strenght,
	Dexterity,
	Vitality,
	Intelligence,
	Mind,
	CriticalHit,
	DirectHit,
	Determination,
	SkillSpeed,
	SpellSpeed,
	Tenacity,
	Piety
	
}
