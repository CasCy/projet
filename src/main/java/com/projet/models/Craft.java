package com.projet.models;

public enum Craft {
	Alchemist,
	Armorer,
	Blacksmith,
	Carpenter,
	Culinarian,
	Goldsmith,
	Leatherworker,
	Weaver
}
