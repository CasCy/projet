package com.projet.services;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.projet.daos.StatsDao;
import com.projet.models.Attrib;
import com.projet.models.Stats;
import com.projet.utils.HibernateUtil;

public class StatsService {

	private StatsDao statsDao;
	
	public StatsService() {
		super();
		statsDao = new StatsDao();
	}
	
	public void ajouterStats(Attrib attrib, int valeur) throws Exception {
		Stats s = new Stats(attrib, valeur);
		ajouterStats(s);
	}
	
	public void ajouterStats(Stats s) throws Exception {
		statsDao.save(s);
	}
	
	public List<Stats> getAll() {
		return statsDao.findAll();
	}


	public void ajouterStats(Set<Stats> liststats) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		ajouterStats(liststats, s);
		s.close();		
	}
	
	public void ajouterStats(Set<Stats> liststats, Session s) {
		s.beginTransaction();
		try {
			ajouterStats(liststats, s, s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
		}
	}
	
	public void ajouterStats(Set<Stats> liststats, Session s, Transaction t) throws Exception {
		for (Stats stat : liststats) statsDao.save(stat);
	}
}
