package com.projet.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.projet.daos.RaidDao;
import com.projet.models.Raid;
import com.projet.models.Roster;

public class RaidService {

	private RaidDao raidDao;
	
	public RaidService() {
		super();
		raidDao = new RaidDao();
	}
	
	
	public void ajouterRaid(LocalDate date, Roster r) throws Exception {
		Raid ra = new Raid(date, r);
		ajouterRaid(ra);
	}
	
	public void ajouterRaid(Raid r) throws Exception {
		raidDao.save(r);
	}
	
	public Raid getById(int id) {
		return raidDao.findById(id);
	}
	
	public List<Raid> getAll() {
		return raidDao.findAll();
	}
	
	public List<Raid> getByDate(LocalDateTime date) {
		return raidDao.findByDate(date);
	}
	
	public Raid getByDateStrict(LocalDateTime date) {
		return raidDao.findByDateStrict(date);
	}
	
	public List<Raid> getByRoster(Roster r) {
		return raidDao.findByRoster(r);
	}
}
