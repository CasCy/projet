package com.projet.services;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.projet.daos.MembreDao;
import com.projet.models.Job;
import com.projet.models.ListeVoeux;
import com.projet.models.Loot;
import com.projet.models.Membre;
import com.projet.models.Prio;
import com.projet.models.Role;
import com.projet.models.Roster;
import com.projet.utils.HibernateUtil;

public class MembreService {

	private MembreDao membreDao;

	public MembreService() {
		super();
		membreDao = new MembreDao();
	}
	
	
	

	public void ajouterMembre(String prenom, String nom, String email, String username, String password, String serveur,  
			Set<Role> roles, Set<Job> jobs, Set<Loot> drops, Roster roster,  List<Prio> prios, ListeVoeux listeVoeux, String img) throws Exception {
		Membre m = new Membre(prenom, nom, email, username, password, serveur, roles, jobs, drops, roster, prios, listeVoeux, img);
		ajouterMembre(m);
	}

	public void ajouterMembre(Membre m) throws Exception {
		membreDao.save(m);
	}
	
	
	
	

	public void ajouterMembrePrios(Membre m, List<Prio> lp) throws Exception {
		m.setPrios(lp);
		membreDao.save(m);
	}
	
	public Membre getById(int id) {
		return membreDao.findById(id);
	}

	public List<Membre> getByRoster(Roster r) {
		return membreDao.findByRoster(r);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	public void supprimerMembre(Membre m) throws Exception {
		membreDao.delete(m);
	}

	public List<Membre> getAll() {
		return membreDao.findAll();
	}

	public Membre getByUsername(String str) {
		return membreDao.findByUsername(str);
	}

	public List<Membre> getByPseudo(String str) {
		return membreDao.findByPseudo(str);
	}

	public Membre getByUsernamePasssword(String username, String password) {
		return membreDao.findByUsernameAndPassword(username, password);
	}

	public List<Membre> getByServeur(String serveur) {
		return membreDao.findByServeur(serveur);
	}

	public List<Membre> getByRole(Role role) {
		return membreDao.findByRole(role);
	}

	public List<Membre> getByJob(Job job) {
		return membreDao.findByJob(job);
	}

	//public void removeMembreLP(Membre m, List)
}
