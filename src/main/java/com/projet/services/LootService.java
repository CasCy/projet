package com.projet.services;

import java.util.List;

import com.projet.daos.LootDao;
import com.projet.models.Loot;

public class LootService {

	private LootDao lootDao;
	
	public LootService() {
		super();
		lootDao = new LootDao();
	}


	public List<Loot> getAll() {
		return lootDao.findAll();
	}

}
