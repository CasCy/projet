package com.projet.services;

import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.projet.daos.ListeVoeuxDao;
import com.projet.models.Item;
import com.projet.models.ListeVoeux;
import com.projet.models.Membre;
import com.projet.utils.HibernateUtil;

public class ListeVoeuxService {

	private ListeVoeuxDao listeVoeuxDao;




	public void ajouterListeVoeux(ListeVoeux lv, Membre m) throws Exception {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Transaction t = s.beginTransaction();
		try {
			ajouterListeVoeux(lv, m, s, t);
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		}
		s.close();
	}

	public void ajouterListeVoeux(ListeVoeux lv, Membre m, Session s, Transaction t) throws Exception {
		Membre membre = (Membre) s.load(Membre.class,  m.getId());
		System.out.println("récup load");
		membre.setListeVoeux(lv);
		System.out.println("add lp to m");
		lv.setMembre(membre);
		System.out.println("add m to lp");
		listeVoeuxDao.save(lv);
		System.out.println("lp ok");
	}
}
