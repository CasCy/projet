package com.projet.services;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.projet.daos.ItemDao;
import com.projet.daos.ListeVoeuxDao;
import com.projet.daos.MembreDao;
import com.projet.models.Craft;
import com.projet.models.Item;
import com.projet.models.Job;
import com.projet.models.ListeVoeux;
import com.projet.models.Loot;
import com.projet.models.Membre;
import com.projet.models.Slot;
import com.projet.models.Source;
import com.projet.models.Stats;
import com.projet.utils.HibernateUtil;

public class ItemService {
	
	private ItemDao itemDao;
	private MembreDao membreDao;
	
	public ItemService() {
		super();
		itemDao = new ItemDao();
	}

	public void ajouterItem(String nom, int ilvl, Slot slot, Source source, Set<Job> jobs, Set<Stats> stats, Set<Loot> loots,
			Set<ListeVoeux> listeVoeux, String img, Craft repair, int physick, int magic) throws Exception {
		Item i = new Item(nom, ilvl, slot, source, jobs, stats, loots, listeVoeux, img, repair, physick, magic);
		ajouterItem(i);
	}
	
	public void ajouterItem(Item i) throws Exception {
		itemDao.save(i);
	}
	
	public List<Item> getAll() {
		return itemDao.findAll();
	}
	
	public Item getById(int id) {
		return itemDao.findById(id);
	}

	public List<Item> getByName(String name) {
		return itemDao.findByName(name);
	}

	public Item getByNameStrict(String name) {
		return itemDao.findByNameStrict(name);
	}

	public List<Item> getByJob(Job job) {
		return itemDao.findByJob(job);
	}
	
	public List<Item> getByMin(int min) {
		return itemDao.findByMinIlvl(min);
	}

	public List<Item> getByMax(int max) {
		return itemDao.findByMaxIlvl(max);
	}

	public List<Item> getByMinMax(int min, int max) {
		return itemDao.findByMinMaxIlvl(min, max);
	}

	public List<Item> getBySlot(Slot slot) {
		return itemDao.findBySlot(slot);
	}
	
	public void addItemJob(Item i, Job j) throws Exception {
		itemDao.addItemJob(i, j);
	}
	
	public void removeItemJob(Item i, Job j) throws Exception {
		itemDao.removeItemJob(i, j);
	}
	
	public void addItemStats(Item i, Stats st) throws Exception {
		itemDao.addItemStats(i, st);
	}
	
	public void removeItemStats(Item i, Stats st) throws Exception {
		itemDao.removeItemStats(i, st);
	}
	
	public void removeItem(Item i) throws Exception {
		itemDao.delete(i);
	}

	public List<Item> getByNameIlvl(String name, int min, int max) {
		return itemDao.findByNameIlvl(name, min, max);
	}

	public List<Item> getByNameIlvlJob(String name, int min, int max, Job job) {
		return itemDao.findByNameIlvlJob(name, min, max, job);
	}

	public List<Item> getByNameIlvlSlot(String name, int min, int max, Slot slot) {
		return itemDao.findByNameIlvlSlot(name, min, max, slot);
	}

	public List<Item> getByNameIlvlSlotJob(String name, int min, int max, Slot slot, Job job) {
		return itemDao.findByNameIlvlSlotJob(name, min, max, slot, job);
	}

	
	public void addItemToListeVoeux(Item i, Membre m) throws Exception {
		Session s = HibernateUtil.getSessionFactory().openSession();
		addItemToListeVoeux(i, m, s);
		s.close();
	}

	public void addItemToListeVoeux(Item i, Membre m, Session s) throws Exception {
		Transaction t = s.beginTransaction();
		try {
			addItemToListeVoeux(i, m, s, t);
			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw e;
		}
	}
	
	public void addItemToListeVoeux(Item i, Membre m, Session s, Transaction t) throws Exception {
		Item it = (Item) s.load(Item.class,  i.getId());
		Membre me = (Membre) s.load(Membre.class,  m.getId());
		me.getListeVoeux().getItems().add(it);
		it.getListeVoeux().add(me.getListeVoeux());
		itemDao.save(it, s, t);
	}

	
	public void remItemToListeVoeux(Item i, Membre m) throws Exception {
		Session s = HibernateUtil.getSessionFactory().openSession();
		remItemToListeVoeux(i, m, s);
		s.close();
	}

	public void remItemToListeVoeux(Item i, Membre m, Session s) throws Exception {
		Transaction t = s.beginTransaction();
		try {
			remItemToListeVoeux(i, m, s, t);
			t.commit();
		} catch (Exception e) {
			t.rollback();
			throw e;
		}
	}
	
	public void remItemToListeVoeux(Item i, Membre m, Session s, Transaction t) throws Exception {
		Item it = (Item) s.load(Item.class,  i.getId());
		Membre me = (Membre) s.load(Membre.class,  m.getId());
		me.getListeVoeux().getItems().remove(it);
		it.getListeVoeux().remove(me.getListeVoeux());
		itemDao.save(it, s, t);
	}
	
	
}

