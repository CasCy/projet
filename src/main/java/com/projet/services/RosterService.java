package com.projet.services;

import java.lang.reflect.Member;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.projet.daos.RosterDao;
import com.projet.models.Membre;
import com.projet.models.Raid;
import com.projet.models.Roster;
import com.projet.utils.HibernateUtil;


public class RosterService {
	
	private RosterDao rosterDao;
	
	public RosterService() {
		super();
		rosterDao = new RosterDao();
	}

//	public void ajouterRoster(String nom, Set<Membre> membres, Membre leader, Set<Raid> raids) throws Exception {
//		Roster r = new Roster(nom, membres, leader, raids);
//		ajouterRoster(r);
//	}
	
	public void ajouterRoster(Roster r, Membre leader) throws Exception {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Transaction t = s.beginTransaction();
		try {
			ajouterRoster(r, leader, s, t);
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		}
		s.close();
	}
	
	public void ajouterRoster(Roster r, Membre leader, Session s, Transaction t) throws Exception {
		Membre l = (Membre) s.load(Membre.class, leader.getId());
		l.setRoster(r);
		rosterDao.save(r, s, t);
	}
	
	public void ajouterRosterMembre(Roster r, Membre m) throws Exception {
		Session s = HibernateUtil.getSessionFactory().openSession();
		ajouterRosterMembre(r, m, s);
		s.close();
	}

	public void ajouterRosterMembre(Roster r, Membre m, Session s) throws Exception {
		s.beginTransaction();
		try {
			ajouterRosterMembre(r, m,s,  s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
			throw e;
		}
	}

	public void ajouterRosterMembre(Roster r, Membre m, Session s, Transaction t) {
		Roster r1 = (Roster) s.load(Roster.class, r.getId());
		Membre m1 = (Membre) s.load(Membre.class, m.getId());
		r1.getMembres().add(m1);
		m1.setRoster(r1);
	}
	
	public void remRosterMembre(Roster r, Membre m) throws Exception {
		Session s = HibernateUtil.getSessionFactory().openSession();
		remRosterMembre(r, m, s);
		s.close();
	}

	public void remRosterMembre(Roster r, Membre m, Session s) throws Exception {
		s.beginTransaction();
		try {
			remRosterMembre(r, m,s,  s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
			throw e;
		}
	}

	public void remRosterMembre(Roster r, Membre m, Session s, Transaction t) {
		Roster r1 = (Roster) s.load(Roster.class, r.getId());
		Membre m1 = (Membre) s.load(Membre.class, m.getId());
		r1.getMembres().remove(m1);
		m1.setRoster(null);
	}
	
	public void swapRosterLead(Membre m) throws Exception {
		Session s = HibernateUtil.getSessionFactory().openSession();
		swapRosterLead(m, s);
		s.close();
	}

	public void swapRosterLead(Membre m, Session s) throws Exception {
		s.beginTransaction();
		try {
			swapRosterLead(m, s,  s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
			throw e;
		}
	}

	public void swapRosterLead(Membre m, Session s, Transaction t) {
		Membre m1 = (Membre) s.load(Membre.class, m.getId());
		m1.getRoster().setLeader(m1);
		
		
		
		
	}
	
	public void kickMemberRoster(Membre m) throws Exception {
		Session s = HibernateUtil.getSessionFactory().openSession();
		kickMemberRoster(m, s);
		s.close();
	}

	public void kickMemberRoster(Membre m, Session s) throws Exception {
		s.beginTransaction();
		try {
			kickMemberRoster(m, s,  s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
			throw e;
		}
	}

	public void kickMemberRoster(Membre m, Session s, Transaction t) {
		Membre m1 = (Membre) s.load(Membre.class, m.getId());
		m1.getRoster().getMembres().remove(m1);
		m1.setRoster(null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
	public List<Roster> getAll() {
		return rosterDao.findAll();
	}
	
	public List<Roster> getByNom(String nom) {
		return rosterDao.findByNom(nom);
	}
	
	public Roster getById (int id) {
		return rosterDao.findById(id);
	}
	
	public Roster getByNomStrict(String nom) {
		return rosterDao.findByNomStrict(nom);
	}
	
	
	
	
	
	
	
	public void ajouterRosterRaid(Roster r, Raid ra) throws Exception {
		rosterDao.ajouterRosterRaid(r, ra);
	}
}
