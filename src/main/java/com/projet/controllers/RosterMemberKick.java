package com.projet.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Membre;
import com.projet.services.MembreService;
import com.projet.services.RosterService;


@WebServlet("/rostermemberkick")
public class RosterMemberKick extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private MembreService ms = new MembreService();
	private RosterService rs =new RosterService();
    
    public RosterMemberKick() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Membre m = (Membre) request.getSession().getAttribute("user");
		List<Membre> lm = new ArrayList<Membre>();
		for(Membre me : m.getRoster().getMembres()) 
			if(m.getId() != me.getId()) 
				lm.add(me);
		request.setAttribute("listeMembre", lm);
		request.getRequestDispatcher("WEB-INF/jsp/rosterMemberKick.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;
		Membre lead = (Membre) request.getSession().getAttribute("user");
		Membre oldM = ms.getById(Integer.parseInt(request.getParameter("id")));
		
		try {
			rs.kickMemberRoster(oldM);
		} catch (Exception e) {
			System.out.println("member kick fail");
			System.out.println(e);
			error = true;
		}
		
		lead.getRoster().getMembres().remove(oldM);
		
		if(error) {
			request.setAttribute("errorMessage", "Server error");
			request.getRequestDispatcher("WEB-INF/jsp/rosterMemberKick.jsp").forward(request, response);
		}
		else {
			
			request.getSession().setAttribute("user", lead);
			request.getRequestDispatcher("WEB-INF/jsp/index.jsp").forward(request, response);
		}
		
	}

}
