package com.projet.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Membre;
import com.projet.services.MembreService;
import com.projet.services.RosterService;


@WebServlet("/addmemberroster")
public class AddMemberRosterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private MembreService ms = new MembreService();
	private RosterService rs = new RosterService();

	public AddMemberRosterServlet() {
		super();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Membre m = (Membre) request.getSession().getAttribute("user");
		List<Membre> lm = ms.getAll();
		List<Membre> lm2 = new ArrayList<Membre>();
		for(Membre membre : lm)
			if(membre.getRoster() == null) lm2.add(membre);
		for(Membre mm : lm2) System.out.println(mm.getPrenom()+" "+mm.getNom());
		request.setAttribute("listeMembre", lm2);
		request.getRequestDispatcher("WEB-INF/jsp/addMemberRoster.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;
		Membre m = new Membre();
		Membre leader = (Membre) request.getSession().getAttribute("user");
		String iD = request.getParameter("id");


		if(!iD.equals("None")) {
			int id = Integer.parseInt(request.getParameter("id"));		
			m = ms.getById(id);

			leader.getRoster().getMembres().add(m);
			m.setRoster(leader.getRoster());

			// enregistrement
			if(!error ) {
				try {
					rs.ajouterRosterMembre(leader.getRoster(), m);
				} catch(Exception e) {
					request.setAttribute("errorMessage", "Server error");
					error = true;
				}
			}
		}
		else error = true;


		List<Membre> lm = ms.getAll();
		List<Membre> lm2 = new ArrayList<Membre>();
		for(Membre membre : lm)
			if(membre.getRoster() == null) lm2.add(membre);
		for(Membre mm : lm2) System.out.println(mm.getPrenom()+" "+mm.getNom());
		request.setAttribute("listeMembre", lm2);

		System.out.println("SALUT LOL");

		// navigation
		if (error) {
			System.out.println("fail");
			request.getRequestDispatcher("WEB-INF/jsp/addMemberRoster.jsp").forward(request, response);
		} else {
			System.out.println("success");
			request.setAttribute("addMemberSuccess", m.getPrenom()+" "+m.getNom()+" has been successfully added to roster "+leader.getRoster().getNom());
			request.getRequestDispatcher("WEB-INF/jsp/addMemberRoster.jsp").forward(request, response);
		}

	}

}
