package com.projet.controllers;

import java.io.IOException;

import javax.persistence.PersistenceException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.projet.models.Membre;
import com.projet.models.Prio;
import com.projet.services.MembreService;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	MembreService ms = new MembreService();



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Membre m = null;
		try {
			m = ms.getByUsernamePasssword(request.getParameter("username"),
						  request.getParameter("password"));
		} catch(PersistenceException e) {
			request.setAttribute("errorMessage", "Unable to connect to database. Please try again later..");
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
		}
//		System.out.println("User: " + m);
//		for(Prio p : m.getPrios()) System.out.println(p);
		
		System.out.println(m);
		
		if (m == null) {
			request.setAttribute("errorMessage", "Username or password invalid.");
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
		} else {
			HttpSession session = request.getSession();
			session.setAttribute("user", m);
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
		}
	}

}
