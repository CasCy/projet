package com.projet.controllers;

import java.io.IOException;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Membre;
import com.projet.models.Raid;
import com.projet.models.Roster;
import com.projet.services.RosterService;

@WebServlet("/newroster")
public class RosterNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	RosterService rs = new RosterService();


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/newroster.jsp").forward(request, response);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;

		Membre leader = (Membre) request.getSession().getAttribute("user");

		Roster newR = new Roster(
				request.getParameter("rostername"),
				new HashSet<Membre>(),
				leader,
				new HashSet<Raid>());

		System.out.println(newR);

		// validation
		if (newR.getNom().isEmpty() || newR.getNom().length() < 4) {
			request.setAttribute("errorMessage", "Invalid input.");
			error = true;
		}

		// enregistrement
		if(!error ) {
			try {
				rs.ajouterRoster(newR, leader);
			} catch(Exception e) {
				request.setAttribute("errorMessage", "Server error");
				error = true;
			}
		}


		// navigation
		if (!error) {
			leader.setRoster(newR);
			request.setAttribute("user", leader);
			request.setAttribute("rosterSuccess", "Your roster has been createded successfully");
			request.getRequestDispatcher("WEB-INF/jsp/newroster.jsp").forward(request, response);
		} else {			
			request.getRequestDispatcher("WEB-INF/jsp/newroster.jsp").forward(request, response);
		};
	}

}
