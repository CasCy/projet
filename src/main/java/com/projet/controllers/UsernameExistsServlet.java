package com.projet.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.services.MembreService;

/**
 * Servlet implementation class UsernameExists
 */
@WebServlet("/usernameexists")
public class UsernameExistsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MembreService ms = new MembreService();
   
    public UsernameExistsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String un = request.getParameter("username");
		if (ms.getByUsername(un) != null )
			response.getWriter().write("true");
		else
			response.getWriter().write("false");
	}

	

}
