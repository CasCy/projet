package com.projet.controllers;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Attrib;
import com.projet.models.Craft;
import com.projet.models.Item;
import com.projet.models.Job;
import com.projet.models.Role;
import com.projet.models.Slot;
import com.projet.models.Source;
import com.projet.models.Stats;
import com.projet.services.ItemService;
import com.projet.services.StatsService;

@WebServlet("/newitem")
public class ItemNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	ItemService is = new ItemService();
	StatsService ss = new StatsService();



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/newItem.jsp").forward(request, response);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;
		boolean exists = false;

		Item item = new Item (
				request.getParameter("name"),
				Integer.parseInt(request.getParameter("ilvl")),
				null,
				null,
				null,
				null,
				null,
				null,
				request.getParameter("img"),
				Craft.valueOf(request.getParameter("repair")),
				Integer.parseInt(request.getParameter("physick")),
				Integer.parseInt(request.getParameter("magic")));

		String slot = request.getParameter("slot");
		String[] jobs = request.getParameterValues("job");
		String[] stats = request.getParameterValues("stat");
		String[] valeurs = request.getParameterValues("value");

		System.out.println("---------------------");
		System.out.println(Integer.parseInt(request.getParameter("ilvl")));
		System.out.println(slot);
		for(String s : jobs) System.out.println(s);
		for(int i=0; i<stats.length; i++) System.out.println(stats[i]+" "+valeurs[i]+"      OK");


		//CREATION + TEST + AJOUT LIST JOBS
		Set<Job> listjobs = new HashSet<Job>();					
		for (String s : jobs) {
			if (s != "None") {
				Job j = Job.valueOf(s);
				if (listjobs.size() == 0) {
					listjobs.add(j);
				} else {
					exists = false;
					for(Job job : listjobs) if(job == j) exists = true;
					if(!exists) listjobs.add(j);
				}
			}
		}
		item.setJobs(listjobs);


		//CREATION + TEST + AJOUT SLOT	
		if (slot != "None") {
			Slot sl = Slot.valueOf(slot);
			item.setSlot(sl);
		}

		//CREATION + TEST + AJOUT SOURCE	
		String source = request.getParameter("source");
		if (source != "None") {
			Source src = Source.valueOf(source);
			item.setSource(src);
		} else {
			request.setAttribute("errorMessage", "Pick a source !");
			error = true;
		}

		//CREATION + AJOUT STATS <----> ITEM
		Set<Stats> liststats = new HashSet<Stats>();
		for( int i = 0 ; i<stats.length; i++) {
			if(stats[i] != "None") {
				Stats s = new Stats();
				s.setAttrib(Attrib.valueOf(stats[i]));
				s.setValeur(Integer.parseInt(valeurs[i]));
				if(liststats.size() == 0) {
					liststats.add(s);
				} else {
					exists = false;
					for(Stats st : liststats) if(s.getAttrib() == st.getAttrib()) exists = true;
					if(!exists) liststats.add(s);
				}
			}
		}
		item.setStats(liststats);

		System.out.println(item);

		// enregistrement
		if(!error ) {
			try {
				System.out.println("test écriture");
				ss.ajouterStats(liststats);
				is.ajouterItem(item);
			} catch(Exception e) {
				System.out.println("fail écriture ...");
				request.setAttribute("errorMessage", "Server error");
				error = true;
			}
		}

		// navigation
		if (error) {
			System.out.println("HODOR FAIL");
			request.setAttribute("editedItem", item);
			request.getRequestDispatcher("WEB-INF/jsp/newItem.jsp").forward(request, response);
		} else {
			System.out.println("add DB ok");
			request.setAttribute("itemSuccess", "Item added");
			request.getRequestDispatcher("WEB-INF/jsp/newItem.jsp").forward(request, response);
		};
	}

}
