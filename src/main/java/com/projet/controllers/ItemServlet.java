package com.projet.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Item;
import com.projet.models.Job;
import com.projet.models.ListeVoeux;
import com.projet.models.Membre;
import com.projet.models.Slot;
import com.projet.models.Source;
import com.projet.models.Stats;
import com.projet.services.ItemService;


@WebServlet("/item")
public class ItemServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ItemService is = new ItemService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;	
		boolean inWL = false;
		boolean exists = false;
		int id = Integer.parseInt(request.getParameter("id"));
		Membre m = (Membre) request.getSession().getAttribute("user");

		Item item = is.getById(id);
		List<Membre> lm = new ArrayList<Membre>();

		if(m != null) {
			for(Item i : m.getListeVoeux().getItems()) {
				if(item.getId() == i.getId()) 
					inWL=true;
			}


			for(ListeVoeux lv : item.getListeVoeux()) {
				exists = false;
					for(Item i : lv.getItems()) {
							if(i == item)
								exists = true;
						}
				if(exists) lm.add(lv.getMembre());
			}
		}

		request.setAttribute("listeMembreWL", lm);

		for(Membre mmm : lm) System.out.println(mmm.getPrenom()+" "+mmm.getNom()+"\n");

		System.out.println(inWL);

		//item.getStats().sort(Comparator.comparing(Stats::getAttrib));

		System.out.println(item);

		if (error) {
			request.setAttribute("item", item);
			request.getRequestDispatcher("WEB-INF/jsp/item.jsp").forward(request, response);
		} else {
			request.setAttribute("inWL", inWL);
			request.setAttribute("item", item);
			request.setAttribute("user", m);
			request.getRequestDispatcher("WEB-INF/jsp/item.jsp").forward(request, response);
		}

	}

}
