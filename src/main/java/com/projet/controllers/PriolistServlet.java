package com.projet.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.projet.models.Membre;
import com.projet.models.Prio;
import com.projet.services.MembreService;

@WebServlet("/priolist")
public class PriolistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private MembreService ms = new MembreService();



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/priolist.jsp").forward(request, response);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;
		
		Membre m = (Membre) request.getSession().getAttribute("user");

		List<Prio> lp = new ArrayList<Prio>();
		Prio p1 = Prio.valueOf(request.getParameter("prio1"));
		Prio p2 = Prio.valueOf(request.getParameter("prio2"));
		Prio p3 = Prio.valueOf(request.getParameter("prio3"));
		Prio p4 = Prio.valueOf(request.getParameter("prio4"));
		Prio p5 = Prio.valueOf(request.getParameter("prio5"));

		lp.add(p1);
		lp.add(p2);
		lp.add(p3);
		lp.add(p4);
		lp.add(p5);
		
		//enregistrement
		if(!error ) {
			try {
				System.out.println("test écriture");
				ms.ajouterMembrePrios(m, lp);
			} catch(Exception e) {
				System.out.println("fail écriture ...");
				request.setAttribute("errorMessage", "Server error");
				error = true;
			}
		}

		System.out.println(m);
		
		// navigation
		if (error) {
			System.out.println("HODOR FAIL");
			request.getRequestDispatcher("WEB-INF/jsp/priolist.jsp").forward(request, response);
		} else {
			m.setPrios(lp);
			request.getSession().setAttribute("user", m);
			System.out.println("add DB ok");
			request.setAttribute("prioSuccess", "Priolist added");
			request.getRequestDispatcher("WEB-INF/jsp/index.jsp").forward(request, response);
		};
	}

}
