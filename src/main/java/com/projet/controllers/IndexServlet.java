package com.projet.controllers;

import java.io.IOException;

import javax.persistence.PersistenceException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.projet.models.Membre;
import com.projet.services.MembreService;

@WebServlet("/index")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	MembreService ms = new MembreService();



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Membre m = (Membre) request.getSession().getAttribute("user");
		request.setAttribute("editedUser", m);
		request.setAttribute("user", m);
		response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
	}
	
}
