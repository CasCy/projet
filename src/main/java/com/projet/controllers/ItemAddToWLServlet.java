package com.projet.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Item;
import com.projet.models.Membre;
import com.projet.services.ItemService;
import com.projet.services.MembreService;


@WebServlet("/addToWl")
public class ItemAddToWLServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ItemService is = new ItemService();
	private MembreService ms = new MembreService();


	public ItemAddToWLServlet() {
		super();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error=false;

		Membre m = (Membre) request.getSession().getAttribute("user");
		int id = Integer.parseInt(request.getParameter("id"));
		Item i = is.getById(id);
		
		System.out.println(m);

		try {
			is.addItemToListeVoeux(i, m);
		} catch (Exception e) {
			request.setAttribute("errorMessage", "Server error");
			System.out.println("OOPS");
			System.out.println(e);
			error = true;
		}
		
		m = ms.getById(m.getId());
		i = is.getById(i.getId());

		// navigation
		if (error) {
			request.getRequestDispatcher("WEB-INF/jsp/item.jsp").forward(request, response);
		} else {
			request.setAttribute("item", i);
			request.setAttribute("user", m);
			request.getSession().setAttribute("user", m);
			request.getRequestDispatcher("WEB-INF/jsp/item.jsp").forward(request, response);
		}


	}

}
