package com.projet.controllers;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Job;
import com.projet.models.Membre;
import com.projet.models.Role;
import com.projet.services.MembreService;

@WebServlet("/editpassword")
public class EditLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private MembreService ms = new MembreService();



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Membre m = (Membre) request.getSession().getAttribute("user");
		request.setAttribute("editedUser", m);
		request.getRequestDispatcher("WEB-INF/jsp/editLogin.jsp").forward(request, response);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error=false;

		Membre oldM = (Membre) request.getSession().getAttribute("user");

		if(oldM != null) {
			Membre newM = new Membre (
					oldM.getId(),
					oldM.getPrenom(),
					oldM.getNom(),
					oldM.getEmail(),
					oldM.getUsername(),
					(request.getParameter("newPassword").isEmpty()) ? oldM.getPassword() : request.getParameter("newPassword"),
							oldM.getServeur(),
							oldM.getRoles(),
							oldM.getJobs(),
							oldM.getLoots(),
							oldM.getRoster(),
							oldM.getPrios(),
							oldM.getListeVoeux(),
							oldM.getImg());

			// validation
			if ((!request.getParameter("newPassword").isEmpty()) &&
					(!request.getParameter("oldPassword").equals(oldM.getPassword()))) {
				error = true;
				System.out.println("fail validation");
				request.setAttribute("passwordErrorMessage", "Wrong password");
			}

			// enregistrement
			if(!error ) {
				try {
					System.out.println("-----------------------");
					System.out.println("test maj");
					ms.ajouterMembre(newM);
				} catch(Exception e) {
					request.setAttribute("errorMessage", "Server error");
					error = true;
				}
			} 

			// navigation
			if (error) {
				System.out.println("fail");
				request.setAttribute("editedUser", oldM);
				request.getRequestDispatcher("WEB-INF/jsp/editLogin.jsp").forward(request, response);
			} else {
				System.out.println("success");
				request.getSession().setAttribute("user", newM);
				request.setAttribute("loginInfoSuccess", "Your password has been successfully changed");
				request.getRequestDispatcher("WEB-INF/jsp/editLogin.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("offline", "Please log in");
			request.getRequestDispatcher("WEB-INF/jsp/editLogin.jsp").forward(request, response);
		}
		
	}

}