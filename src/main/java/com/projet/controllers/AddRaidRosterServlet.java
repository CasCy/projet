package com.projet.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.projet.models.Item;
import com.projet.models.Loot;
import com.projet.models.Membre;
import com.projet.models.Raid;
import com.projet.services.ItemService;
import com.projet.services.MembreService;
import com.projet.services.RaidService;


@WebServlet("/addraidroster")
public class AddRaidRosterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private MembreService ms = new MembreService();
	private ItemService is = new ItemService();
	private RaidService rs = new RaidService();

	public AddRaidRosterServlet() {
		super();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Membre m = (Membre) request.getSession().getAttribute("user");
		List<Membre> lm = ms.getByRoster(m.getRoster());
		List<Item> li = is.getAll();
		li.sort(Comparator.comparing(Item::getSlot));		
		for(Membre mm : lm) System.out.println(mm.getPrenom()+" "+mm.getNom());
		request.setAttribute("listeMembre", lm);
		request.setAttribute("listeItems", li);
		request.getRequestDispatcher("WEB-INF/jsp/addRaidRoster.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("COUCOU");

		
		boolean error = false;
		Membre m = (Membre) request.getSession().getAttribute("user");
		String[] items = request.getParameterValues("itemid");
		String[] membres = request.getParameterValues("memberid");

		
		Raid raid = new Raid(
				LocalDate.now(),
				m.getRoster()
				);

		if(items != null) {
			for(int i=0; i<items.length; i++) {
				Loot loot= new Loot(is.getById(Integer.parseInt(items[i])), ms.getById(Integer.parseInt(membres[i])), raid);
				raid.getLoots().add(loot);
				loot.setRaid(raid);
				Membre me = ms.getById(Integer.parseInt(membres[i]));
				Item it = is.getById(Integer.parseInt(items[i]));
				try {
					is.remItemToListeVoeux(it, me);
				} catch (Exception e) {
					System.out.println(e);
					System.out.println("fail remoWL"+it.getNom()+" "+me.getPrenom()+" "+me.getNom());
				}							
			}
		}

		try {
			rs.ajouterRaid(raid);
		} catch (Exception e) {
			request.setAttribute("errorMessage", "Server error");
			error = true;
			System.out.println(e);
		}


		System.out.println("SALUT LOL");
		
		List<Membre> lm = ms.getByRoster(m.getRoster());
		List<Item> li = is.getAll();
		li.sort(Comparator.comparing(Item::getSlot));		
		request.setAttribute("listeMembre", lm);
		request.setAttribute("listeItems", li);
		
		m = ms.getById(m.getId());

		// navigation
		if (error) {
			System.out.println("fail");
			request.getRequestDispatcher("WEB-INF/jsp/addRaidRoster.jsp").forward(request, response);
		} else {
			HttpSession session = request.getSession();
			session.setAttribute("user", m);
			System.out.println("success");
			request.setAttribute("addRaidSuccess", "Raid has been successfully added to roster ");
			request.getRequestDispatcher("WEB-INF/jsp/addRaidRoster.jsp").forward(request, response);
		}

	}

}
