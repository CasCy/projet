package com.projet.controllers;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Job;
import com.projet.models.Membre;
import com.projet.models.Role;
import com.projet.services.MembreService;

@WebServlet("/editmember")
public class EditMembreServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private MembreService ms = new MembreService();



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Membre m = (Membre) request.getSession().getAttribute("user");
		request.setAttribute("editedUser", m);
		request.getRequestDispatcher("WEB-INF/jsp/editMemberInfo.jsp").forward(request, response);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error=false;

		Membre oldM = (Membre) request.getSession().getAttribute("user");

		if(oldM != null) {
			Membre newM = new Membre (
					oldM.getId(),
					request.getParameter("prenom"),
					request.getParameter("nom"),
					oldM.getEmail(),
					oldM.getUsername(),
					oldM.getPassword(),
					request.getParameter("serveur"),
					oldM.getLoots(),
					oldM.getRoster(),
					oldM.getPrios(),
					oldM.getListeVoeux(),
					oldM.getImg());

			String[] jobs = request.getParameterValues("job");
			String[] roles = request.getParameterValues("role");

			System.out.println("---------------------");
			for(String s : jobs) System.out.println(s);
			for(String s : roles) System.out.println(s);


			//CREATION + TEST + AJOUT LIST JOBS
			Set<Job> listjobs = new HashSet<Job>();					
			for (String s : jobs) {
				if (s != "None") {
					Job j = null;
					switch(s) {
					case "Paladin": j = Job.Paladin; break; 
					case "DarkKnight": j = Job.DarkKnight; break; 
					case "Gunbreaker": j = Job.Gunbreaker; break; 
					case "Warrior": j = Job.Warrior; break; 
					case "WhiteMage": j = Job.WhiteMage; break; 
					case "Astrologian": j = Job.Astrologian; break; 
					case "Scholar": j = Job.Scholar; break; 
					case "Bard": j = Job.Bard; break; 
					case "Machinist": j = Job.Machinist; break; 
					case "Dancer": j = Job.Dancer; break; 
					case "BlackMage": j = Job.BlackMage; break; 
					case "Summoner": j = Job.Summoner; break; 
					case "RedMage": j = Job.RedMage; break; 
					case "Ninja": j = Job.Ninja; break; 
					case "Dragoon": j = Job.Dragoon; break; 
					case "Monk": j = Job.Monk; break; 
					case "Samurai": j = Job.Samurai; break; 
					default: break;
					}					
					boolean exists = false;
					for(Job job : listjobs) if(job == j) exists = true;
					if(!exists) listjobs.add(j);					
				}
			}
			newM.setJobs(listjobs);


			//CREATION + TEST + AJOUT LIST ROLES
			Set<Role> listroles = new HashSet<Role>();					
			for (String s : roles) {
				if (s != "None") {
					Role r = null;
					switch(s) {
					case "Tank": r = Role.Tank; break; 
					case "Heal": r = Role.Heal; break; 
					case "Dps": r = Role.Dps; break; 
					default: break;
					}
					if (listroles.size() == 0) {
						listroles.add(r);
					} else {
						boolean exists = false;
						for(Role role : listroles) if(role == r) exists = true;
						if(!exists) listroles.add(r);
					}
				}
			}
			newM.setRoles(listroles);



			// enregistrement
			if(!error ) {
				try {
					ms.ajouterMembre(newM);
				} catch(Exception e) {
					request.setAttribute("errorMessage", "Server error");
					error = true;
				}
			}

			// navigation
			if (error) {
				request.setAttribute("editedUser", oldM);
				request.getRequestDispatcher("WEB-INF/jsp/editMemberInfo.jsp").forward(request, response);
			} else {
				request.getSession().setAttribute("user", newM);
				request.setAttribute("charInfoSuccess", "Your character has been successfully changed");
				request.getRequestDispatcher("WEB-INF/jsp/editMemberInfo.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("offline", "Please log in");
			request.getRequestDispatcher("WEB-INF/jsp/editMemberInfo.jsp").forward(request, response);
		}
	}

}