package com.projet.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Membre;
import com.projet.models.Roster;
import com.projet.services.RosterService;


@WebServlet("/viewroster")
public class RosterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private RosterService rs = new RosterService();
       
  
    public RosterServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Membre m = (Membre) request.getSession().getAttribute("user");
		Roster r = rs.getById(m.getRoster().getId());
		List<Membre> lm = new ArrayList<Membre>();
		for(Membre mm : r.getMembres()) lm.add(mm);
		lm.sort(Comparator.comparing(Membre::getPrenom));
		
		request.setAttribute("leader", r.getLeader());
		request.setAttribute("listeMembre", lm);
		request.getRequestDispatcher("WEB-INF/jsp/roster.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
