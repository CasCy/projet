package com.projet.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.projet.models.Job;
import com.projet.models.ListeVoeux;
import com.projet.models.Membre;
import com.projet.models.Prio;
import com.projet.models.Role;
import com.projet.services.ItemService;
import com.projet.services.ListeVoeuxService;
import com.projet.services.MembreService;

@WebServlet("/newuser")
public class UserNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	MembreService ms = new MembreService();
	ListeVoeuxService lvs = new ListeVoeuxService();
	ItemService is = new ItemService();


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setAttribute("signIn", true);
		request.getRequestDispatcher("WEB-INF/jsp/newuser.jsp").forward(request, response);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;

		Membre newM = new Membre (
				request.getParameter("prenom"),
				request.getParameter("nom"),
				request.getParameter("email"),
				request.getParameter("username"),
				request.getParameter("password"),
				request.getParameter("serveur"));

		String[] jobs = request.getParameterValues("job");
		String[] roles = request.getParameterValues("role");
		
		if(roles.length > 3 || jobs.length > 17) {
			error = true;
			request.setAttribute("errorMessage", "Too many Jobs or Roles");
		}
		
		if( (roles.length < 1 && roles[0].equals("None") || (jobs.length < 1 && roles[0].equals("None")) ) ) {
			error = true;
			request.setAttribute("errorMessage", "Choose at least one Job and Role");
		}
		
		if(!error) {

		System.out.println("---------------------");
		for(String s : jobs) System.out.println(s);
		for(String s : roles) System.out.println(s);


		//CREATION + TEST + AJOUT LIST JOBS
		Set<Job> listjobs = new HashSet<Job>();					
		for (String s : jobs) {
			if (s != "None") {
				Job j = Job.valueOf(s);
				if (listjobs.size() == 0) {
					listjobs.add(j);
				} else {
					boolean exists = false;
					for(Job job : listjobs) if(job == j) exists = true;
					if(!exists) listjobs.add(j);
				}
			}
		}
		newM.setJobs(listjobs);


		//CREATION + TEST + AJOUT LIST ROLES
		Set<Role> listroles = new HashSet<Role>();					
		for (String s : roles) {
			if (s != "None") {
				Role r = Role.valueOf(s);
				if (listroles.size() == 0) {
					listroles.add(r);
				} else {
					boolean exists = false;
					for(Role role : listroles) if(role == r) exists = true;
					if(!exists) listroles.add(r);
				}
			}
		}
		newM.setRoles(listroles);
		
		newM.setListeVoeux(new ListeVoeux());
		System.out.println(newM);
		
		// validation
		if (newM.getUsername().isEmpty() ||
				(newM.getPassword().length() < 8 ||
						(newM.getPassword().isEmpty() ||
								(newM.getEmail().isEmpty() ||
										(newM.getPrenom().isEmpty() ||
												(newM.getNom().isEmpty())))))) {
			request.setAttribute("errorMessage", "Invalid input.");
			error = true;
		}
		
		}
		
		

		// enregistrement
		if(!error ) {
			try {
				ms.ajouterMembre(newM);
			} catch(Exception e) {
				request.setAttribute("errorMessage", "Server error");
				error = true;
			}
		}

		newM.setPrios(new ArrayList<Prio>());
		System.out.println(newM.getPrios().size());

		// navigation
		if (!error) {
			HttpSession session = request.getSession();
			session.setAttribute("user", newM);
			request.setAttribute("charInfoSuccess", "Your character has been updated successfully");
			request.getRequestDispatcher("WEB-INF/jsp/index.jsp").forward(request, response);
		} else {
			request.setAttribute("editedUser", newM);
			request.setAttribute("signIn", true);
			request.getRequestDispatcher("WEB-INF/jsp/newuser.jsp").forward(request, response);
		};
	}

}
