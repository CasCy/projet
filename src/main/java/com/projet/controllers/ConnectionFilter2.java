package com.projet.controllers;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Membre;

@WebFilter({"/editpassword", "/editmember", "/viewWL"})
public class ConnectionFilter2 implements Filter {
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		Membre m = (Membre) req.getSession().getAttribute("user");
		
				
		if (m == null) {
			System.out.println("Unauthorized access request");
			res.sendRedirect(res.encodeRedirectURL(req.getContextPath() + "/"));
		} 
		else {
			chain.doFilter(request, response);
		}
	}
}