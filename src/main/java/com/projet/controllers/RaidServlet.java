package com.projet.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Item;
import com.projet.models.Membre;
import com.projet.models.Raid;
import com.projet.services.RaidService;


@WebServlet("/viewraids")
public class RaidServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private RaidService rs = new RaidService();
       
    
    public RaidServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Membre m = (Membre) request.getSession().getAttribute("user");
		List<Raid> lrtmp = rs.getByRoster(m.getRoster());
		lrtmp.sort(Comparator.comparing(Raid::getDate));
		List<Raid> lr = new ArrayList<Raid>();
		for(int i=0; i<lrtmp.size(); i++) lr.add(lrtmp.get(lrtmp.size()-1-i));
		
		request.setAttribute("member", m);
		request.setAttribute("raids", lr);
		request.getRequestDispatcher("WEB-INF/jsp/raids.jsp").forward(request, response);
	}	

}
