package com.projet.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Membre;
import com.projet.services.MembreService;
import com.projet.services.RosterService;


@WebServlet("/rosterleadswap")
public class RosterLeadSwapServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private MembreService ms = new MembreService();
	private RosterService rs =new RosterService();
    
    public RosterLeadSwapServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Membre m = (Membre) request.getSession().getAttribute("user");
		List<Membre> lm = new ArrayList<Membre>();
		for(Membre me : m.getRoster().getMembres()) lm.add(me);
		request.setAttribute("listeMembre", lm);
		request.getRequestDispatcher("WEB-INF/jsp/rosterLeadSwap.jsp").forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;
		Membre oldL = (Membre) request.getSession().getAttribute("user");
		Membre newL = ms.getById(Integer.parseInt(request.getParameter("id")));
		
		try {
			rs.swapRosterLead(newL);
		} catch (Exception e) {
			System.out.println("lead swap fail");
			System.out.println(e);
			error = true;
		}
		
		oldL.getRoster().setLeader(newL);
		
		if(error) {
			request.setAttribute("errorMessage", "Server error");
			request.getRequestDispatcher("WEB-INF/jsp/rosterLeadSwap.jsp").forward(request, response);
		}
		else {
			
			request.getSession().setAttribute("user", oldL);
			request.getRequestDispatcher("WEB-INF/jsp/index.jsp").forward(request, response);
		}
		
	}

}
