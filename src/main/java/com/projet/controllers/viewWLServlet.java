package com.projet.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Item;
import com.projet.models.Membre;

@WebServlet("/viewWL")
public class viewWLServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public viewWLServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Membre m = (Membre) request.getSession().getAttribute("user");
		List<Item> li = new ArrayList<Item>();
		for(Item i: m.getListeVoeux().getItems()) li.add(i);
		li.sort(Comparator.comparing(Item::getNom));
		request.setAttribute("listeItems", li);
		request.setAttribute("user", ((Membre) request.getSession().getAttribute("user")));
		request.getRequestDispatcher("WEB-INF/jsp/viewWL.jsp").forward(request, response);

	}

}
