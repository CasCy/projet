package com.projet.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Membre;
import com.projet.services.RosterService;

/**
 * Servlet implementation class leaveroster
 */
@WebServlet("/leaveroster")
public class LeaveRosterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private RosterService rs= new RosterService();

	
	public LeaveRosterServlet() {
		super();
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;
		
		Membre m =(Membre) request.getSession().getAttribute("user");
		
		try {
			rs.remRosterMembre(m.getRoster(), m);
		} catch (Exception e) {
			System.out.println("remove fail");
			System.out.println(e);
			error = true;
		}
		
		m.getRoster().getMembres().remove(m);
		m.setRoster(null);
		
		if(error) {
			request.setAttribute("errorMessage", "Server error");
			request.getRequestDispatcher("WEB-INF/jsp/index.jsp").forward(request, response);
		}
		else {
			
			request.getSession().setAttribute("user", m);
			request.getRequestDispatcher("WEB-INF/jsp/index.jsp").forward(request, response);
		}

	}

}
