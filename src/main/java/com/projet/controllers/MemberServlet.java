package com.projet.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Item;
import com.projet.models.Loot;
import com.projet.models.Membre;
import com.projet.services.LootService;
import com.projet.services.MembreService;


@WebServlet("/member")
public class MemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	private MembreService ms = new MembreService();
	private LootService ls = new LootService();
	
    public MemberServlet() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		Membre m = ms.getById(id);
		List<Item> li = new ArrayList<Item>();
		List<Loot> ll = ls.getAll();
		for(Loot l : ll) 
			if(l.getMembre().getId() == m.getId()) 
				li.add(l.getItem());
		request.setAttribute("member", m);
		request.setAttribute("itemLoot", li);
		request.getRequestDispatcher("WEB-INF/jsp/member.jsp").forward(request, response);
	}

}
