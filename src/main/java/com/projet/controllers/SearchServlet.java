package com.projet.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projet.models.Item;
import com.projet.models.Job;
import com.projet.models.Slot;
import com.projet.models.Source;
import com.projet.services.ItemService;


@WebServlet("/search")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ItemService is = new ItemService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;	
		String searched = request.getParameter("itemSearch");

		int min = Integer.parseInt(request.getParameter("ilvlmin"));
		int max = Integer.parseInt(request.getParameter("ilvlmax"));
		String slot = request.getParameter("slot");
		String job = request.getParameter("job");
		Slot slo;
		Job jo;
		System.out.println(searched);
		System.out.println(("min = "+min+"\nmax = "+max));
		System.out.println(slot);
		System.out.println(job);
		
		List<Item> li = new ArrayList<Item>();
		if (slot.equals("All") && job.equals("All")) {
			li = is.getByNameIlvl(searched, min, max);
		}
		if(slot.equals("All") && !job.equals("All")) {
			jo = Job.valueOf(job);
			li = is.getByNameIlvlJob(searched, min, max, jo);
		}
		if (!slot.equals("All") && job.equals("All")) {
			slo = Slot.valueOf(slot);
			li = is.getByNameIlvlSlot(searched, min, max, slo);
		}
		if (!slot.equals("All") && !job.equals("All")) {
			jo = Job.valueOf(job);
			slo = Slot.valueOf(slot);
			li = is.getByNameIlvlSlotJob(searched, min, max, slo, jo);
		}
		
		
		
		li.sort(Comparator.comparing(Item::getNom));
		
		
		if (li.size() == 0) {
			error = true;
			System.out.println("HODOR");
		}
		
		
		
		else for (Item i : li) System.out.println(i);
		if (!error) {
			request.setAttribute("searchedItem", li);
			request.getRequestDispatcher("WEB-INF/jsp/index.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("WEB-INF/jsp/index.jsp").forward(request, response);
		}

	}

}
