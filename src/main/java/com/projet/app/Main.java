package com.projet.app;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.projet.models.Attrib;
import com.projet.models.Item;
import com.projet.models.Job;
import com.projet.models.Membre;
import com.projet.models.Prio;
import com.projet.models.Raid;
import com.projet.models.Role;
import com.projet.models.Roster;
import com.projet.models.Slot;
import com.projet.models.Stats;
import com.projet.services.ItemService;
import com.projet.services.MembreService;
import com.projet.services.RaidService;
import com.projet.services.RosterService;
import com.projet.services.StatsService;


public class Main {

	public static void main(String[] args) throws Exception {

		MembreService ms = new MembreService();
		RosterService rs = new RosterService();
		RaidService ras = new RaidService();
		StatsService ss = new StatsService();
		ItemService is = new ItemService();

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
//		Role rh[] = { Role.Heal }; Set<Role> roleh = new HashSet<Role>(Arrays.asList(rh));
//		Role rt[] = { Role.Tank }; Set<Role> rolet = new HashSet<Role>(Arrays.asList(rt));
//		Role rd[] = { Role.Dps }; Set<Role> roled = new HashSet<Role>(Arrays.asList(rd));
//		Role rht[] = { Role.Heal , Role.Tank }; Set<Role> roleht = new HashSet<Role>(Arrays.asList(rht));
//		Role rhd[] = { Role.Heal , Role.Dps }; Set<Role> rolehd = new HashSet<Role>(Arrays.asList(rhd));
//		Role rtd[] = { Role.Dps , Role.Tank}; Set<Role> roletd = new HashSet<Role>(Arrays.asList(rtd));
//		Role rhtd[] = { Role.Heal , Role.Tank , Role.Dps }; Set<Role> rolehtd = new HashSet<Role>(Arrays.asList(rhtd));
//
//		Job j1[] = { Job.WhiteMage , Job.Bard , Job.Paladin }; Set<Job> jobJoph = new HashSet<Job>(Arrays.asList(j1));
//		Job j2[] = { Job.RedMage , Job.BlackMage , Job.Summoner , Job.Machinist , Job.Scholar }; Set<Job> jobEvrae = new HashSet<Job>(Arrays.asList(j2));
//		Job j3[] = { Job.Bard , Job.Astrologian }; Set<Job> jobShiv = new HashSet<Job>(Arrays.asList(j3));
//		
//		Job ji1[] = {Job.WhiteMage, Job.Astrologian, Job.Scholar}; Set<Job> jobi1 = new HashSet<Job>(Arrays.asList(ji1));
//		Job ji2[] = {Job.DarkKnight, Job.Warrior, Job.Paladin}; Set<Job> jobi2 = new HashSet<Job>(Arrays.asList(ji2));
//		Job ji3[] = {Job.WhiteMage}; Set<Job> jobi3 = new HashSet<Job>(Arrays.asList(ji3));
//		Job ji4[] = {Job.WhiteMage, Job.Astrologian, Job.Scholar}; Set<Job> jobi4 = new HashSet<Job>(Arrays.asList(ji4));
//		Job ji5[] = {Job.BlackMage}; Set<Job> jobi5 = new HashSet<Job>(Arrays.asList(ji5));
//
//		Prio p1[] = {Prio.Heal, Prio.Tank, Prio.Ranged}; List<Prio> prio1 = Arrays.asList(p1);
//		Prio p2[] = {Prio.Caster, Prio.Heal, Prio.MnkSam}; List<Prio> prio2 = Arrays.asList(p2);
////		Prio p1[] = {Prio.Heal, Prio.Tank, Prio.Ranged}; Set<Prio> prio1 = new HashSet<Prio>(Arrays.asList(p1));
////		Prio p2[] = {Prio.Caster, Prio.Heal, Prio.MnkSam}; Set<Prio> prio2 = new HashSet<Prio>(Arrays.asList(p2));
//
//		Roster orion = new Roster("Orion");
//		Roster ygg = new Roster("Yggdrasil");
//
//		Raid raid1 = new Raid(LocalDateTime.now(), null);
//		
//		Stats stats1 = new Stats(Attrib.CriticalHit, 245);
//		Stats stats2 = new Stats(Attrib.DirectHit, 290);
//		Stats stats3 = new Stats(Attrib.Mind, 345);
//		Stats st1[] = {stats1, stats2}; Set<Stats> itemset1 = new HashSet<Stats>(Arrays.asList(st1));
//		
//		
//		Item i1 = new Item("Diamond Coat of Healing", 370, Slot.Body, jobi1, null, null, null);
//		Item i2 = new Item("Diamond Coat of Fending", 370, Slot.Body, jobi2, null, null, null);
//				
//		ras.ajouterRaid(raid1);
//		ras.ajouterRaid(LocalDateTime.now(), null);
//		
//		ss.ajouterStats(Attrib.Determination, 315);
//		ss.ajouterStats(stats1);
//		ss.ajouterStats(stats2);
//		ss.ajouterStats(stats3);
//		
//		is.ajouterItem(i1);
//		is.ajouterItem("Diamond Cane", 375, Slot.Weapon, jobi3, null, null, null);
//		is.ajouterItem(i2);
//		is.ajouterItem("Diamond GreatSword", 373, Slot.Weapon, jobi4, null, null, null);
//
//		rs.ajouterRoster("Orion", null, null);
//		rs.ajouterRoster("Yggdrasil", null, null);
//
//		ms.ajouterMembre("Joph", "Stalk", "Jophmail@gmail.com" ,"jophlogin", "jophpwd", "Ragnarok", roleht , jobJoph, null, null, null, null);
//		ms.ajouterMembre("Evrae", "Ashitar'ha", "Evraemail@gmail.com" ,"evlogin", "evpwd", "Ragnarok", roled , jobEvrae, null, null, null, null);
//		ms.ajouterMembre("Shiverna", "Eleddrel", "Shivmail@gmail.com" ,"shivlogin", "shivpwd", "Moogle", rolehd, jobShiv, null, null, null, null);
//
//
//
//		
//		System.out.println("\n\n\t\t/* * * * * *");
//		System.out.println("\t\t * MEMBRE  *");
//		System.out.println("\t\t * * * * * */");
//
//		Membre m;
//		List<Membre> lm;
//
//		System.out.println("\n\n\t---------LISTE DES MEMBRES---------");
//		lm = ms.getAll();
//		if (lm.size()!=0) 
//			for (Membre me : lm ) System.out.println(me);
//		else System.out.println("\t\tRien trouvé");
//		System.out.println("\t\tNombre de résultats : "+lm.size());
//
//		System.out.println("\n\n\t---------ADD + DELETE MEMBRES---[SDF USERNAME]------");
//		ms.ajouterMembre("sdfsdf", "sdfsdfsdf", "sdfff@ss.com" ,"sdf", "fz", "sdf", null, null, null, null, null, null);
//		lm = ms.getAll();
//		if (lm.size()!=0) 
//			for (Membre me : lm ) System.out.println(me);
//		else System.out.println("\t\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lm.size()+"\n\n");
//		ms.supprimerMembre(ms.getByUsername("sdf"));
//		System.out.println("\tEffacé !\n");
//		lm = ms.getAll();
//		if (lm.size()!=0) 
//			for (Membre me : lm ) System.out.println(me);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lm.size());
//
//		System.out.println("\n\n\t---------MEMBRE PAR USERNAME---[evlogin]--------");
//		m = ms.getByUsername("evlogin");
//		System.out.println(m);
//
//		System.out.println("\n\n\t--------MEMBRE PAR UNAME+PWD---[shivlogin-shivpwd]--------");
//		m = ms.getByUsernamePasssword("shivlogin", "shivpwd");
//		System.out.println(m);
//
//		System.out.println("\n\n\t--------MEMBRE PAR PSEUDO---[ta]---------");
//		lm = ms.getByPseudo("ta");
//		if (lm.size()!=0) 
//			for (Membre me : lm ) System.out.println(me);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lm.size());
//
//		System.out.println("\n\n\t--------MEMBRE PAR SERVEUR---[Moogle]---------");
//		lm = ms.getByServeur("Moogle");
//		if (lm.size()!=0) 
//			for (Membre me : lm ) System.out.println(me);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lm.size());
//
//		System.out.println("\n\n\t--------MEMBRE PAR ROLE---[Role.Heal]---------");
//		lm = ms.getByRole(Role.Heal);
//		if (lm.size()!=0) 
//			for (Membre me : lm ) System.out.println(me);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lm.size());
//
//		System.out.println("\n\n\t--------MEMBRE PAR JOB---[Job.Bard]---------");
//		lm = ms.getByJob(Job.Bard);
//		if (lm.size()!=0) 
//			for (Membre me : lm ) System.out.println(me);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lm.size());
//		
//		System.out.println("\n\n\t--------ADD LISTE PRIO <--> MEMBRE ---[jophlogin]---------");
//		ms.ajouterMembreLP(ms.getByUsername("jophlogin"), prio1);
//		System.out.println(ms.getByUsername("jophlogin"));
//		
//		System.out.println("\n\n\t--------ADD LISTE PRIO <--> MEMBRE ---[jophlogin]---------");
//		ms.ajouterMembreLP(ms.getByUsername("jophlogin"), prio1);
//		System.out.println(ms.getByUsername("jophlogin"));
//		
//		System.out.println("\n\n\t--------ADD LISTE PRIO <--> MEMBRE ---[jophlogin]---------");
//		ms.ajouterMembreLP(ms.getByUsername("jophlogin"), prio1);
//		System.out.println(ms.getByUsername("jophlogin"));
//
//		
//		
//
//		System.out.println("\n\n\t\t/* * * * * *");
//		System.out.println("\t\t * ROSTER  *");
//		System.out.println("\t\t * * * * * */");
//		Roster r;
//		List<Roster> lr;
//
//		System.out.println("\n\n\t--------LISTE DES ROSTER------------");
//		lr = rs.getAll();
//		if (lr.size()!=0) 
//			for (Roster ro : lr ) System.out.println(ro);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lr.size());
//
//		System.out.println("\n\n\t--------ROSTER PAR NOM---[Ori]---------");
//		lr = rs.getByNom("Ori");
//		if (lr.size()!=0) 
//			for (Roster ro : lr ) System.out.println(ro);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lr.size());
//
//		System.out.println("\n\n\t--------ADD MEMBRE ROSTER---[ORION <--> JOPH]---------");
//		rs.ajouterRosterMembre(rs.getByNomStrict("Orion") , ms.getByUsername("jophlogin"));
//		System.out.println("\t Joph ajouté au roster Orion");
//		System.out.println(ms.getByUsername("jophlogin"));
//
//		System.out.println("\n\n\t--------ADD MEMBRE ROSTER---[ORION <--> EVRAE]---------");
//		rs.ajouterRosterMembre(rs.getByNomStrict("Orion") , ms.getByUsername("evlogin"));
//		System.out.println("\t Evrae ajouté au roster Orion");
//		System.out.println(ms.getByUsername("evlogin"));
//
//		System.out.println("\n\n\t--------ADD RAID ROSTER---[RAID ID 1 <--> ORION]---------");
//		rs.ajouterRosterRaid(rs.getByNomStrict("Orion") , ras.getById(1));
//		System.out.println("\t RAID ID "+ras.getById(1).getId()+" ajouté au roster Orion");
//		System.out.println(ras.getById(1));			
//
//		
//		
//
//		System.out.println("\n\n\t\t/* * * * * *");
//		System.out.println("\t\t *  RAID   *");
//		System.out.println("\t\t * * * * * */");
//		Raid raid;
//		List<Raid> lraid;
//
//		System.out.println("\n\n\t---------LISTE DES RAIDS---------");
//		lraid = ras.getAll();
//		if (lraid.size()!=0) 
//			for (Raid ra : lraid ) System.out.println(ra);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lraid.size());
//		
//		System.out.println("\n\n\t---------RAIDS PAR DATE---[DATE=RAID_ID(1).date]------");
//		lraid = ras.getByDate(ras.getById(1).getDate());
//		if (lraid.size()!=0) 
//			for (Raid ra : lraid ) System.out.println(ra);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lraid.size());
//		
//		
//		
//		
//		System.out.println("\n\n\t\t/* * * * * * * * * * *");
//		System.out.println("\t\t *  ROSTER <-> CLASS *");
//		System.out.println("\t\t * * * * * * * * * * */");
//
//		System.out.println("\n\n\t--------GET ROSTER PAR MEMBRE---[JOPH]---------");
//		lr = rs.getByNom(ms.getByUsername("jophlogin").getRoster().getNom());
//		if (lr.size()!=0) 
//			for (Roster ro : lr ) System.out.println(ro);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lr.size());
//		
//		System.out.println("\n\n\t--------GET RAID PAR ROSTER---[ORION]---------");
//		lraid = ras.getByRoster(rs.getById(rs.getByNomStrict("Orion").getId()));
//		if (lraid.size()!=0) 
//			for (Raid ro : lraid ) System.out.println(ro);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lr.size());
//
//		
//		
//		
//		System.out.println("\n\n\t\t/* * * * * *");
//		System.out.println("\t\t *  STATS   *");
//		System.out.println("\t\t * * * * * */");
//		Stats stats;
//		List<Stats> lstats;
//		
//		System.out.println("\n\n\t--------LISTE DES STATS------------");
//		lstats = ss.getAll();
//		if (lstats.size()!=0) 
//			for (Stats ls1 : lstats ) System.out.println(ls1);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lstats.size());
//		
//		
//		
//		
//		System.out.println("\n\n\t\t/* * * * * *");
//		System.out.println("\t\t *  ITEM   *");
//		System.out.println("\t\t * * * * * */");
//		Item i;
//		List<Item> li ; 
//		
//		System.out.println("\n\n\t--------LISTE DES ITEMS------------");
//		li = is.getAll();
//		if (li.size()!=0)
//			for (Item item : li) System.out.println(item);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+li.size());
//		
//		System.out.println(("\n\n\t--------ADD + REMOVE ITEM------------"));
//		System.out.println("\tAjout de l'item baton blm");
//		is.ajouterItem("Diamond Globe", 375, Slot.Weapon, jobi5, null, null, null);
//		li = is.getAll();
//		if (li.size()!=0)
//			for (Item item : li) System.out.println(item);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+li.size());
//		System.out.println("\n\tSuppression de l'item ajouté");
//		is.removeItem(is.getById(is.getByNameStrict("Diamond Globe").getId()));
//		li = is.getAll();
//		if (li.size()!=0)
//			for (Item item : li) System.out.println(item);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+li.size());
//		System.out.println("\n\tSuppression de l'item ajouté");
//		
//		
//		System.out.println("\n\n\t--------ITEMS PAR NOM---[Coat]---------");
//		li = is.getByName("Coat");
//		if (li.size()!=0)
//			for (Item item : li) System.out.println(item);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+li.size());
//		
//		System.out.println("\n\n\t--------ITEMS PAR JOB---[JOB.WHITEMAGE]---------");
//		li = is.getByJob(Job.WhiteMage);
//		if (li.size()!=0)
//			for (Item item : li) System.out.println(item);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+li.size());
//		
//		System.out.println("\n\n\t--------ITEMS PAR SLOT---[SLOT.WEAPON]---------");
//		li = is.getBySlot(Slot.Weapon);
//		if (li.size()!=0)
//			for (Item item : li) System.out.println(item);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+li.size());
//		
//		System.out.println("\n\n\t--------ITEMS PAR ILVL MIN---[372]---------");
//		li = is.getByMin(372);
//		if (li.size()!=0)
//			for (Item item : li) System.out.println(item);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+li.size());
//		
//		System.out.println("\n\n\t--------ITEMS PAR ILVL MAX---[372]---------");
//		li = is.getByMax(372);
//		if (li.size()!=0)
//			for (Item item : li) System.out.println(item);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+li.size());
//		
//		System.out.println("\n\n\t--------ITEMS PAR ILVL MIN MAX---[372 374]---------");
//		li = is.getByMinMax(372, 374);
//		if (li.size()!=0)
//			for (Item item : li) System.out.println(item);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+li.size());
//		
//		System.out.println("\n\n\t--------ADD ITEMS JOB---[DIAMOND CANE JOB.ASTROLODIAN]---------");
//		is.addItemJob(is.getByNameStrict("Diamond Cane"), Job.Astrologian);
//		System.out.println(is.getByNameStrict("Diamond Cane"));
//		
//		System.out.println("\n\n\t--------REMOVE ITEMS JOB---[DIAMOND CANE JOB.ASTROLODIAN]---------");
//		is.removeItemJob(is.getByNameStrict("Diamond Cane"), Job.Astrologian);
//		System.out.println(is.getByNameStrict("Diamond Cane"));
//		
//		
//		
//		
//		System.out.println("\n\n\t\t/* * * * * * * * * * *");
//		System.out.println("\t\t *  ITEM <-> STATS   *");
//		System.out.println("\t\t * * * * * * * * * * */");
//		
//		System.out.println("\n\n\t--------ADD ITEMS STATS---[DIAMOND CANE STATS1]---------");
//		is.addItemStats(is.getByNameStrict("Diamond Cane"), stats1);
//		System.out.println(is.getByNameStrict("Diamond Cane"));
//		
//		System.out.println("\n\n\t--------ADD ITEMS STATS---[DIAMOND CANE STATS2]---------");
//		is.addItemStats(is.getByNameStrict("Diamond Cane"), stats3);
//		System.out.println(is.getByNameStrict("Diamond Cane"));
//		
//		System.out.println("\n\n\t--------REMOVE ITEMS STATS---[DIAMOND CANE STATS2]---------");
//		is.removeItemStats(is.getByNameStrict("Diamond Cane"), stats3);
//		System.out.println(is.getByNameStrict("Diamond Cane"));
//
//		
//
//
//		System.out.println("\n\n\t\t/* * * * * * * * * * *");
//		System.out.println("\t\t *  MEMBRE <-> CLASS *");
//		System.out.println("\t\t * * * * * * * * * * */");
//		
//		System.out.println("\n\n\t--------MEMBRE PAR ROSTER---[Orion]---------");
//		lm = ms.getByRoster(rs.getByNomStrict("Orion"));
//		if (lm.size()!=0) 
//			for (Membre me : lm ) System.out.println(me);
//		else System.out.println("\tRien trouvé");
//		System.out.println("\tNombre de résultats : "+lm.size());
//		
//		
//		
//		System.out.println("\n\n\t\t/* * * * * * * * *  * * * *");
//		System.out.println("\t\t *  MEMBRE <-> LISTPRIO   *");
//		System.out.println("\t\t * * * * * *  * * * * * * */");
		
		
		
		
		
	
		
		
		
		
		
	}

}
