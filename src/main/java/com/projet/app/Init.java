package com.projet.app;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Init {
	public static void main( String[] args ) throws Exception {
		// creation d'une session factory
		System.out.println("> creation de la sessionFactory");
		SessionFactory sf = new Configuration().configure().buildSessionFactory();
		
		sf.close();
		System.out.println("> done");
	}
}
