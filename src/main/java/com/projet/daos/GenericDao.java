package com.projet.daos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.projet.utils.HibernateUtil;

public abstract class GenericDao<T> {

	private Class<T> clazz;
	
	public GenericDao(Class<T> clazz) {
		this.clazz = clazz;
	}

	public void save(T c) throws Exception {
		SessionFactory sf = HibernateUtil.getSessionFactory(); 
		Session s = sf.openSession();
		save(c,s);
		s.close();	
	}
	
	public void save(T c, Session s, Transaction t) throws Exception {
		s.saveOrUpdate(c);
	}
	
	public void save(T c, Session s) throws Exception {
		s.beginTransaction();
		try {
			save(c,s, s.getTransaction());
			System.out.println("save OK");
			s.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("save FAIL");
			s.getTransaction().rollback();
			throw e;
		}
	}
	
	public void delete(T c) throws Exception {
		SessionFactory sf = HibernateUtil.getSessionFactory(); 
		Session s = sf.openSession();
		delete(c,s);
		s.close();	
	}
	
	public void delete(T c, Session s, Transaction t) throws Exception {
		s.delete(c);
	}
	
	public void delete(T c, Session s) throws Exception {
		s.beginTransaction();
		try {
			delete(c,s, s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
			throw e;
		}
	}
	
	public T findById(int id) {
		SessionFactory sf = HibernateUtil.getSessionFactory(); 
		Session s = sf.openSession();
		T c = findById(id, s);
		s.close();
		return c;
	}	
	
	public T findById(int id, Session s) {
		return s.get(clazz, id);
	}
	
	public List<T> findAll() {
		SessionFactory sf = HibernateUtil.getSessionFactory(); 
		Session s = sf.openSession();
		List<T> clients = findAll(s);
		s.close();
		return clients;
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findAll(Session s) {
		return s.createQuery("from "+clazz.getName()).list();
	} 
	
	public void merge(T c) throws Exception {
		SessionFactory sf = HibernateUtil.getSessionFactory(); 
		Session s = sf.openSession();
		merge(c,s);
		s.close();	
	}
	
	public void merge(T c, Session s, Transaction t) throws Exception {
		s.merge(c);
	}
	
	public void merge(T c, Session s) throws Exception {
		s.beginTransaction();
		try {
			merge(c,s, s.getTransaction());
			System.out.println("save OK");
			s.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("save FAIL");
			s.getTransaction().rollback();
			throw e;
		}
	}
}
