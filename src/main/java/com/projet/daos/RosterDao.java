package com.projet.daos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.projet.models.Membre;
import com.projet.models.Raid;
import com.projet.models.Roster;
import com.projet.utils.HibernateUtil;

public class RosterDao extends GenericDao<Roster>{

	public RosterDao() {
		super(Roster.class);
	}



	/***********
	 * FIND BY *
	 ***********/

	//NOM						OK
	//NOM STRICT				OK
	//AJOUTER MEMBRE			OK
	//AJOUTER ROSTER RAID		OK


	//NOM
	public List<Roster> findByNom(String nom) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Roster> r = findByNom(nom, s);
		return r;
	}

	@SuppressWarnings("unchecked")
	public List<Roster> findByNom(String nom, Session s) {
		return (List<Roster>) s.createQuery("FROM Roster "
				+ "WHERE nom LIKE :nom")
				.setParameter("nom", "%"+nom+"%")
				.list();
	}

	
	//NOM
		public Roster findByNomStrict(String nom) {
			Session s = HibernateUtil.getSessionFactory().openSession();
			Roster r = findByNomStrict(nom, s);
			return r;
		}

		public Roster findByNomStrict(String nom, Session s) {
			return (Roster) s.createQuery("FROM Roster "
					+ "WHERE nom LIKE :nom")
					.setParameter("nom", nom)
					.uniqueResult();
		}
	
		
	//AJOUTER ROSTER <--> MEMBRE
	public void ajouterRosterMembre(Roster r, Membre m) throws Exception {
		Session s = HibernateUtil.getSessionFactory().openSession();
		ajouterRosterMembre(r, m, s);
		s.close();
	}

	public void ajouterRosterMembre(Roster r, Membre m, Session s) throws Exception {
		s.beginTransaction();
		try {
			ajouterRosterMembre(r, m,s,  s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
			throw e;
		}
	}

	public void ajouterRosterMembre(Roster r, Membre m, Session s, Transaction t) {
		Roster r1 = (Roster) s.load(Roster.class, r.getId());
		Membre m1 = (Membre) s.load(Membre.class, m.getId());
		r1.getMembres().add(m1);
		m1.setRoster(r1);
	}

	//AJOUTER ROSTER <--> RAID
		public void ajouterRosterRaid(Roster r, Raid ra) throws Exception {
			Session s = HibernateUtil.getSessionFactory().openSession();
			ajouterRosterRaid(r, ra, s);
			s.close();
		}

		public void ajouterRosterRaid(Roster r, Raid ra, Session s) throws Exception {
			s.beginTransaction();
			try {
				ajouterRosterRaid(r, ra, s, s.getTransaction());
				s.getTransaction().commit();
			} catch (Exception e) {
				s.getTransaction().rollback();
				throw e;
			}
		}

		public void ajouterRosterRaid(Roster r, Raid ra, Session s, Transaction t) {
			Roster r1 = (Roster) s.load(Roster.class, r.getId());
			Raid ra1 = (Raid) s.load(Raid.class, ra.getId());
			r1.getRaids().add(ra1);
			ra1.setRoster(r1);;
			
		}
}
