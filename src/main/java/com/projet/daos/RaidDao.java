package com.projet.daos;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.Session;
import com.projet.models.Raid;
import com.projet.models.Roster;
import com.projet.utils.HibernateUtil;

public class RaidDao extends GenericDao<Raid> {
	
	public RaidDao() {
		super(Raid.class);
	}

	
	/**********
	 * FIND BY **
	 *************/
	
	//DATE						OK	
	//DATE STRICT				OK
	//ROSTER					no
	
	//DATE
	public List<Raid> findByDate(LocalDateTime date) {
		Session s= HibernateUtil.getSessionFactory().openSession();
		List<Raid> r = findByDate(date, s);
		return r;
	}
	
	@SuppressWarnings("unchecked")
	public List<Raid> findByDate(LocalDateTime date, Session s) {
		return (List<Raid>) s.createQuery("FROM Raid "
				+ "WHERE :date IN date")
				.setParameter("date", date)
				.list();
	}
	
	//DATE STRICT
	public Raid findByDateStrict(LocalDateTime date) {
		Session s= HibernateUtil.getSessionFactory().openSession();
		Raid r = findByDateStrict(date, s);
		return r;
	}
	
	public Raid findByDateStrict(LocalDateTime date, Session s) {
		return (Raid) s.createQuery("FROM Raid "
				+ "WHERE :date IN date")
				.setParameter("date", date)
				.uniqueResult();
	}
	
	
	//ROSTER
	public List<Raid> findByRoster(Roster r) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Raid> lr = findByRoster(r, s);
		return lr;
	}
	
	@SuppressWarnings("unchecked")
	public List<Raid> findByRoster(Roster r, Session s) {
		return (List<Raid>) s.createQuery("FROM Raid "
				+ "WHERE :rid IN roster_id")
			.setParameter("rid", r.getId())
			.list();
	}
}
