package com.projet.daos;

import java.util.List;

import org.hibernate.Session;

import com.projet.models.Loot;
import com.projet.utils.HibernateUtil;

public class LootDao extends GenericDao<Loot>{

	public LootDao() {
		super(Loot.class);
	}

	
	/**********
	 * FIND BY **
	 *************/
	
	//item
	public List<Loot> findByItem(String nom) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Loot> l = findByItem(nom, s);
		return l;
	}
	
	@SuppressWarnings("unchecked")
	public List<Loot> findByItem(String nom, Session s) {
		return (List<Loot>) s.createQuery("from Loot inner join Item on loot.item_id = item.id where item.nom = %:nom%")
				.setParameter("nom", "%"+nom+"%");
	}
	
	//membre
	public List<Loot> findByPseudo(String name) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Loot> l = findByPseudo(name, s);
		return l;
	}
	
	@SuppressWarnings("unchecked")
	public List<Loot> findByPseudo(String name, Session s) {
		return (List<Loot>) s.createQuery("from loot inner join Utilisateur on loot.membre_id = Utilisateur.id where Utilisateur.prenom = :uname or Utilisateur.nom = :uname")
				.setParameter("uname", "%"+name+"%");
	}
	
	//raid
	public List<Loot> findByRaid(String date) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Loot> l = findByRaid(date, s);
		return l;
	}
	
	@SuppressWarnings("unchecked")
	public List<Loot> findByRaid(String date, Session s) {
		return (List<Loot>) s.createNamedQuery("from loot inner join raid on loot.raid_id = raid.id where raid.date = :date")
				.setParameter("date", date);
	}
}
