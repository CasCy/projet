package com.projet.daos;

import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.projet.models.Job;
import com.projet.models.Membre;
import com.projet.models.Prio;
import com.projet.models.Role;
import com.projet.models.Roster;
import com.projet.utils.HibernateUtil;



public class MembreDao extends GenericDao<Membre>{

	public MembreDao() {
		super(Membre.class);
	}


	/* * * * * *									/* * * * * *					/* * * * * *
	 * FIND BY *									*   ADD    *					*  REMOVE  *
	 * * * * * * 									* * * * * /* 					* * * * * */



	//USERNAME && PASSWORD				OK			ROSTER			OK				ROSTER
	//USERNAME STRICT					OK			ITEM			???				ITEM			???
	//PSEUDO (DANS PRENOM OU NOM)		OK			JOBS							JOBS
	//JOB								OK			ROLES							ROMES
	//ROLE								OK			PRIOs							PRIOS
	//SERVEUR							OK






	//USERNAME && PASSWORD

	public Membre findByUsernameAndPassword(String username, String password) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Membre m = findByUsernameAndPassword(username, password, s);
		s.close();
		return m;		
	}

	public Membre findByUsernameAndPassword(String username, String password, Session s) {
		return (Membre) s.createQuery("FROM Membre "
				+ "WHERE username = :uname AND password = :pwd")
				.setParameter("uname", username)
				.setParameter("pwd", password)
				.uniqueResult();
	}


	//USERNAME STRICT		

	public Membre findByUsername(String username) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Membre m = findByUsername(username, s);
		s.close();
		return m;		
	}

	public Membre findByUsername(String username, Session s) {
		return (Membre) s.createQuery("FROM Membre "
				+ "WHERE username = :uname")
				.setParameter("uname", username)
				.uniqueResult();
	}


	//PSEUDO (DANS PRENOM OU NOM)

	public List<Membre> findByPseudo(String name) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Membre> m = findByPseudo(name, s);
		s.close();
		return m;		
	}

	@SuppressWarnings("unchecked")
	public List<Membre> findByPseudo(String name, Session s) {
		return (List<Membre>) s.createQuery("FROM Utilisateur "
				+ "WHERE prenom LIKE :uname OR nom LIKE :uname ")
				.setParameter("uname", "%"+name+"%")
				.list();
	}


	//JOB

	public List<Membre> findByJob(Job job) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Membre> m = findByJob(job, s);
		s.close();
		return m;		
	}

	@SuppressWarnings("unchecked")
	public List<Membre> findByJob(Job job, Session s) {
		return (List<Membre>) s.createQuery("FROM Membre m WHERE :job IN elements(m.jobs)")
				.setParameter("job", job)
				.list();
	}


	//ROLE

	public List<Membre> findByRole(Role role) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Membre> m = findByRole(role, s);
		s.close();
		return m;		
	}

	@SuppressWarnings("unchecked")
	public List<Membre> findByRole(Role role, Session s) {
		return (List<Membre>) s.createQuery("FROM Membre m WHERE :role IN elements(m.roles)")
				.setParameter("role", role)
				.list();
	}


	//SERVEUR

	public List<Membre> findByServeur(String serveur) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Membre> m = findByServeur(serveur, s);
		s.close();
		return m;		
	}

	@SuppressWarnings("unchecked")
	public List<Membre> findByServeur(String serveur, Session s) {
		return (List<Membre>) s.createQuery("FROM Membre "
				+ "WHERE serveur LIKE :serveur")
				.setParameter("serveur", serveur)
				.list();
	}


	//ROSTER

	public List<Membre> findByRoster(Roster r) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Membre> lm = findByRoster(r, s);
		s.close();
		return lm;
	}

	@SuppressWarnings("unchecked")
	public List<Membre> findByRoster(Roster r, Session s) {
		return (List<Membre>) s.createQuery("From Membre m "
				+"WHERE :id IN m.roster")
				.setParameter("id", r.getId())
				.list();

	}

	//	//ITEM
	//	
	//	public List<Membre> findByItem (String item) {
	//		Session s = HibernateUtil.getSessionFactory().openSession();
	//		List<Membre> l = findByItem(item, s);
	//		return l;
	//	}
	//
	//	@SuppressWarnings("unchecked")
	//	public List<Membre> findByItem(String item, Session s) {
	//		return (List<Membre>) s.createQuery("FROM Membre "
	//				+ "INNER JOIN Loot "
	//				+ "ON Loot.membre_id = Membre.id "
	//				+ "INNER JOIN Item "
	//				+ "ON Loot.item_id = Item.id "
	//				+ "WHERE Item.nom = :iname")
	//			.setParameter("iname", item)
	//			.list();
	//	}


	//ADD JOB

	public void ajouterMembreJobs(Membre m, Set<Job> lj) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		ajouterMembreJobs(m, lj, s);
		s.close();
	}

	public void ajouterMembreJobs(Membre m, Set<Job> lj, Session s) {
		s.beginTransaction();
		try {
			ajouterMembreJobs(m, lj, s, s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
		}
	}

	public void ajouterMembreJobs(Membre m, Set<Job> lj, Session s, Transaction t) {
		m = (Membre) s.load(Membre.class,  m.getId());		
		m.setJobs(lj);
	}


	//ADD ROLE

	public void ajouterMembreRoles(Membre m, Set<Role> lr) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		ajouterMembreRoles(m, lr, s);
		s.close();
	}

	public void ajouterMembreRoles(Membre m, Set<Role> lr, Session s) {
		s.beginTransaction();
		try {
			ajouterMembreRoles(m, lr, s, s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
		}
	}

	public void ajouterMembreRoles(Membre m, Set<Role> lr, Session s, Transaction t) {
		m = (Membre) s.load(Membre.class,  m.getId());		
		m.setRoles(lr);
	}


	//ADD PRIO

	public void ajouterMembrePrios(Membre m, List<Prio> lp) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		ajouterMembrePrios(m, lp, s);
		s.close();
	}

	public void ajouterMembrePrios(Membre m, List<Prio> lp, Session s) {
		s.beginTransaction();
		try {
			ajouterMembrePrios(m, lp, s, s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
		}
	}

	public void ajouterMembrePrios(Membre m, List<Prio> lp, Session s, Transaction t) {
		m = (Membre) s.load(Membre.class,  m.getId());		
		m.setPrios(lp);
	}


	//REMOVE ITEM


	//REMOVE JOB

	public void removeMembreJobs(Membre m, Set<Job> lj) throws Exception {
		Session s = HibernateUtil.getSessionFactory().openSession();
		removeMembreJobs(m, lj, s);
		s.close();
	}

	public void removeMembreJobs(Membre m, Set<Job> lj, Session s) throws Exception {
		s.beginTransaction();
		try {
			removeMembreJobs(m, lj, s, s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
		}
	}

	public void removeMembreJobs(Membre m, Set<Job> lj, Session s, Transaction t) {
		m = (Membre) s.load(Membre.class,  m.getId());		
		m.getJobs().remove(lj);
	}


	//REMOVE ROLE

	public void removeMembreRoles(Membre m, Set<Role> lr) throws Exception {
		Session s = HibernateUtil.getSessionFactory().openSession();
		removeMembreRoles(m, lr, s);
		s.close();
	}

	public void removeMembreRoles(Membre m, Set<Role> lr, Session s) throws Exception {
		s.beginTransaction();
		try {
			removeMembreRoles(m, lr, s, s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
		}
	}

	public void removeMembreRoles(Membre m, Set<Role> lr, Session s, Transaction t) {
		m = (Membre) s.load(Membre.class,  m.getId());		
		m.getRoles().remove(lr);
	}


	//REMOVE PRIO

	public void removeMembrePrios(Membre m, List<Prio> lp) throws Exception {
		Session s = HibernateUtil.getSessionFactory().openSession();
		removeMembrePrios(m, lp, s);
		s.close();
	}

	public void removeMembrePrios(Membre m, List<Prio> lp, Session s) throws Exception {
		s.beginTransaction();
		try {
			removeMembrePrios(m, lp, s, s.getTransaction());
			s.getTransaction().commit();
		} catch (Exception e) {
			s.getTransaction().rollback();
		}
	}

	public void removeMembrePrios(Membre m, List<Prio> lp, Session s, Transaction t) {
		m = (Membre) s.load(Membre.class,  m.getId());		
		m.getPrios().remove(lp);
	}

}
