package com.projet.daos;

import com.projet.models.ListeVoeux;

public class ListeVoeuxDao extends GenericDao<ListeVoeux>{
	
	public ListeVoeuxDao() {
		super(ListeVoeux.class);
	}

}
