package com.projet.daos;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.projet.models.Item;
import com.projet.models.Job;
import com.projet.models.Slot;
import com.projet.models.Stats;
import com.projet.utils.HibernateUtil;

public class ItemDao extends GenericDao<Item>{

	public ItemDao() {
		super(Item.class);
	}


	/* * * * * *									/* * * * * *
	 * FIND BY *									 *   ADD   *
	 * * * * * *									 * * * * * */

	//NAME						OK					ADD ITEM JOB			OK
	//NAME STRICT				OK					REMOVE ITEM JOB			OK
	//JOB						OK					ADD ITEM STAT			OK	
	//JOB						OK					REMOVE ITEM STAT		OK
	//SLOT						OK					ADD ITEM WL
	//NAME & ILVL				OK					REMOVE ITEM WL
	//ILVL MIN					OK					ADD ITEM LOOT
	//ILVL MAX					OK					
	//ILVL MIN MAX				OK					 
	//LOOT --> MEMBRE						
	//WL --> MEMBRE							



	//NAME

	public List<Item> findByName(String name) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Item> li = findByName(name, s);
		s.close();
		return li;
	}

	@SuppressWarnings("unchecked")
	public List<Item> findByName(String name, Session s) {
		return (List<Item>) s.createQuery("FROM Item "
				+ "WHERE nom LIKE :nom")
				.setParameter("nom", "%"+name+"%")
				.list();
	}


	//NAME

	public Item findByNameStrict(String name) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Item i = findByNameStrict(name, s);
		s.close();
		return i;
	}

	public Item findByNameStrict(String name, Session s) {
		return (Item) s.createQuery("FROM Item "
				+ "WHERE nom LIKE :nom")
				.setParameter("nom", name)
				.uniqueResult();
	}


	//JOB

	public List<Item> findByJob(Job job) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Item> li = findByJob(job, s);
		s.close();
		return li;
	}

	@SuppressWarnings("unchecked")
	public List<Item> findByJob(Job job, Session s) {
		return (List<Item>) s.createQuery("FROM Item "
				+ "WHERE :job IN elements(jobs)")
				.setParameter("job", job)
				.list();
	}


	//SLOT

	public List<Item> findBySlot(Slot slot) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Item> li = findBySlot(slot, s);
		s.close();
		return li;
	}

	@SuppressWarnings("unchecked")
	public List<Item> findBySlot(Slot slot, Session s) {
		return (List<Item>) s.createQuery("FROM Item "
				+ "WHERE :slot = slot")
				.setParameter("slot", slot)
				.list();
	}


	//NAME & ILVL & SLOT & JOB

	public List<Item> findByNameIlvl(String name, int min, int max) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Item> li = findByNameIlvl(name, min, max, s);
		s.close();
		return li;
	}

	@SuppressWarnings("unchecked")
	public List<Item> findByNameIlvl(String name, int min, int max, Session s) {
		return (List<Item>) s.createQuery("From Item "
				+ "WHERE (nom LIKE :name) AND (ilvl >= :min) AND (ilvl <= :max)")
				.setParameter("name", "%"+name+"%")
				.setParameter("min",  min)
				.setParameter("max",  max)
				.list();
	}

	public List<Item> findByNameIlvlJob(String name, int min, int max, Job job) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Item> li = findByNameIlvlJob(name, min, max, job, s);
		s.close();
		return li;
	}

	@SuppressWarnings("unchecked")
	public List<Item> findByNameIlvlJob(String name, int min, int max, Job job, Session s) {	
		return (List<Item>) s.createQuery("From Item "
				+ "WHERE (nom LIKE :name) AND (ilvl >= :min) AND (ilvl <= :max) AND (:job IN elements(jobs))")
				.setParameter("name", "%"+name+"%")
				.setParameter("min",  min)
				.setParameter("max",  max)
				.setParameter("job", job)
				.list();
	}

	public List<Item> findByNameIlvlSlot(String name, int min, int max, Slot slot) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Item> li = findByNameIlvlSlot(name, min, max, slot, s);
		s.close();
		return li;
	}

	@SuppressWarnings("unchecked")
	public List<Item> findByNameIlvlSlot(String name, int min, int max, Slot slot, Session s) {	
		return (List<Item>) s.createQuery("From Item "
				+ "WHERE (nom LIKE :name) AND (ilvl >= :min) AND (ilvl <= :max) AND (slot = :slot)")
				.setParameter("name", "%"+name+"%")
				.setParameter("min",  min)
				.setParameter("max",  max)
				.setParameter("slot", slot)
				.list();
	}

	public List<Item> findByNameIlvlSlotJob(String name, int min, int max, Slot slot, Job job) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Item> li = findByNameIlvlSlotJob(name, min, max, slot, job, s);
		s.close();
		return li;
	}

	@SuppressWarnings("unchecked")
	public List<Item> findByNameIlvlSlotJob(String name, int min, int max, Slot slot, Job job, Session s) {	
		return (List<Item>) s.createQuery("From Item "
				+ "WHERE (nom LIKE :name) AND (ilvl >= :min) AND (ilvl <= :max) AND (slot = :slot) AND (:job IN elements(jobs))")
				.setParameter("name", "%"+name+"%")
				.setParameter("min",  min)
				.setParameter("max",  max)
				.setParameter("job", job)
				.setParameter("slot", slot)
				.list();
	}



//ILVL MIN

public List<Item> findByMinIlvl(int min) {
	Session s = HibernateUtil.getSessionFactory().openSession();
	List<Item> li = findByMinIlvl(min, s);
	s.close();
	return li;
}

@SuppressWarnings("unchecked")
public List<Item> findByMinIlvl(int min, Session s) {
	return (List<Item>) s.createQuery("FROM Item "
			+ "WHERE ilvl >= :ilvl")
			.setParameter("ilvl", min)
			.list();
}


//ILVL MAX

public List<Item> findByMaxIlvl(int max) {
	Session s = HibernateUtil.getSessionFactory().openSession();
	List<Item> li = findByMaxIlvl(max, s);
	s.close();
	return li;
}

@SuppressWarnings("unchecked")
public List<Item> findByMaxIlvl(int max, Session s ) {
	return (List<Item>) s.createQuery("FROM Item "
			+ "WHERE ilvl <= :ilvl")
			.setParameter("ilvl", max)
			.list();
}


//ILVL MINMAX

public List<Item> findByMinMaxIlvl(int min, int max) {
	Session s = HibernateUtil.getSessionFactory().openSession();
	List<Item> li = findByMinMaxIlvl(min, max, s);
	s.close();
	return li;
}

@SuppressWarnings("unchecked")
public List<Item> findByMinMaxIlvl(int min, int max, Session s ) {
	return (List<Item>) s.createQuery("FROM Item "
			+ "WHERE ilvl >= :min AND ilvl <= :max")
			.setParameter("min", min)
			.setParameter("max", max)
			.list();
}


//	//LOOT --> MEMBRE
//
//	public List<Item> findByLoot(String username) {
//		Session s = HibernateUtil.getSessionFactory().openSession();
//		List<Item> i = findByLoot(username, s);
//		return i;
//	}
//
//	@SuppressWarnings("unchecked")
//	public List<Item> findByLoot(String username, Session s) {
//		return (List<Item>) s.createQuery("FROM Item"
//				+ "INNER JOIN Loot"
//				+ "ON Loot.item_id = Item.id"
//				+ "INNER JOIN Membre"
//				+ "ON Loot.item_id = Membre.id"
//				+ "WHERE Membre.username = :uname")
//				.setParameter("uname", username)
//				.list();
//	}
//
//
//	//WL --> MEMBRE
//
//	public List<Item> findByWl(String username) {
//		Session s = HibernateUtil.getSessionFactory().openSession();
//		List<Item> i = findByWl(username, s);
//		return i;
//	}
//
//	@SuppressWarnings("unchecked")
//	public List<Item> findByWl(String username, Session s) {
//		return (List<Item>) s.createQuery("FROM Item"
//				+ "INNER JOIN Liste_voeux"
//				+ "ON Listevoeux_item.item_id = Item.id"
//				+ "INNER JOIN Listevoeux"
//				+ "ON Listevoeux_Item.listeVoeux_id = Listevoeux.id"
//				+ "INNER JOIN Membre"
//				+ "ON Listevoeux.id = Membre.id"
//				+ "WHERE Membre.username = :uname")
//				.setParameter("uname", username)
//				.list();
//
//	}




//ADD ITEM JOB

public void addItemJob(Item i, Job j) throws Exception {
	Session s = HibernateUtil.getSessionFactory().openSession();
	addItemJob(i, j, s);
	s.close();
}

public void addItemJob(Item i, Job j, Session s) throws Exception {
	s.beginTransaction();
	try {
		addItemJob(i, j, s, s.getTransaction());
		s.getTransaction().commit();
	} catch (Exception e) {
		s.getTransaction().rollback();
		throw e;
	}
}

public void addItemJob(Item i, Job j, Session s, Transaction t) {
	Item item = (Item) s.load(Item.class, i.getId());
	item.getJobs().add(j);
}


//REMOVE ITEM JOB

public void removeItemJob(Item i, Job j) throws Exception {
	Session s = HibernateUtil.getSessionFactory().openSession();
	removeItemJob(i, j, s);
	s.close();
}

public void removeItemJob(Item i, Job j, Session s) throws Exception {
	s.beginTransaction();
	try {
		removeItemJob(i, j, s, s.getTransaction());
		s.getTransaction().commit();
	} catch (Exception e) {
		s.getTransaction().rollback();
		throw e;
	}
}

public void removeItemJob(Item i, Job j, Session s, Transaction t) {
	Item item = (Item) s.load(Item.class, i.getId());
	item.getJobs().remove(j);
}


//ADD ITEM STATS

public void addItemStats(Item i, Stats st) throws Exception {
	Session s = HibernateUtil.getSessionFactory().openSession();
	addItemStats(i, st, s);
	s.close();
}

public void addItemStats(Item i, Stats st, Session s) throws Exception {
	s.beginTransaction();
	try {
		addItemStats(i, st, s, s.getTransaction());
		s.getTransaction().commit();
	} catch (Exception e) {
		s.getTransaction().rollback();
		throw e;
	}
}

public void addItemStats(Item i, Stats st, Session s, Transaction t) {
	Item item = (Item) s.load(Item.class, i.getId());
	item.getStats().add(st);
}


//REMOVE ITEM STATS

public void removeItemStats(Item i, Stats st) throws Exception {
	Session s = HibernateUtil.getSessionFactory().openSession();
	removeItemStats(i, st, s);
	s.close();
}

public void removeItemStats(Item i, Stats st, Session s) throws Exception {
	s.beginTransaction();
	try {
		removeItemStats(i, st, s, s.getTransaction());
		s.getTransaction().commit();
	} catch (Exception e) {
		s.getTransaction().rollback();
		throw e;
	}
}

public void removeItemStats(Item i, Stats st, Session s, Transaction t) {
	Item item = (Item) s.load(Item.class, i.getId());
	item.getStats().remove(st);

}

}
